package net.openid.conformance.openbanking_brasil.opendata.investmentsAPI;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.openbanking_brasil.opendata.investments.GetFixedIncomeBankValidatorV1RC2;
import net.openid.conformance.openbanking_brasil.opendata.investments.GetFixedIncomeCreditValidatorV1RC2;
import net.openid.conformance.openbanking_brasil.opendata.investments.GetFundsValidatorV1RC2;
import net.openid.conformance.openbanking_brasil.opendata.investments.GetTreasureValidatorV1RC2;
import net.openid.conformance.openbanking_brasil.opendata.investments.GetVariableIncomeValidatorV1RC2;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

public class InvestmentsValidatorsV1RC2Test extends AbstractJsonResponseConditionUnitTest {

	@Test
	@UseResurce("jsonResponses/opendata/investments/GetFixedIncomeBankResponseV1RC2.json")
	public void GetFixedIncomeBankValidatorV1RC2() {
		run(new GetFixedIncomeBankValidatorV1RC2());
	}

	@Test
	@UseResurce("jsonResponses/opendata/investments/GetFixedIncomeCreditResponseV1RC2.json")
	public void GetFixedIncomeCreditValidatorV1RC2() {
		run(new GetFixedIncomeCreditValidatorV1RC2());
	}

	@Test
	@UseResurce("jsonResponses/opendata/investments/GetFundsResponseV1RC2.json")
	public void GetFundsValidatorV1RC2() {
		run(new GetFundsValidatorV1RC2());
	}

	@Test
	@UseResurce("jsonResponses/opendata/investments/GetTreasureResponseV1RC2.json")
	public void GetTreasureValidatorV1RC2() {
		run(new GetTreasureValidatorV1RC2());
	}

	@Test
	@UseResurce("jsonResponses/opendata/investments/GetVariableIncomeResponseV1RC2.json")
	public void GetVariableIncomeValidatorV1RC2() {
		run(new GetVariableIncomeValidatorV1RC2());
	}
}

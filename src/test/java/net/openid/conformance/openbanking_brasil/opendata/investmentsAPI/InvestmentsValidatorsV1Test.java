package net.openid.conformance.openbanking_brasil.opendata.investmentsAPI;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.condition.client.jsonAsserting.ErrorMessagesUtils;
import net.openid.conformance.openbanking_brasil.opendata.investments.GetFixedIncomeBankValidatorV1;
import net.openid.conformance.openbanking_brasil.opendata.investments.GetFixedIncomeCreditValidatorV1;
import net.openid.conformance.openbanking_brasil.opendata.investments.GetFundsValidatorV1;
import net.openid.conformance.openbanking_brasil.opendata.investments.GetTreasureValidatorV1;
import net.openid.conformance.openbanking_brasil.opendata.investments.GetVariableIncomeValidatorV1;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import static org.hamcrest.core.StringContains.containsString;
import static org.junit.Assert.assertThat;

public class InvestmentsValidatorsV1Test extends AbstractJsonResponseConditionUnitTest {

	@Test
	@UseResurce("jsonResponses/opendata/investments/GetFixedIncomeBankResponseV1.json")
	public void GetFixedIncomeBankValidatorV1() {
		run(new GetFixedIncomeBankValidatorV1());
	}

	@Test
	@UseResurce("jsonResponses/opendata/investments/GetFixedIncomeCreditResponseV1.json")
	public void GetFixedIncomeCreditValidatorV1() {
		run(new GetFixedIncomeCreditValidatorV1());
	}

	@Test
	@UseResurce("jsonResponses/opendata/investments/GetFundsResponseV1.json")
	public void GetFundsValidatorV1() {
		run(new GetFundsValidatorV1());
	}

	@Test
	@UseResurce("jsonResponses/opendata/investments/GetFundsResponseError(MustNOTBeEmpty)V1.json")
	public void GetFundsValidatorV1DataMustNotBeEmpty() {
		GetFundsValidatorV1 condition = new GetFundsValidatorV1();
		ConditionError error = runAndFail(condition);
		assertThat(error.getMessage(), containsString(ErrorMessagesUtils
			.createArrayMustNotBeEmptyMessage("data", condition.getApiName())));
	}

	@Test
	@UseResurce("jsonResponses/opendata/investments/GetFundsResponseError(DataMustBeProvided)V1.json")
	public void GetFundsValidatorV1DataMustBeProvided() {
		GetFundsValidatorV1 condition = new GetFundsValidatorV1();
		ConditionError error = runAndFail(condition);
		assertThat(error.getMessage(), containsString(ErrorMessagesUtils
			.createElementNotFoundMessage("data", condition.getApiName())));
	}

	@Test
	@UseResurce("jsonResponses/opendata/investments/GetTreasureResponseV1.json")
	public void GetTreasureValidatorV1() {
		run(new GetTreasureValidatorV1());
	}

	@Test
	@UseResurce("jsonResponses/opendata/investments/GetVariableIncomeResponseV1.json")
	public void GetVariableIncomeValidatorV1() {
		run(new GetVariableIncomeValidatorV1());
	}
}

package net.openid.conformance.openbanking_brasil.opendata.acquiringServices;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

@UseResurce("jsonResponses/opendata/acquiringServices/PersonalAcquiringServicesResponseV1RC2.json")
public class PersonalAcquiringServicesValidatorV1RC2Test extends AbstractJsonResponseConditionUnitTest {

	@Test
	public void evaluate() {
		run(new PersonalAcquiringServicesValidatorV1RC2());
	}
}

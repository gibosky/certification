package net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.condition.client.jsonAsserting.ErrorMessagesUtils;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.validators.CreateConsentErrorValidatorV2;
import net.openid.conformance.util.UseResurce;
import org.hamcrest.core.StringContains;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.HttpStatus;

import static org.hamcrest.Matchers.containsString;
import static org.junit.Assert.assertThat;

public class CreateConsentErrorValidatorV2Test extends AbstractJsonResponseConditionUnitTest {
	@Before
	public void init() {
		setJwt(true);
	}


	@Test
	@UseResurce("jsonResponses/errors/422/good422ConsentErrorResponseV2.json")
	public void validateGood422Error() {
		setJwt(true);
		setStatus(HttpStatus.UNPROCESSABLE_ENTITY.value());
		CreateConsentErrorValidatorV2 condition = new CreateConsentErrorValidatorV2();
		run(condition);
	}

	@Test
	@UseResurce("jsonResponses/errors/422/good422ConsentErrorResponseWithGoodExtraValuesV2.json")
	public void validate422ErrorWithExtraValues() {
		setJwt(true);
		setStatus(HttpStatus.UNPROCESSABLE_ENTITY.value());
		CreateConsentErrorValidatorV2 condition = new CreateConsentErrorValidatorV2();
		run(condition);
	}

	@Test
	@UseResurce("jsonResponses/errors/422/good422ConsentErrorResponseWithBadExtraValuesV2.json")
	public void validate422ErrorWithBadExtraValues() {
		setJwt(true);
		setStatus(HttpStatus.UNPROCESSABLE_ENTITY.value());
		CreateConsentErrorValidatorV2 condition = new CreateConsentErrorValidatorV2();
		ConditionError error = runAndFail(condition);
		assertThat(error.getMessage(), StringContains.containsString(ErrorMessagesUtils.createFieldKeyNotMatchPatternMessage("extraField", condition.getApiName())));

	}

	@Test
	@UseResurce("jsonResponses/errors/422/bad422WrongCode.json")
	public void validateBad422ErrorWrongCode() {
		setJwt(true);
		setStatus(HttpStatus.UNPROCESSABLE_ENTITY.value());
		CreateConsentErrorValidatorV2 condition = new CreateConsentErrorValidatorV2();
		ConditionError conditionError = runAndFail(condition);
		assertThat(conditionError.getMessage(), containsString(ErrorMessagesUtils.createFieldValueNotMatchEnumerationMessage("code", condition.getApiName())));
	}

	@Test
	@UseResurce("jsonResponses/errors/422/good422ConsentErrorResponseWrongArraySizeV2.json")
	public void validateGood422ErrorWrongArraySize() {
		setJwt(true);
		setStatus(HttpStatus.UNPROCESSABLE_ENTITY.value());
		CreateConsentErrorValidatorV2 condition = new CreateConsentErrorValidatorV2();
		ConditionError conditionError = runAndFail(condition);
		assertThat(conditionError.getMessage(), containsString(ErrorMessagesUtils.createArrayIsMoreThanMaxItemsMessage("errors", condition.getApiName())));
	}



	@Test
	@UseResurce("jsonResponses/errors/goodErrorResponse.json")
	public void validateGoodError() {
		setJwt(false);
		setStatus(HttpStatus.FORBIDDEN.value());
		CreateConsentErrorValidatorV2 condition = new CreateConsentErrorValidatorV2();
		run(condition);

	}

	@Test
	@UseResurce("jsonResponses/errors/goodErrorResponseBadArrayLength.json")
	public void validateGoodErrorBadArraySize() {
		setJwt(false);
		setStatus(HttpStatus.FORBIDDEN.value());
		CreateConsentErrorValidatorV2 condition = new CreateConsentErrorValidatorV2();
		ConditionError conditionError = runAndFail(condition);
		assertThat(conditionError.getMessage(), containsString(ErrorMessagesUtils.createArrayIsMoreThanMaxItemsMessage("errors", condition.getApiName())));

	}


	@Test
	@UseResurce("jsonResponses/errors/badErrorBodyResponse.json")
	public void validateBadErrorBodyResponse() {
		setJwt(true);
		setStatus(HttpStatus.UNPROCESSABLE_ENTITY.value());
		CreateConsentErrorValidatorV2 condition = new CreateConsentErrorValidatorV2();
		ConditionError conditionError = runAndFail(condition);
		assertThat(conditionError.getMessage(), containsString("Unable to find element errors on the CreateConsentErrorValidatorV2 API response"));
	}

	@Test
	@UseResurce("jsonResponses/errors/goodErrorResponseBadLengthCode.json")
	public void validateGoodErrorResponseBadLengthCode() {
		setJwt(false);
		setStatus(HttpStatus.FORBIDDEN.value());
		CreateConsentErrorValidatorV2 condition = new CreateConsentErrorValidatorV2();
		ConditionError conditionError = runAndFail(condition);
		assertThat(conditionError.getMessage(), containsString(ErrorMessagesUtils.createFieldValueIsMoreThanMaxLengthMessage("code", condition.getApiName())));
	}

	@Test
	@UseResurce("jsonResponses/errors/goodErrorResponseBadLengthDetail.json")
	public void validateGoodErrorResponseBadLengthDetail() {
		setJwt(false);
		setStatus(HttpStatus.FORBIDDEN.value());
		CreateConsentErrorValidatorV2 condition = new CreateConsentErrorValidatorV2();
		ConditionError conditionError = runAndFail(condition);
		assertThat(conditionError.getMessage(), containsString(ErrorMessagesUtils.createFieldValueIsMoreThanMaxLengthMessage("detail", condition.getApiName())));
	}

	@Test
	@UseResurce("jsonResponses/errors/goodErrorResponseBadLengthTitle.json")
	public void validateGoodErrorResponseBadLengthTitle() {
		setJwt(false);
		setStatus(HttpStatus.FORBIDDEN.value());
		CreateConsentErrorValidatorV2 condition = new CreateConsentErrorValidatorV2();
		ConditionError conditionError = runAndFail(condition);
		assertThat(conditionError.getMessage(), containsString(ErrorMessagesUtils.createFieldValueIsMoreThanMaxLengthMessage("title", condition.getApiName())));
	}

	@Test
	@UseResurce("jsonResponses/errors/goodErrorsMissingCode.json")
	public void validateGoodErrorsMissingCode() {
		setJwt(false);
		setStatus(HttpStatus.FORBIDDEN.value());
		CreateConsentErrorValidatorV2 condition = new CreateConsentErrorValidatorV2();
		ConditionError conditionError = runAndFail(condition);
		assertThat(conditionError.getMessage(), containsString("Unable to find element code on the CreateConsentErrorValidatorV2 API response"));
	}

	@Test
	@UseResurce("jsonResponses/errors/goodErrorsMissingDetail.json")
	public void validateGoodErrorsMissingDetail() {
		setJwt(false);
		setStatus(HttpStatus.FORBIDDEN.value());
		CreateConsentErrorValidatorV2 condition = new CreateConsentErrorValidatorV2();
		ConditionError conditionError = runAndFail(condition);
		assertThat(conditionError.getMessage(), containsString("Unable to find element detail on the CreateConsentErrorValidatorV2 API response"));
	}

	@Test
	@UseResurce("jsonResponses/errors/goodErrorsMissingTitle.json")
	public void validateGoodErrorsMissingTitle() {
		setJwt(false);
		setStatus(HttpStatus.FORBIDDEN.value());
		CreateConsentErrorValidatorV2 condition = new CreateConsentErrorValidatorV2();
		ConditionError conditionError = runAndFail(condition);
		assertThat(conditionError.getMessage(), containsString("Unable to find element title on the CreateConsentErrorValidatorV2 API response"));
	}


}

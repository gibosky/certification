package net.openid.conformance.openbanking_brasil.testmodules.support.generalValidators;

import com.google.gson.JsonElement;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.condition.client.jsonAsserting.ErrorMessagesUtils;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.BodyExtractor;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import java.text.ParseException;
import java.util.Objects;

import static org.junit.Assert.assertThat;
import static org.hamcrest.core.StringContains.containsString;

public class MetaValidatorTest extends AbstractJsonResponseConditionUnitTest {

    private final static String RESPONSE_ENV_KEY = "resource_endpoint_response_full";
    private final static String JSON_RESPONSES_BASE_PATH = "jsonResponses/generalValidators/metaValidator/";

    class CustomValidator extends AbstractJsonAssertingCondition {
        private final MetaValidator metaValidator;

        CustomValidator(boolean isTotalRecordsMandatory, boolean isTotalPagesMandatory, boolean isRequestDateTimeMandatory) {
            metaValidator = new MetaValidator(
                this,
                isTotalRecordsMandatory,
                isTotalPagesMandatory,
                isRequestDateTimeMandatory
            );
        }

        CustomValidator() {
            metaValidator = new MetaValidator(this);
        }

        @Override
        @PreEnvironment(required = RESPONSE_ENV_KEY)
        public Environment evaluate(Environment env) {
            JsonElement body = bodyFrom(env);
            metaValidator.assertMetaObject(body);
            String errorMessage = metaValidator.getErrorMessage();
            if (!Objects.equals(errorMessage, "")) {
                throw error(errorMessage, metaValidator.getArgs());
            }
            return env;
        }

        @Override
        protected JsonElement bodyFrom(Environment env) {
            try {
                return BodyExtractor.bodyFrom(env, RESPONSE_ENV_KEY)
                    .orElseThrow(() -> error("Could not extract body from response"));
            } catch (ParseException e) {
                throw error("Could not parse the body");
            }
        }
    }

    @Test
    @UseResurce(JSON_RESPONSES_BASE_PATH + "goodResponseStandard.json")
    public void validateMetaValidator() {
        CustomValidator condition = new CustomValidator();
        run(condition);
    }

    @Test
    @UseResurce(JSON_RESPONSES_BASE_PATH + "badResponseMissingMeta.json")
    public void validateStructureMissingMeta() {
        CustomValidator condition = new CustomValidator();
        ConditionError error = runAndFail(condition);
        assertThat(error.getMessage(), containsString(
            ErrorMessagesUtils.createElementNotFoundMessage("meta", condition.getApiName())
        ));
    }

    @Test
    @UseResurce(JSON_RESPONSES_BASE_PATH + "responseMissingTotalRecords.json")
    public void validateStructureMissingTotalRecordsNotAllowed() {
        CustomValidator condition = new CustomValidator();
        ConditionError error = runAndFail(condition);
        assertThat(error.getMessage(), containsString(
            ErrorMessagesUtils.createElementNotFoundMessage("totalRecords", condition.getApiName())
        ));
    }

    @Test
    @UseResurce(JSON_RESPONSES_BASE_PATH + "responseMissingTotalRecords.json")
    public void validateStructureMissingTotalRecordsAllowed() {
        CustomValidator condition = new CustomValidator(false, true, true);
        run(condition);
    }

    @Test
    @UseResurce(JSON_RESPONSES_BASE_PATH + "responseMissingTotalPages.json")
    public void validateStructureMissingTotalPagesNotAllowed() {
        CustomValidator condition = new CustomValidator();
        ConditionError error = runAndFail(condition);
        assertThat(error.getMessage(), containsString(
            ErrorMessagesUtils.createElementNotFoundMessage("totalPages", condition.getApiName())
        ));
    }

    @Test
    @UseResurce(JSON_RESPONSES_BASE_PATH + "responseMissingTotalPages.json")
    public void validateStructureMissingTotalPagesAllowed() {
        CustomValidator condition = new CustomValidator(true, false, true);
        run(condition);
    }

    @Test
    @UseResurce(JSON_RESPONSES_BASE_PATH + "responseMissingRequestDateTime.json")
    public void validateStructureMissingRequestDateTimeNotAllowed() {
        CustomValidator condition = new CustomValidator();
        ConditionError error = runAndFail(condition);
        assertThat(error.getMessage(), containsString(
            ErrorMessagesUtils.createElementNotFoundMessage("requestDateTime", condition.getApiName())
        ));
    }

    @Test
    @UseResurce(JSON_RESPONSES_BASE_PATH + "responseMissingRequestDateTime.json")
    public void validateStructureMissingRequestDateTimeAllowed() {
        CustomValidator condition = new CustomValidator(true, true, false);
        run(condition);
    }

    @Test
    @UseResurce(JSON_RESPONSES_BASE_PATH + "badResponseTotalRecordsGreaterThanPageSizeTotalPagesLessOrEqualToOne.json")
    public void validateStructureTotalRecordsGreaterThanPageSizeTotalPagesLessOrEqualToOne() {
        CustomValidator condition = new CustomValidator();
        ConditionError error = runAndFail(condition);
        assertThat(error.getMessage(), containsString(
            "Number of records exceeds page size, but totalPages is not greater than 1"
        ));
    }

    @Test
    @UseResurce(JSON_RESPONSES_BASE_PATH + "badResponseTotalRecordsGreaterThanPageSizeNumberOfRecordsNotEqualPageSize.json")
    public void validateStructureTotalRecordsGreaterThanPageSizeNumberOfRecordsNotEqualPageSize() {
        CustomValidator condition = new CustomValidator();
        ConditionError error = runAndFail(condition);
        assertThat(error.getMessage(), containsString(
            "Number of records does not match pageSize"
        ));
    }

    @Test
    @UseResurce(JSON_RESPONSES_BASE_PATH + "badResponseTotalRecordsGreaterThanPageSizeTotalPagesNotEqualExpectedAmount.json")
    public void validateStructureTotalRecordsGreaterThanPageSizeTotalPagesNotEqualExpectedAmount() {
        CustomValidator condition = new CustomValidator();
        ConditionError error = runAndFail(condition);
        assertThat(error.getMessage(), containsString(
            "The totalPages value is not equal to the amount of pages expected"
        ));
    }

    @Test
    @UseResurce(JSON_RESPONSES_BASE_PATH + "goodResponseTotalRecordsGreaterThanPageSize.json")
    public void validateStructureCorrectTotalRecordsGreaterThanPageSize() {
        CustomValidator condition = new CustomValidator();
        run(condition);
    }

    @Test
    @UseResurce(JSON_RESPONSES_BASE_PATH + "badResponseTotalRecordsSmallerThanPageSizeTotalPagesNotEqualOne.json")
    public void validateStructureTotalRecordsSmallerThanPageSizeTotalPagesNotEqualOne() {
        CustomValidator condition = new CustomValidator();
        ConditionError error = runAndFail(condition);
        assertThat(error.getMessage(), containsString(
            "Number of records does not exceed page size, but totalPages is not equal to 1"
        ));
    }

    @Test
    @UseResurce(JSON_RESPONSES_BASE_PATH + "badResponseTotalRecordsSmallerThanPageSizeNumberOfRecordsNotEqualTotalRecords.json")
    public void validateStructureTotalRecordsSmallerThanPageSizeNumberOfRecordsNotEqualTotalRecords() {
        CustomValidator condition = new CustomValidator();
        ConditionError error = runAndFail(condition);
        assertThat(error.getMessage(), containsString(
            "Number of records does not match totalRecords"
        ));
    }

    @Test
    @UseResurce(JSON_RESPONSES_BASE_PATH + "goodResponseTotalRecordsSmallerThanPageSize.json")
    public void validateStructureCorrectTotalRecordsSmallerThanPageSize() {
        CustomValidator condition = new CustomValidator();
        run(condition);
    }

    @Test
    @UseResurce(JSON_RESPONSES_BASE_PATH + "badResponseZeroRecordsTotalPagesGreaterThanZero.json")
    public void validateStructureZeroRecordsTotalPagesGreaterThanZero() {
        CustomValidator condition = new CustomValidator();
        ConditionError error = runAndFail(condition);
        assertThat(error.getMessage(), containsString(
            "When there are no resources, both totalRecords and totalPages should be 0"
        ));
    }

    @Test
    @UseResurce(JSON_RESPONSES_BASE_PATH + "badResponseZeroRecordsTotalRecordsGreaterThanZero.json")
    public void validateStructureZeroRecordsTotalRecordsGreaterThanZero() {
        CustomValidator condition = new CustomValidator();
        ConditionError error = runAndFail(condition);
        assertThat(error.getMessage(), containsString(
            "When there are no resources, both totalRecords and totalPages should be 0"
        ));
    }

    @Test
    @UseResurce(JSON_RESPONSES_BASE_PATH + "goodResponseZeroRecords.json")
    public void validateStructureCorrectZeroRecords() {
        CustomValidator condition = new CustomValidator();
        run(condition);
    }
}

package net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.condition.client.jsonAsserting.ErrorMessagesUtils;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.validators.ValidateMetaOnlyRequestDateTime;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import static org.hamcrest.Matchers.containsString;
import static org.junit.Assert.assertThat;

public class ValidateMetaOnlyRequestDateTimeTest extends AbstractJsonResponseConditionUnitTest {

	@Test
	@UseResurce("jsonResponses/resourcesAPI/resourcesAPIResponseMultipleResources.json")
	public void validateStructure() {
		environment.mapKey(ValidateMetaOnlyRequestDateTime.RESPONSE_ENV_KEY, "resource_endpoint_response_full");
		run(new ValidateMetaOnlyRequestDateTime());
	}

	@Test
	@UseResurce("jsonResponses/resourcesAPI/resourcesAPIResponseMultipleResourcesWithExtraFields.json")
	public void validateMetaWithExtraFields() {
		setJwt(false);
		environment.mapKey(ValidateMetaOnlyRequestDateTime.RESPONSE_ENV_KEY, "resource_endpoint_response_full");
		run(new ValidateMetaOnlyRequestDateTime());
	}

	@Test
	@UseResurce("jsonResponses/resourcesAPI/resourcesAPIResponseMultipleResourcesWithBadExtraFields.json")
	public void validateMetaWithBadExtraFields() {
		setJwt(false);
		environment.mapKey(ValidateMetaOnlyRequestDateTime.RESPONSE_ENV_KEY, "resource_endpoint_response_full");
		ValidateMetaOnlyRequestDateTime condition = new ValidateMetaOnlyRequestDateTime();
		ConditionError conditionError = runAndFail(condition);
		assertThat(conditionError.getMessage(), containsString(ErrorMessagesUtils.createFieldKeyNotMatchPatternMessage("extraField", condition.getApiName())));

	}

	@Test
	@UseResurce("jsonResponses/resourcesAPI/resourcesAPIResponseMultipleResources.json")
	public void validateStructureJwt() {
		setJwt(true);
		environment.mapKey(ValidateMetaOnlyRequestDateTime.RESPONSE_ENV_KEY, "resource_endpoint_response_full");
		run(new ValidateMetaOnlyRequestDateTime());
	}

	@Test
	@UseResurce("jsonResponses/metaData/goodConsentResponseWithoutSelfOrMeta.json")
	public void validateEmptyOptionalMeta() {
		setJwt(false);
		ValidateMetaOnlyRequestDateTime condition = new ValidateMetaOnlyRequestDateTime();
		environment.mapKey(ValidateMetaOnlyRequestDateTime.RESPONSE_ENV_KEY, "resource_endpoint_response_full");
		environment.putBoolean(ValidateMetaOnlyRequestDateTime.IS_META_OPTIONAL, true);
		run(condition);

	}

	@Test
	@UseResurce("jsonResponses/metaData/goodConsentResponseWithoutSelfOrMeta.json")
	public void validateEmptyMeta() {
		setJwt(false);
		ValidateMetaOnlyRequestDateTime condition = new ValidateMetaOnlyRequestDateTime();
		environment.mapKey(ValidateMetaOnlyRequestDateTime.RESPONSE_ENV_KEY, "resource_endpoint_response_full");
		ConditionError conditionError = runAndFail(condition);
		assertThat(conditionError.getMessage(), containsString("Unable to find element meta on the ValidateMetaOnlyRequestDateTime API response"));
	}

	@Test
	@UseResurce("jsonResponses/metaData/goodMetaRecordsPagesOnly.json")
	public void validateGoodMetaWithoutTime() {
		setJwt(false);
		environment.mapKey(ValidateMetaOnlyRequestDateTime.RESPONSE_ENV_KEY, "resource_endpoint_response_full");
		ValidateMetaOnlyRequestDateTime condition = new ValidateMetaOnlyRequestDateTime();
		ConditionError conditionError = runAndFail(condition);
		assertThat(conditionError.getMessage(), containsString("Unable to find element requestDateTime on the ValidateMetaOnlyRequestDateTime API response"));
	}


	@Test
	@UseResurce("jsonResponses/metaData/badMetaWrongRequestDateTime.json")
	public void validateBadMetaWithWrongTime() {
		setJwt(false);
		environment.mapKey(ValidateMetaOnlyRequestDateTime.RESPONSE_ENV_KEY, "resource_endpoint_response_full");
		ValidateMetaOnlyRequestDateTime condition = new ValidateMetaOnlyRequestDateTime();
		ConditionError conditionError = runAndFail(condition);
		assertThat(conditionError.getMessage(), containsString(ErrorMessagesUtils.createFieldValueNotMatchPatternMessage("requestDateTime", condition.getApiName())));
	}


}

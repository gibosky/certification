package net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.condition.client.jsonAsserting.ErrorMessagesUtils;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.validators.PaymentInitiationPixPaymentsValidatorV2;
import net.openid.conformance.util.UseResurce;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.core.StringContains.containsString;
import static org.junit.Assert.assertThat;

public class PaymentInitiationPixPaymentsValidatorV2Test extends AbstractJsonResponseConditionUnitTest {

	private static final String END_TO_END_ID = "E9040088820210128000800123873170";

	@Before
	public void init() {
		environment.putString("endToEndId", END_TO_END_ID);

		JsonObject request = new JsonObject();
		environment.putObject("resource_request_entity_claims", request);
		JsonObject data = new JsonObject();
		request.add("data", data);
		data.addProperty("endToEndId", END_TO_END_ID);
	}

	@Test
	@UseResurce("jsonResponses/paymentInitiation/pixByPayments/v2/paymentInitiationResponseOk.json")
	public void validateStructure() {
		PaymentInitiationPixPaymentsValidatorV2 condition = new PaymentInitiationPixPaymentsValidatorV2();
		run(condition);
	}


	@Test
	@UseResurce("jsonResponses/paymentInitiation/pixByPayments/v2/paymentInitiationResponseOkWithGoodExtraFields.json")
	public void validateGoodExtraFields() {
		PaymentInitiationPixPaymentsValidatorV2 condition = new PaymentInitiationPixPaymentsValidatorV2();
		run(condition);
	}

	@Test
	@UseResurce("jsonResponses/paymentInitiation/pixByPayments/v2/paymentInitiationResponseOkWithBadExtraFields.json")
	public void validateBadExtraFields() {
		PaymentInitiationPixPaymentsValidatorV2 condition = new PaymentInitiationPixPaymentsValidatorV2();
		ConditionError error = runAndFail(condition);
		assertThat(error.getMessage(), containsString(ErrorMessagesUtils.createFieldKeyNotMatchPatternMessage("extraField", condition.getApiName())));
	}


	@Test
	@UseResurce("jsonResponses/paymentInitiation/pixByPayments/v1/paymentInitiationConsentResponsePixMissingEndToEndId.json")
	public void validateStructureMissingEndToEndId() {
		PaymentInitiationPixPaymentsValidatorV2 condition = new PaymentInitiationPixPaymentsValidatorV2();
		ConditionError error = runAndFail(condition);
		assertThat(error.getMessage(), containsString(ErrorMessagesUtils.createElementNotFoundMessage("endToEndId", condition.getApiName())));
	}

	@Test
	@UseResurce("jsonResponses/paymentInitiation/pixByPayments/v1/paymentInitiationConsentResponsePixBadEndToEndId.json")
	public void validateStructureBadEndToEndId() {
		PaymentInitiationPixPaymentsValidatorV2 condition = new PaymentInitiationPixPaymentsValidatorV2();
		ConditionError error = runAndFail(condition);
		assertThat(error.getMessage(), containsString(ErrorMessagesUtils.createFieldValueNotMatchPatternMessage("endToEndId", condition.getApiName())));
	}

	@Test
	@UseResurce("jsonResponses/paymentInitiation/pixByPayments/v2/paymentInitiationResponseDifferentEndToEndId.json")
	public void validateStructureDifferentEndToEndId() {
		PaymentInitiationPixPaymentsValidatorV2 condition = new PaymentInitiationPixPaymentsValidatorV2();
		ConditionError error = runAndFail(condition);
		assertThat(error.getMessage(), containsString("Response endToEndId is not to equal to the request endToEndId"));
	}

	@Test
	@UseResurce("jsonResponses/paymentInitiation/pixByPayments/v2/paymentInitiationResponseDictNoProxy.json")
	public void validateStructureDictNoProxy() {
		PaymentInitiationPixPaymentsValidatorV2 condition = new PaymentInitiationPixPaymentsValidatorV2();
		ConditionError error = runAndFail(condition);
		assertThat(error.getMessage(), containsString(ErrorMessagesUtils.createElementNotFoundMessage("proxy", condition.getApiName())));
	}

	@Test
	@UseResurce("jsonResponses/paymentInitiation/pixByPayments/v2/paymentInitiationResponseManuWithProxy.json")
	public void validateStructureManuWithProxy() {
		PaymentInitiationPixPaymentsValidatorV2 condition = new PaymentInitiationPixPaymentsValidatorV2();
		ConditionError error = runAndFail(condition);
		assertThat(error.getMessage(), containsString(ErrorMessagesUtils.createMustNotBePresentMessage("proxy", condition.getApiName())));
	}


	@Test
	@UseResurce("jsonResponses/paymentInitiation/pixByPayments/v2/paymentInitiationResponsePdngWithRejectionReason.json")
	public void validateStructurePdngWithRejectionReason() {
		PaymentInitiationPixPaymentsValidatorV2 condition = new PaymentInitiationPixPaymentsValidatorV2();
		ConditionError error = runAndFail(condition);
		assertThat(error.getMessage(), containsString(ErrorMessagesUtils.createMustNotBePresentMessage("rejectionReason", condition.getApiName())));
	}

	@Test
	@UseResurce("jsonResponses/paymentInitiation/pixByPayments/v2/paymentInitiationResponseRjctWithRejectionReason.json")
	public void validateStructureRjctWithRejectionReason() {
		PaymentInitiationPixPaymentsValidatorV2 condition = new PaymentInitiationPixPaymentsValidatorV2();
		run(condition);
	}

	@Test
	@UseResurce("jsonResponses/paymentInitiation/pixByPayments/v2/paymentInitiationResponseRjctWithoutRejectionReason.json")
	public void validateStructureRjctWithoutRejectionReason() {
		PaymentInitiationPixPaymentsValidatorV2 condition = new PaymentInitiationPixPaymentsValidatorV2();
		ConditionError error = runAndFail(condition);
		assertThat(error.getMessage(), containsString(ErrorMessagesUtils.createElementNotFoundMessage("rejectionReason", condition.getApiName())));
	}

	@Test
	@UseResurce("jsonResponses/paymentInitiation/pixByPayments/v2/paymentInitiationResponseWithoutTransactionId.json")
	public void validateStructureWithoutTransactionId() {
		PaymentInitiationPixPaymentsValidatorV2 condition = new PaymentInitiationPixPaymentsValidatorV2();
		run(condition);
	}

	@Test
	@UseResurce("jsonResponses/paymentInitiation/pixByPayments/v2/paymentInitiationResponseWithDifferentTransactionId.json")
	public void validateStructureWithWrongTransactionId() {
		PaymentInitiationPixPaymentsValidatorV2 condition = new PaymentInitiationPixPaymentsValidatorV2();
		environment.getElementFromObject("resource_request_entity_claims", "data").getAsJsonObject().addProperty("transactionIdentification", "E00038166201907261559y6j6");
		ConditionError error = runAndFail(condition);
		assertThat(error.getMessage(), containsString("Response transactionIdentification is not to equal to the request transactionIdentification"));
	}



}

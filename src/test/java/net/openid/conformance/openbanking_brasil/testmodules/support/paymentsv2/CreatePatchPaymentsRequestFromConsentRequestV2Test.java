package net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.logging.TestInstanceEventLog;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;
import net.openid.conformance.util.JsonObjectBuilder;
import org.junit.Test;

import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.mock;

public class CreatePatchPaymentsRequestFromConsentRequestV2Test {


	@Test
	public void test() {
		Environment env = new Environment();
		CreatePatchPaymentsRequestFromConsentRequestV2 cond = new CreatePatchPaymentsRequestFromConsentRequestV2();
		TestInstanceEventLog eventLog = mock(TestInstanceEventLog.class);
		cond.setProperties("UNIT-TEST", eventLog, Condition.ConditionResult.INFO);
		String identificationString = "123456789";
		JsonObject consentRequest = new JsonObjectBuilder()
			.addFields("brazilPaymentConsent.data.loggedUser.document", Map.of("identification", identificationString))
			.build();

		env.putObject("resource", consentRequest);

		try {
			cond.execute(env);

			JsonElement patchRequest = env.getElementFromObject("resource","brazilPatchPixPayment");
			assertNotNull(patchRequest);
			JsonObject data = patchRequest.getAsJsonObject().getAsJsonObject("data");
			assertNotNull(data);
			JsonElement status = data.get("status");
			assertNotNull(status);
			assertEquals("CANC", OIDFJSON.getString(status));
			JsonObject cancellation = data.getAsJsonObject("cancellation");
			assertNotNull(cancellation);
			JsonObject cancelledBy = cancellation.getAsJsonObject("cancelledBy");
			assertNotNull(cancelledBy);
			JsonObject document = cancelledBy.getAsJsonObject("document");
			assertNotNull(document);
			JsonElement identification = document.get("identification");
			assertNotNull(identification);
			assertEquals(identificationString, OIDFJSON.getString(identification));
			JsonElement rel = document.get("rel");
			assertNotNull(rel);
			assertEquals("CPF", OIDFJSON.getString(rel));

		} catch (ConditionError error) {
			throw new AssertionError("Condition failed", error);
		}


	}

}

package net.openid.conformance.raidiam.validators.organisationCertificates;

import net.openid.conformance.logging.ApiName;

/**
 * Api endpoint: GET /organisations/{OrganisationId}/certificates/kid/{CertificateOrKeyId}
 *
 */
@ApiName("Raidiam Directory GET Organisation Certificates By CertificateOrKeyId")
public class GetCertificatesByCertificateOrKeyIdValidator extends PostCertificatesByCertificateTypeValidator {
}

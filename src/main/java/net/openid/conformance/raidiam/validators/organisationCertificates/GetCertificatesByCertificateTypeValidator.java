package net.openid.conformance.raidiam.validators.organisationCertificates;

import net.openid.conformance.logging.ApiName;

/**
 * Api endpoint: GET /organisations/{OrganisationId}/certificates/{OrganisationCertificateType}
 *
 */
@ApiName("Raidiam Directory GET Organisation Certificates By CertificateType")
public class GetCertificatesByCertificateTypeValidator extends GetCertificatesValidator {
}

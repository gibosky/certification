package net.openid.conformance.raidiam.validators.contacts;

import net.openid.conformance.logging.ApiName;

/**
 * Api endpoint: GET /organisations/{OrganisationId}/contacts/{ContactId}
 */
@ApiName("Raidiam Directory GET Contacts By ContactId")
public class GetContactsByContactIdValidator extends PostContactsValidator {
}

package net.openid.conformance.raidiam.validators.contacts;

import net.openid.conformance.logging.ApiName;

/**
 * Api endpoint: PUT /organisations/{OrganisationId}/contacts/{ContactId}
 */
@ApiName("Raidiam Directory PUT Contacts by ContactId")
public class PutContactsByContactIdValidator extends PostContactsValidator {
}

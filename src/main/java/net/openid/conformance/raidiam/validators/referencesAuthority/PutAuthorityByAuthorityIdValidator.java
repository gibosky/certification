package net.openid.conformance.raidiam.validators.referencesAuthority;

import net.openid.conformance.logging.ApiName;

/**
 * This class corresponds to {@link PostAuthorityValidator}
 * Api url: ****
 * Api endpoint: PUT /references/authorities/{AuthorityId}
 * Api git hash: ****
 *
 */
@ApiName("Raidiam Directory PUT References - Authority by AuthorityId")
public class PutAuthorityByAuthorityIdValidator extends PostAuthorityValidator {
}

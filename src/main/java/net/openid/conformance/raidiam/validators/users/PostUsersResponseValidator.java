package net.openid.conformance.raidiam.validators.users;


import net.openid.conformance.logging.ApiName;

/**
 * This class corresponds to {@link GetUsersResponseValidator}
 * Api url: ****
 * Api endpoint: /users
 * Api git hash: ****
 *
 */

@ApiName("Raidiam Directory Post Users")
public class PostUsersResponseValidator extends GetUsersResponseValidator {
}

package net.openid.conformance.raidiam.validators.authorisationServers.resourcesAPI;

import net.openid.conformance.logging.ApiName;

/**
 * Api endpoint: PUT /organisations/{OrganisationId}/authorisationservers/{AuthorisationServerId}
 * /apiresources/{ApiResourceId}
 */
@ApiName("Raidiam Directory PUT Authorisation Servers API Resources by ResourceId")
public class PutResourceByResourceIdValidator extends PostResourceValidator {
}

package net.openid.conformance.condition.client.jsonAsserting;

import com.google.gson.JsonElement;

import java.util.HashMap;
import java.util.Map;

public class JsonAssertingResponseBodyParser {


	public Map<String, JsonElement> parseBody(String path, JsonElement element) {
		Map<String, JsonElement> elements = new HashMap<>();

		if (element.isJsonObject()) {
			element.getAsJsonObject().entrySet()
				.forEach(entry -> {
					JsonElement el = entry.getValue();
					String fullPath = path + '.' + entry.getKey();
					if (element.isJsonObject()) {
						elements.putAll(parseBody(fullPath, el));
					}
				});
		}

		elements.put(path, element);

		return elements;
	}
}


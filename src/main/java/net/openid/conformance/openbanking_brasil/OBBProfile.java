package net.openid.conformance.openbanking_brasil;

public interface OBBProfile {

	String OBB_PROFILE = "Open Finance Brasil Functional Tests";
	String OBB_PROFIlE_PHASE1 = "Open Finance Brasil Functional Tests for Phase 1 - Open Data";
	String OBB_PROFIlE_PHASE2 = "Open Finance Brasil Functional Tests for Phase 2 - Customer Data - API Version 1";
	String OBB_PROFIlE_PHASE2_VERSION2 = "Open Finance Brasil Functional Tests for Phase 2 - Customer Data - API Version 2";
	String OBB_PROFIlE_PHASE3 = "Open Finance Brasil Functional Tests for Phase 3 - Payment Initiation";
	String OBB_PROFIlE_PHASE4 = "Open Finance Brasil Functional Tests for Phase 4";
	String OBB_PROFILE_DCR = "Open Finance Brasil Functional DCR Tests";

	String OBB_PROFILE_PROD_FVP = "Open Finance Brasil Functional Production Tests - FVP";

	String OBB_PROFILE_OPEN_INSURANCE_PHASE1 = "Open Insurance Brasil Functional Tests for Phase 1 - Open Data";
	String OBB_PROFILE_OPEN_INSURANCE_PHASE2 = "Open Insurance Brasil Functional Tests for Phase 2 - Customer Data";

	String OBB_PROFILE_OPIN_DCR = "Open Insurance Brasil Functional DCR Tests";

	/** test plans using this profile will only be visible on dev.conformance, and not on web.conformance */
	String DEV_ONLY = "Development only functional tests";

}

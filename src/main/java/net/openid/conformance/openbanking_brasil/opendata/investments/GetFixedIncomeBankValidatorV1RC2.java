package net.openid.conformance.openbanking_brasil.opendata.investments;

import com.google.common.collect.Sets;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.logging.ApiName;
import net.openid.conformance.openbanking_brasil.opendata.OpenDataLinksAndMetaValidator;
import net.openid.conformance.openbanking_brasil.productsNServices.ProductNServicesCommonFields;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.util.SetUtils;
import net.openid.conformance.util.field.ObjectArrayField;
import net.openid.conformance.util.field.ObjectField;
import net.openid.conformance.util.field.StringField;

import java.util.Set;

/**
 * Api url: https://openbanking-brasil.github.io/openapi/swagger-apis/investments/1.0.0-rc2.0.yml
 * Api endpoint: /bank-fixed-incomes
 * Git hash:
 */

@ApiName("Investments Bank Fixed Incomes V1RC2")
public class GetFixedIncomeBankValidatorV1RC2 extends AbstractJsonAssertingCondition {
	private static class Fields extends ProductNServicesCommonFields {
	}
	private final OpenDataLinksAndMetaValidator linksAndMetaValidator = new OpenDataLinksAndMetaValidator(this);

	private static final Set<String> PRODUCT_TYPE = Sets.newHashSet("CDB", "RDB", "LCI", "LCA");
	private static final Set<String> REDEMPTION_TERM = Sets.newHashSet("DIARIA", "DATA_VENCIMENTO", "DIARIA_PRAZO_CARENCIA");
	private static final Set<String> INDEXER = SetUtils.createSet("CDI, DI, TR, IPCA, IGP_M, IGP_DI, INPC, BCP, TLC, SELIC, PRE_FIXADO, OUTROS");
	private static final Set<String> INTERVAL = Sets.newHashSet("1_FAIXA", "2_FAIXA", "3_FAIXA", "4_FAIXA");
	private static final Set<String> GRACE_PERIOD = SetUtils.createSet("1_360, 361_1080, 1081+");
	private static final Set<String> TARGET_AUDIENCE = SetUtils.createSet("PESSOA_NATURAL, PESSOA_JURIDICA");


	@Override
	@PreEnvironment(strings = "resource_endpoint_response")
	public Environment evaluate(Environment environment) {
		JsonElement body = bodyFrom(environment);

		assertField(body,
			new ObjectArrayField
				.Builder("data")
				.setValidator(this::assertData)
				.mustNotBeEmpty()
				.build());

		linksAndMetaValidator.assertMetaAndLinks(body);
		logFinalStatus();
		return environment;
	}

	private void assertData(JsonObject data) {
		assertField(data,
			new ObjectField
				.Builder("participant")
				.setValidator(this::assertParticipant)
				.build());

		assertField(data,
			new StringField
				.Builder("issuerInstitutionCnpjNumber")
				.setMaxLength(14)
				.setPattern("^\\d{14}$")
				.build());

		assertField(data,
			new StringField
				.Builder("issuerInstitutionName")
				.setMinLength(1)
				.setMaxLength(250)
				.setPattern("[\\w\\W\\s]*")
				.build());

		assertField(data,
			new StringField
				.Builder("investmentType")
				.setEnums(PRODUCT_TYPE)
				.build());

		assertField(data,
			new ObjectField
				.Builder("index")
				.setValidator(index -> {
					assertField(index,
						new StringField
							.Builder("indexer")
							.setEnums(INDEXER)
							.build());

					assertField(index,
						new StringField
							.Builder("indexerAdditionalInfo")
							.setMaxLength(140)
							.setPattern("[\\w\\W\\s]*")
							.setOptional()
							.build());

					assertField(index,
						new ObjectField
							.Builder("issueRemunerationRate")
							.setValidator(this::assertIssueRemunerationRate)
							.build());
				})
				.build());

		assertField(data,
			new ObjectField
				.Builder("investmentConditions")
				.setValidator(this::assertInvestmentConditions)
				.build());

		assertField(data,
			new StringField
				.Builder("targetAudience")
				.setEnums(TARGET_AUDIENCE)
				.build());
	}

	private void assertInvestmentConditions(JsonObject investmentConditions) {
		assertField(investmentConditions,
			new StringField
				.Builder("minimumAmount")
				.setMinLength(4)
				.setMaxLength(19)
				.setPattern("^\\d{1,16}\\.\\d{2}$")
				.build());

		assertField(investmentConditions,
			new StringField.
				Builder("redemptionTerm")
				.setEnums(REDEMPTION_TERM)
				.build());

		assertField(investmentConditions,
			new StringField.
				Builder("expirationPeriod")
				.setEnums(GRACE_PERIOD)
				.build());

		assertField(investmentConditions,
			new StringField.
				Builder("gracePeriod")
				.setEnums(GRACE_PERIOD)
				.build());
	}

	private void assertIssueRemunerationRate(JsonObject issueRemunerationRate) {
		assertField(issueRemunerationRate,
			new ObjectArrayField
				.Builder("prices")
				.setValidator(this::assertPrices)
				.setMinItems(4)
				.setMaxItems(4)
				.build());

		assertField(issueRemunerationRate,
			new StringField
				.Builder("minimum")
				.setMinLength(8)
				.setMaxLength(8)
				.setPattern("^\\d{1}\\.\\d{6}$")
				.build());

		assertField(issueRemunerationRate,
			new StringField
				.Builder("maximum")
				.setMinLength(8)
				.setMaxLength(8)
				.setPattern("^\\d{1}\\.\\d{6}$")
				.build());
	}

	private void assertPrices(JsonObject prices) {
		assertField(prices,
			new StringField.
				Builder("interval")
				.setEnums(INTERVAL)
				.build());

		assertField(prices,
			new StringField.
				Builder("value")
				.setMinLength(8)
				.setMaxLength(8)
				.setPattern("^\\d{1}\\.\\d{6}$")
				.build());

		assertField(prices,
			new StringField.
				Builder("operationRate")
				.setMinLength(8)
				.setMaxLength(8)
				.setPattern("^\\d{1}\\.\\d{6}$")
				.build());
	}

	private void assertParticipant(JsonObject participantIdentification) {
		assertField(participantIdentification,
			new StringField
				.Builder("brand")
				.setMaxLength(80)
				.build());

		assertField(participantIdentification, Fields.name().setMaxLength(80).build());
		assertField(participantIdentification, Fields.cnpjNumber().setMaxLength(14).setPattern("^\\d{14}$").build());

		assertField(participantIdentification,
			new StringField
				.Builder("urlComplementaryList")
				.setMaxLength(1024)
				.setPattern("^(https?:\\/\\/)?(www\\.)?[-a-zA-Z0-9@:%._\\+~#=]{2,256}\\.[a-z]{2,6}\\b([-a-zA-Z0-9@:%_\\+.~#?&\\/\\/=]*)$")
				.setOptional()
				.build());
	}
}

package net.openid.conformance.openbanking_brasil.opendata.acquiringServices;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.logging.ApiName;
import net.openid.conformance.openbanking_brasil.opendata.OpenDataLinksAndMetaValidator;
import net.openid.conformance.openbanking_brasil.productsNServices.ProductNServicesCommonFields;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.util.SetUtils;
import net.openid.conformance.util.field.ObjectArrayField;
import net.openid.conformance.util.field.ObjectField;
import net.openid.conformance.util.field.StringField;

import java.util.Set;

/**
 * Api url: https://openbanking-brasil.github.io/openapi/swagger-apis/acquiring-services/1.0.0-rc2.0.yml
 * Api endpoint: /businesses
 * Api version: 1.0.0-rc2.0
 * Git hash:
 */

@ApiName("Business Acquiring Services V1RC2")
public class BusinessAcquiringServicesValidatorV1RC2 extends AbstractJsonAssertingCondition {
	private static class Fields extends ProductNServicesCommonFields { }
	private final OpenDataLinksAndMetaValidator linksAndMetaValidator = new OpenDataLinksAndMetaValidator(this);
	public static final Set<String> FEE_NAME = SetUtils.createSet("TAXA_DESCONTO_MODALIDADE_CREDITO, TAXA_DESCONTO_MODALIDADE_DEBITO");
	public static final Set<String> CODE = SetUtils.createSet("MDR_CREDITO, MDR_DEBITO");
	public static final Set<String> INTERVAL = SetUtils.createSet("1_FAIXA, 2_FAIXA, 3_FAIXA, 4_FAIXA");

	@Override
	@PreEnvironment(strings = "resource_endpoint_response")
	public Environment evaluate(Environment environment) {
		JsonElement body = bodyFrom(environment);

		assertField(body,
			new ObjectArrayField.Builder("data")
				.setValidator(this::assertData)
				.mustNotBeEmpty()
				.build());

		linksAndMetaValidator.assertMetaAndLinks(body);
		logFinalStatus();
		return environment;
	}

	private void assertData(JsonObject data) {
		assertField(data,
			new ObjectField.Builder("participant")
				.setValidator(this::assertParticipantIdentification)
				.build());

		assertField(data,
			new StringField
				.Builder("feeName")
				.setMaxLength(38)
				.setEnums(FEE_NAME)
				.build());

		assertField(data,
			new StringField.
				Builder("code")
				.setEnums(CODE)
				.build());

		assertField(data,
			new ObjectArrayField
				.Builder("prices")
				.setValidator(this::assertPrices)
				.setMinItems(4)
				.setMaxItems(4)
				.build());

		assertField(data,
			new StringField.
				Builder("chargingTriggerInfo")
				.setMinLength(1)
				.setMaxLength(200)
				.build());

		assertField(data,
			new StringField.
				Builder("minimum")
				.setMinLength(8)
				.setMaxLength(8)
				.setPattern("^\\d{1}\\.\\d{6}$")
				.build());

		assertField(data,
			new StringField.
				Builder("maximum")
				.setMinLength(8)
				.setMaxLength(8)
				.setPattern("^\\d{1}\\.\\d{6}$")
				.build());
	}

	private void assertPrices(JsonObject prices) {
		assertField(prices,
			new StringField.
				Builder("interval")
				.setEnums(INTERVAL)
				.build());

		assertField(prices,
			new StringField.
				Builder("value")
				.setMinLength(8)
				.setMaxLength(8)
				.setPattern("^\\d{1}\\.\\d{6}$")
				.build());

		assertField(prices,
			new StringField.
				Builder("customerRate")
				.setMinLength(8)
				.setMaxLength(8)
				.setPattern("^\\d{1}\\.\\d{6}$")
				.build());
	}

	private void assertParticipantIdentification(JsonObject participantIdentification) {
		assertField(participantIdentification,
			new StringField
				.Builder("brand")
				.setMaxLength(80)
				.build());

		assertField(participantIdentification, Fields.name().setMaxLength(80).build());
		assertField(participantIdentification, Fields.cnpjNumber().setPattern("^\\d{14}$").build());

		assertField(participantIdentification,
			new StringField
				.Builder("urlComplementaryList")
				.setMaxLength(1024)
				.setPattern("^(https?:\\/\\/)?(www\\.)?[-a-zA-Z0-9@:%._\\+~#=]{2,256}\\.[a-z]{2,6}\\b([-a-zA-Z0-9@:%_\\+.~#?&\\/\\/=]*)$")
				.setOptional()
				.build());
	}
}

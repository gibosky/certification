package net.openid.conformance.openbanking_brasil.testmodules.support;

public class OverrideClientWithPagtoClient extends AbstractOverrideClient {
	// software id is 368bceef-6096-4e1a-a702-64c07d12c98a

	@Override
	String clientCert() {
		return "-----BEGIN CERTIFICATE-----\n" +
			"MIIHEDCCBfigAwIBAgIUUiSHIbS48sptJAv8nwl++cAcLA0wDQYJKoZIhvcNAQEL\n" +
			"BQAwcTELMAkGA1UEBhMCQlIxHDAaBgNVBAoTE09wZW4gQmFua2luZyBCcmFzaWwx\n" +
			"FTATBgNVBAsTDE9wZW4gQmFua2luZzEtMCsGA1UEAxMkT3BlbiBCYW5raW5nIFNB\n" +
			"TkRCT1ggSXNzdWluZyBDQSAtIEcxMB4XDTIyMTIwODE3MTMwMFoXDTIzMTIxMTEw\n" +
			"MDAwMFowggEkMQswCQYDVQQGEwJCUjELMAkGA1UECBMCU1AxDzANBgNVBAcTBkxP\n" +
			"TkRPTjEcMBoGA1UEChMTT3BlbiBCYW5raW5nIEJyYXNpbDEtMCsGA1UECxMkNzRl\n" +
			"OTI5ZDktMzNiNi00ZDg1LThiYTctYzE0NmM4NjdhODE3MScwJQYDVQQDEx50cHAu\n" +
			"c2FuZGJveC5vcGVuYmFua2luZy5vcmcuYnIxFzAVBgNVBAUTDjQzMTQyNjY2MDAw\n" +
			"MTk3MR0wGwYDVQQPExRQcml2YXRlIE9yZ2FuaXphdGlvbjETMBEGCysGAQQBgjc8\n" +
			"AgEDEwJVSzE0MDIGCgmSJomT8ixkAQETJDM2OGJjZWVmLTYwOTYtNGUxYS1hNzAy\n" +
			"LTY0YzA3ZDEyYzk4YTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAL/J\n" +
			"yfS9WinqaUpmIypO32z488UjJmjkyUoR7bADivjUZ9b2O5zM4uwrOXD/jjMzeCU7\n" +
			"qHg7inzHkrmp1qYKDOL2k+qqRtfHvmiFTiqEyP4NbgREkZN0ZUGGsN1ESk2PPTq7\n" +
			"+KvJl7B7PGiWKH0N5MlW7wmTkvrfOIEfymXDkjvvsN+oYh2R3Yq59Zkk2fDTxTWb\n" +
			"J7ipb/rFuWqMz95bQhtg0TCiKwRPDCeng/U5skXveabXAjyOd+oNa8MaX3+f5Kmw\n" +
			"PCsbGb7hVao4RTgjOuIkPneVxgTbLZEsieQZYLswkfMJ67x11l0J1nw8eFq6LTve\n" +
			"GyjWH9H7i4qodo1onsUCAwEAAaOCAukwggLlMAwGA1UdEwEB/wQCMAAwHQYDVR0O\n" +
			"BBYEFLjQQibxxqJ/WN5iJiQ5HCpmdyrGMB8GA1UdIwQYMBaAFIZ/WK0X9YK2TrQF\n" +
			"s/uwzhFD30y+MEwGCCsGAQUFBwEBBEAwPjA8BggrBgEFBQcwAYYwaHR0cDovL29j\n" +
			"c3Auc2FuZGJveC5wa2kub3BlbmJhbmtpbmdicmFzaWwub3JnLmJyMEsGA1UdHwRE\n" +
			"MEIwQKA+oDyGOmh0dHA6Ly9jcmwuc2FuZGJveC5wa2kub3BlbmJhbmtpbmdicmFz\n" +
			"aWwub3JnLmJyL2lzc3Vlci5jcmwwKQYDVR0RBCIwIIIedHBwLnNhbmRib3gub3Bl\n" +
			"bmJhbmtpbmcub3JnLmJyMA4GA1UdDwEB/wQEAwIFoDATBgNVHSUEDDAKBggrBgEF\n" +
			"BQcDAjCCAagGA1UdIASCAZ8wggGbMIIBlwYKKwYBBAGDui9kATCCAYcwggE2Bggr\n" +
			"BgEFBQcCAjCCASgMggEkVGhpcyBDZXJ0aWZpY2F0ZSBpcyBzb2xlbHkgZm9yIHVz\n" +
			"ZSB3aXRoIFJhaWRpYW0gU2VydmljZXMgTGltaXRlZCBhbmQgb3RoZXIgcGFydGlj\n" +
			"aXBhdGluZyBvcmdhbmlzYXRpb25zIHVzaW5nIFJhaWRpYW0gU2VydmljZXMgTGlt\n" +
			"aXRlZHMgVHJ1c3QgRnJhbWV3b3JrIFNlcnZpY2VzLiBJdHMgcmVjZWlwdCwgcG9z\n" +
			"c2Vzc2lvbiBvciB1c2UgY29uc3RpdHV0ZXMgYWNjZXB0YW5jZSBvZiB0aGUgUmFp\n" +
			"ZGlhbSBTZXJ2aWNlcyBMdGQgQ2VydGljaWNhdGUgUG9saWN5IGFuZCByZWxhdGVk\n" +
			"IGRvY3VtZW50cyB0aGVyZWluLjBLBggrBgEFBQcCARY/aHR0cDovL3JlcG9zaXRv\n" +
			"cnkuc2FuZGJveC5wa2kub3BlbmJhbmtpbmdicmFzaWwub3JnLmJyL3BvbGljaWVz\n" +
			"MA0GCSqGSIb3DQEBCwUAA4IBAQBvs38DsnAQFbS0/gy3+2QmSbUt65/pZdBLblq0\n" +
			"862qJci68woqHC4/OCQqeN6huxrrx6LY21phADbkuptvH6w9sgh3cxvliXJMHV3G\n" +
			"bXCmgu4otIOQRJEETDw0jS4mjEc8OVSrkJSqIcCCwGgNkL/lU+LTtqGfdoM9/AcZ\n" +
			"W6CfwC2NDx3YzKc/4hAAhBIG/ld/bi1L1TLXALL3PQZlAff17KbXNV8XnUlrleqS\n" +
			"xaVgjMrJkIzT4vEQtEEBDNWaPJD9HRoJZs3oxejI2dOQDWTxkyQCI+9r3UcG5EzA\n" +
			"LoCzpMJvOCmSZNhMvS14SVb5mxUAL5NQrrE5jrZMjaU0RlFi\n" +
			"-----END CERTIFICATE-----";
	}
	@Override
	String clientKey() {
		return "-----BEGIN PRIVATE KEY-----\n" +
			"MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQC/ycn0vVop6mlK\n" +
			"ZiMqTt9s+PPFIyZo5MlKEe2wA4r41GfW9juczOLsKzlw/44zM3glO6h4O4p8x5K5\n" +
			"qdamCgzi9pPqqkbXx75ohU4qhMj+DW4ERJGTdGVBhrDdREpNjz06u/iryZewezxo\n" +
			"lih9DeTJVu8Jk5L63ziBH8plw5I777DfqGIdkd2KufWZJNnw08U1mye4qW/6xblq\n" +
			"jM/eW0IbYNEwoisETwwnp4P1ObJF73mm1wI8jnfqDWvDGl9/n+SpsDwrGxm+4VWq\n" +
			"OEU4IzriJD53lcYE2y2RLInkGWC7MJHzCeu8ddZdCdZ8PHhaui073hso1h/R+4uK\n" +
			"qHaNaJ7FAgMBAAECggEAVb2t1wdsLr8IcWV2JSz7v+kQejq7qjtWLhIT1BPRZmr3\n" +
			"PWf5wKDsdi7g0D6/4O4KsYKZT582y5AhbXWba8GkfzdDa67SWxeGbY3jIykX/lQc\n" +
			"sjXBa02nEwNXQKSOH1yBQCfXBV3rvc9qc0tmMHospUKDPSHHnPZ8eWWwA4zKbJZ0\n" +
			"yKWQe5OYe6CRJbMWVBMk9YEy6Z5/DjJmGaTr/YZPcQgFD9ggPVDZXL9zmPcEl8ys\n" +
			"qEHjKeeEuxvZgAvSxtNCPtU5TENti/QQFPgg9OUyZQL+77x0BHnGx9kuzkbTVjqy\n" +
			"2jvyCENE3J/FbjB1z/fuqJiPkrg2Ibk3DWvbokU05QKBgQD+3jc9SnmKYHzaiI4q\n" +
			"Stdm0epgYiBnptB7C6McUufFSfoInx3QY2OzRnofm6CnyilAd4bQm5oBedpdlYTb\n" +
			"gnm0G2hwxt1UPVNlj5v69vcl+yCY3G1TSy3KnSYRbXl+sHaFG3AjAuUP52my2r8C\n" +
			"l2kxjKioHD5dXGVl8ULraEwbPwKBgQDAo9oAXtnDi3Xd0kMRaS1rtVvLSfnWJHc4\n" +
			"V2y28JFM2te7eNuvQZMwSegXhHwdbZCWr3Yeop4qYbCrSbdJneALXkFK63uS0QaB\n" +
			"oHDh/59D/KmQUK0YIrHx/UdSxArBNLXLVnZVboZ2wcary94vIk3OUsyQIAkfCKB5\n" +
			"/3XNtMQY+wKBgQDo8zjwJ2sRpIFZyOiQFVnuG72qnOXbd2gknw1V7Rz0gVosmSrJ\n" +
			"9p4BFVC4JSnhUvEDgHxTnXVVFggV83y8mfQdP9PlCI7x2R+pu2opJ6PcLj2sGgU+\n" +
			"1X8kKyDJWxNqTWX24Y5MIfA16iD9HdzqRHQcmuDozu/dq37uf2bX/btaRwKBgDXq\n" +
			"5m5qHdssYn4Ghr0Nyie2aA3bE7FLHY9IcOY3KQPw0KbpKdvAp9jBJtPGQhLegrSa\n" +
			"QZ5Ld6d3FkKq7EEPjhDAfdfhVwH9vSt044Ntz37w7ei1m+0AcPvBH0BcHIJ9JVDX\n" +
			"T2GUANCmZdvZwMNCf7J63ESOot7rxDbBOJCtwqEXAoGANsHwZWpjxeaTlFxYMwyF\n" +
			"liKk29OmaCDhR4xJyIs3o6E9Bvkr0Fn9XNi3QOl7r/KdUIxy5KNF3lVTad/XOtI7\n" +
			"AICxXWUaATER9Cu1z7xhG2GsWJWkvALB9bKFFThLbQBnUlSFXU7LHqMrEKGKsA3Q\n" +
			"5hJCLPTp7VNEUqcNVaNICDE=\n" +
			"-----END PRIVATE KEY-----\n";
	}

	@Override
	String clientJwks() {
		// there's no separate key for this client, it uses the org jwks for auth
		return orgJwks;
	}

	@Override
	String role() {
		return "PAGTO";
	}

	@Override
	String directoryClientId() {
		return "019NDXYT8Nn0T0ger0Rey";
	}
}

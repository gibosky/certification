package net.openid.conformance.openbanking_brasil.testmodules.v2.payments;

import net.openid.conformance.condition.client.*;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.EnsureProxyPresentInConfig;
import net.openid.conformance.openbanking_brasil.testmodules.support.*;
import net.openid.conformance.openbanking_brasil.testmodules.support.payments.AddJWTAcceptHeaderRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.payments.AddJsonAcceptHeaderRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.payments.AddNoAcceptHeaderRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.payments.FAPIBrazilCallPaymentConsentEndpointWithBearerTokenNoAcceptField;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.validators.*;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.sequence.client.OpenBankingBrazilPreAuthorizationSteps;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "payments-contenttype-jwt-refreshtoken-v2",
	displayName = "Payments API v2 test module for refresh token rotation and accept header",
	summary = "This test is a PIX Payments current date test that aims to test both the refresh token rotation expected behavior and also how the instituion must handles the accept header when it's not sent as application/jwt\n" +
		"\u2022 Call POST token endpoint using client-credentials grant, however request the scopes=\"payments\" and \"openid\"\n" +
		"\u2022 Expect 201 - Created - Make sure token has been issued with only payments scope, ignoring the openid additional scope, and returning the scope optional object in line with https://www.rfc-editor.org/rfc/rfc6749#section-3.3\n" +
		"\u2022 Create consent with valid e-mail payload, localInstrument to be set to DICT\n" +
		"\u2022 Call the POST Consents endpoints, set header \"accept\": \"*/*\"\n" +
		"\u2022 Expects 201 - Make sure a JWT is returned - Validate response \n" +
		"\u2022 Redirects the user to authorize the created consent\n" +
		"\u2022 Call GET Consent\n" +
		"\u2022 Expects 200 - Validate if status is \"AUTHORISED\" and validate response\n" +
		"\u2022 Calls the POST Payments Endpoint, set header \"accept\": \"*/*\"\n" +
		"\u2022 Expects 201 - Make sure a JWT is returned - Validate response \n" +
		"\u2022 Call the GET Payments endpoint, set header \"accept\": \"*/*\"\n" +
		"\u2022 Expects either a 200 - make sure response is JWT\n" +
		"\u2022 Call the GET Payments endpoint, set header \"accept\": \"application/json\"\n" +
		"\u2022 Expects either a 200 or a 406 response - If 200 make sure response is JWT\n" +
		"\u2022 Call the GET Consents endpoint, set header \"accept\": \"*/*\"\n" +
		"\u2022 Expects either a 200 - make sure response is JWT - Make sure that the self link returned is exactly equal to the requested URI\n" +
		"\u2022 Call the GET Consents endpoint, set header \"accept\": \"application/json\"\n" +
		"\u2022 Expects either a 200 or a 406 response - If 200 make sure response is JWT",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"resource.consentUrl",
		"resource.loggedUserIdentification",
		"resource.businessEntityIdentification",
		"resource.debtorAccountIspb",
		"resource.debtorAccountIssuer",
		"resource.debtorAccountNumber",
		"resource.debtorAccountType",
		"resource.paymentAmount"
	}
)
public class PaymentsApiContentTypeJwtRefreshTokenTestModuleV2 extends AbstractOBBrasilQrCodePaymentFunctionalTestModuleV2 {

	@Override
	protected void configureDictInfo() {
		callAndStopOnFailure(SelectDICTCodeLocalInstrument.class);
		callAndStopOnFailure(SelectDICTCodePixLocalInstrument.class);
		callAndStopOnFailure(RemoveQRCodeFromConfig.class);
		callAndStopOnFailure(EnsureProxyPresentInConfig.class);
	}

	@Override
	protected void validateClientConfiguration() {
		callAndStopOnFailure(SetScopeInClientConfigurationToOpenId.class);
		super.validateClientConfiguration();
	}

	@Override
	protected ConditionSequence createOBBPreauthSteps() {
		ConditionSequence preAuthSteps = new OpenBankingBrazilPreAuthorizationSteps(false, false, addTokenEndpointClientAuthentication, true, false, false)
			.replace(SetPaymentsScopeOnTokenEndpointRequest.class, condition(AddScopeToTokenEndpointRequest.class))
			.insertAfter(CheckIfTokenEndpointResponseError.class, condition(EnsureTokenEndpointResponseScopeWasPayments.class).requirements("RFC6749-3.3").dontStopOnFailure())
			.replace(FAPIBrazilCallPaymentConsentEndpointWithBearerToken.class, condition(FAPIBrazilCallPaymentConsentEndpointWithBearerTokenNoAcceptField.class));
		return preAuthSteps;
	}

	@Override
	protected void requestProtectedResource() {
		if (!validationStarted) {
			validationStarted = true;
			eventLog.startBlock("Calling POST Payments endpoint - */* accept header - expects 201 with JWT response");
			ConditionSequence pixSequence = getPixPaymentSequence()
				.replace(SetApplicationJwtAcceptHeaderForResourceEndpointRequest.class, condition(AddNoAcceptHeaderRequest.class));
			call(pixSequence);
			eventLog.startBlock(currentClientString() + "Validate response");
			env.mapKey(ValidateMetaOnlyRequestDateTime.RESPONSE_ENV_KEY, "resource_endpoint_response_full");
			env.putBoolean(ValidateMetaOnlyRequestDateTime.IS_META_OPTIONAL, false);
			call(getPaymentValidationSequence());

			env.mapKey("access_token", "saved_client_credentials");

			eventLog.startBlock("Calling GET Payments endpoint - */* accept header - expects 200 with JWT response");
			ConditionSequence getPaymentNoAcceptSequence = new CallGetPaymentEndpointSequence()
				.replace(AddJWTAcceptHeaderRequest.class, condition(AddNoAcceptHeaderRequest.class));
			call(getPaymentNoAcceptSequence);

			eventLog.startBlock("Calling GET Payments endpoint - application/json accept header - expects 200 with JWT response or 406");
			ConditionSequence getPaymentJsonAcceptSequence = new CallGetPaymentEndpointSequenceErrorAgnostic()
				.replace(AddJWTAcceptHeaderRequest.class, condition(AddJsonAcceptHeaderRequest.class));
			call(getPaymentJsonAcceptSequence);

			eventLog.startBlock("Calling GET Consents endpoint - */* accept header - expects 200 with JWT response");
			ConditionSequence getConsentNoAcceptSequence = new CallGetPaymentConsentEndpointSequence()
				.replace(AddJWTAcceptHeaderRequest.class, condition(AddNoAcceptHeaderRequest.class));
			call(getConsentNoAcceptSequence);

			eventLog.startBlock("Calling GET Consents endpoint - application/json accept header - expects 200 with JWT response or 406");
			ConditionSequence getConsentJsonAcceptSequence = new CallGetPaymentConsentEndpointSequenceErrorAgnostic()
				.replace(AddJWTAcceptHeaderRequest.class, condition(AddJsonAcceptHeaderRequest.class));
			call(getConsentJsonAcceptSequence);

			eventLog.endBlock();
		}
	}
}

package net.openid.conformance.openbanking_brasil.testmodules.support.yacs;

import java.util.HashMap;
import java.util.Map;

public class ValidateConsentsAndPaymentsEndpoints extends AbstractValidateEndpointExistsForAs{
	@Override
	protected Map<String, Map<String, String>> getEndpoints() {
		Map<String, Map<String, String>> outerMap = new HashMap<>();
		Map<String, String> consentsInnerMap = new HashMap<>();
		Map<String, String> resourcesInnerMap = new HashMap<>();
		consentsInnerMap.put("endpoint", "payments/v2/consents");
		consentsInnerMap.put("version", "2.0.0");
		outerMap.put("payments-consents", consentsInnerMap);
		resourcesInnerMap.put("endpoint", "payments/v2/pix/payments");
		resourcesInnerMap.put("version", "2.0.0");
		outerMap.put("payments-pix", resourcesInnerMap);
		return outerMap;	}
}

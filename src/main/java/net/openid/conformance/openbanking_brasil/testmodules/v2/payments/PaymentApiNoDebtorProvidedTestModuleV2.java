package net.openid.conformance.openbanking_brasil.testmodules.v2.payments;

import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.support.AskForScreenshotWithAccountSelection;
import net.openid.conformance.openbanking_brasil.testmodules.support.EnforceAbsenceOfDebtorAccount;
import net.openid.conformance.openbanking_brasil.testmodules.support.SelectDICTCodeLocalInstrument;
import net.openid.conformance.openbanking_brasil.testmodules.support.payments.SanitiseQrCodeConfig;
import net.openid.conformance.testmodule.PublishTestModule;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.VariantHidesConfigurationFields;

@PublishTestModule(
	testName = "payments-api-test-no-debtor-account-v2",
	displayName = "Payments API test module to allow user to select debtor account",
	summary = "Ensure the user is asked to select a debtor account when not provided in the request.\n\n" +
		"\u2022 Calls POST Consents Endpoint with no debtor account information, using DICT as the localInstrument\n" +
		"\u2022 Expects 201 - Validate Response\n\n" +
		"\u2022 Redirects the user to authorize the created consent\n" +
		"\u2022 Call GET Consent\n" +
		"\u2022 Expects 200 - Validate if status is \"AUTHORISED\"\n" +
		"\u2022 Calls the POST Payments \n" +
		"\u2022 Expects 201 - Validate message\n" +
		"\u2022 Poll the Get Payments endpoint with the PaymentID Created while payment status is RCVD, ACCP or ACPD\n" +
		"\u2022 Expectes Definitive  state (ACSC) - Validate Response\n" +
		"A screenshot should be uploaded showing the user is presented with an option to select an account.",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"resource.consentUrl"
	}
)
@VariantHidesConfigurationFields(parameter = FAPI1FinalOPProfile.class, value = "openbanking_brazil", configurationFields = {
	"resource.brazilCpf",
	"resource.brazilCnpj"
})
public class PaymentApiNoDebtorProvidedTestModuleV2 extends AbstractOBBrasilQrCodePaymentFunctionalTestModuleV2 {

	@Override
	protected void configureDictInfo() {
		callAndStopOnFailure(SelectDICTCodeLocalInstrument.class);
		callAndStopOnFailure(EnforceAbsenceOfDebtorAccount.class);
		callAndStopOnFailure(SanitiseQrCodeConfig.class);
	}

	@Override
	protected void createPlaceholder() {
		callAndStopOnFailure(AskForScreenshotWithAccountSelection.class);

		env.putString("error_callback_placeholder", env.getString("payments_placeholder"));
	}


	@Override
	protected void onPostAuthorizationFlowComplete() {
		waitForPlaceholders();

		eventLog.log(getName(), "All test steps have run. The test will remaining in 'WAITING' state until the required screenshot is uploaded using the 'Upload Images' button at the top of the page. It may take upto 30 seconds for the test to move to 'FINISHED' after the upload.");

		setStatus(Status.WAITING);
	}

}

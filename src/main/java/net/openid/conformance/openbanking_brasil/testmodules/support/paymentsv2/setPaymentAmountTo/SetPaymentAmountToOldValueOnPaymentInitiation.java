package net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.setPaymentAmountTo;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PostEnvironment;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;

import java.util.Optional;

public class SetPaymentAmountToOldValueOnPaymentInitiation extends AbstractCondition {

	@Override
	@PreEnvironment(required = "resource_request_entity_claims", strings = "old_amount")
	@PostEnvironment(required = "resource_request_entity_claims")
	public Environment evaluate(Environment env) {
		String oldAmount = env.getString("old_amount");
		JsonObject paymentObj = Optional.ofNullable(env.getElementFromObject("resource_request_entity_claims", "data.payment"))
			.orElseThrow(() -> error("There is no data.payment object on resource_request_entity_claims"))
			.getAsJsonObject();
		paymentObj.addProperty("amount", oldAmount);
		logSuccess("The payment amount has been restore to the original amount", args(
			"amount", oldAmount
		));
		return env;
	}
}

package net.openid.conformance.openbanking_brasil.testmodules.dcr;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.AddScopeToDynamicRegistrationRequest;
import net.openid.conformance.condition.client.AddSoftwareStatementToClientConfigurationRequest;
import net.openid.conformance.condition.client.AddTlsClientAuthSubjectDnToClientConfigurationRequest;
import net.openid.conformance.condition.client.AddTlsClientAuthSubjectDnToDynamicRegistrationRequest;
import net.openid.conformance.condition.client.CallClientConfigurationEndpoint;
import net.openid.conformance.condition.client.CallTokenEndpointAndReturnFullResponse;
import net.openid.conformance.condition.client.CheckClientConfigurationAccessTokenFromClientConfigurationEndpoint;
import net.openid.conformance.condition.client.CheckClientConfigurationUriFromClientConfigurationEndpoint;
import net.openid.conformance.condition.client.CheckClientIdFromClientConfigurationEndpoint;
import net.openid.conformance.condition.client.CheckIfTokenEndpointResponseError;
import net.openid.conformance.condition.client.CheckRedirectUrisFromClientConfigurationEndpoint;
import net.openid.conformance.condition.client.CheckRegistrationClientEndpointContentType;
import net.openid.conformance.condition.client.CheckRegistrationClientEndpointContentTypeHttpStatus200;
import net.openid.conformance.condition.client.CheckTokenEndpointHttpStatus200;
import net.openid.conformance.condition.client.CreateClientConfigurationRequestFromDynamicClientRegistrationResponse;
import net.openid.conformance.condition.client.CreateTokenEndpointRequestForClientCredentialsGrant;
import net.openid.conformance.condition.client.ExtractMTLSCertificates2FromConfiguration;
import net.openid.conformance.condition.client.FAPIBrazilCallDirectorySoftwareStatementEndpointWithBearerToken;
import net.openid.conformance.condition.client.FAPIBrazilExtractClientMTLSCertificateSubject;
import net.openid.conformance.condition.client.SetConsentsScopeOnTokenEndpointRequest;
import net.openid.conformance.condition.client.SetPaymentsScopeOnTokenEndpointRequest;
import net.openid.conformance.fapi1advancedfinal.AbstractFAPI1AdvancedFinalBrazilDCR;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.support.AddScopeToClientConfigurationFromConsentUrl;
import net.openid.conformance.openbanking_brasil.testmodules.support.ExtractMTLSCertificates3FromConfiguration;
import net.openid.conformance.testmodule.PublishTestModule;
import net.openid.conformance.testmodule.TestFailureException;
import net.openid.conformance.variant.ClientAuthType;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.VariantHidesConfigurationFields;

import java.util.Optional;


@PublishTestModule(
	testName = "dcr-brcac2022-support-fvp",
	displayName = "This test will make sure that the tested server supports both the old and the new format of BRCACs specified by the Open Banking Brasil Certificate Standard Document",
	summary = "This test will use a Hardcoded certificate issued from the Software Statement with ID 10120340-3318-4baf-99e2-0b56729c4ab2. As this Software is used on other tests, make sure that there isn't already a registered client for this software before executing the test \n" +
		"This tests will make sure that the tested server supports both the old and the new format of BRCACs specified by the Open Banking Brasil Certificate Standard Document\n" +
		"Test Behaviour:\n" +
		"\u2022 Perform a DCR against the target Server using old style BRCAC\n" +
		"\u2022 Expect a success 201 - First client_id (1) created for this set of credentials\n" +
		"\u2022 Request an access token with the first client_id (1) using the client_credentials grant asking for either payments or consents (depending on client configuration) scope - Expect a success\n" +
		"\u2022 Using the new style BRCAC call the GET Registration API for the first client_id (1) - Expect a success\n" +
		"\u2022 Using the new style BRCAC obtain an SSA from the participant directory\n" +
		"\u2022 Using the new style BRCAC call the PUT Registration API for the first client_id (1) with a matching request body - Expect a success\n" +
		"\u2022 Perform a DCR against the target Server using the new style BRCAC\n" +
		"\u2022 Expect a success 201 - Second client_id (2) created\n" +
		"\u2022 Request an access token with the second client_id (2) using the client_credentials grant asking for either payments or consents (depending on client configuration) scope - Expect a success\n" +
		"\u2022 Unregister both created clients using the new style BRCAC",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.discoveryUrl",
		"mtls2.cert",
		"mtls2.key",
		"mtls2.ca",
		"mtls3.cert",
		"mtls3.key",
		"mtls3.ca",
		"directory.client_id",
		"directory.discoveryUrl",
		"directory.apibase",
		"directory.keystore",
		"directory2.client_id"
	}
)
@VariantHidesConfigurationFields(parameter = FAPI1FinalOPProfile.class, value = "openbanking_brazil", configurationFields = {
	"client.org_jwks",
	"resource.brazilPixPayment",
	"resource.brazilCpf",
	"resource.brazilCnpj",
})
public class DcrBrcac2022SupportFvpTestModule extends AbstractFAPI1AdvancedFinalBrazilDCR {

	private boolean isOldBrcacClient = true;
	private ClientAuthType clientAuthType;

	@Override
	protected void configureClient() {
		clientAuthType = getVariant(ClientAuthType.class);
		callAndStopOnFailure(AddScopeToClientConfigurationFromConsentUrl.class);

		eventLog.startBlock("Switch to second client");
		String directory2ClientId = Optional.ofNullable(env.getString("config", "directory2.client_id"))
			.orElseThrow(() -> new TestFailureException(getId(), "Could not find directory2.client_id"));
		env.putString("config", "directory.client_id", directory2ClientId);

		super.configureClient();
	}

	@Override
	protected void getSsa() {
		//Hack to avoid copying paste the entire super.configureClient to change only the ExtractMTLSCertificatesFromConfiguration condition
		callAndContinueOnFailure(ExtractMTLSCertificates3FromConfiguration.class);
		env.removeObject("mutual_tls_authentication");
		env.mapKey("mutual_tls_authentication", "mutual_tls_authentication3");
		env.mapKey("certificate_subject", "certificate_subject3");
		super.getSsa();
		env.unmapKey("mutual_tls_authentication");
		env.unmapKey("certificate_subject");
		callAndStopOnFailure(ExtractMTLSCertificates2FromConfiguration.class);
		env.mapKey("mutual_tls_authentication", "mutual_tls_authentication2");
		env.mapKey("certificate_subject", "certificate_subject2");
		callAndStopOnFailure(FAPIBrazilExtractClientMTLSCertificateSubject.class);
	}

	@Override
	protected void setupResourceEndpoint() {
		// Protected resource is not used in this test
	}

	@Override
	protected void performAuthorizationFlow() {

		performClientCredentialsGrant();

		if (isOldBrcacClient) {
			isOldBrcacClient = false;
			eventLog.startBlock("Retrieve client configuration using new BRCAC");

			env.unmapKey("mutual_tls_authentication");
			env.unmapKey("certificate_subject");
			env.mapKey("mutual_tls_authentication", "mutual_tls_authentication3");
			env.mapKey("certificate_subject", "certificate_subject3");

			callClientConfigurationEndpoint();
			eventLog.endBlock();
			eventLog.startBlock("Make PUT request to the client configuration using new BRCAC");

			createClientConfigurationRequest();

			callClientConfigurationEndpoint();
			eventLog.endBlock();

			deleteClient();

			eventLog.startBlock("Perform Dynamic Client Registration");

			if (clientAuthType == ClientAuthType.MTLS) {
				callAndStopOnFailure(AddTlsClientAuthSubjectDnToDynamicRegistrationRequest.class);
			}
			addSoftwareStatementToRegistrationRequest();
			callRegistrationEndpoint();
			eventLog.endBlock();
			performAuthorizationFlow();
		} else {
			deleteClient();
			fireTestFinished();
		}
	}

	@Override
	protected void addJwksToRequest() {
		callAndStopOnFailure(AddScopeToDynamicRegistrationRequest.class);
		super.addJwksToRequest();
	}

	@Override
	protected void deleteClient() {
		eventLog.startBlock("Unregister dynamically registered client");
		super.deleteClient();
		eventLog.endBlock();
	}

	protected void performClientCredentialsGrant() {
		eventLog.startBlock("Calling Token Endpoint with client_credentials grant using " +
			(isOldBrcacClient ? "Old BRCAC" : "New BRCAC"));

		callAndStopOnFailure(CreateTokenEndpointRequestForClientCredentialsGrant.class);

		if (brazilPayments.isTrue()) {
			callAndStopOnFailure(SetPaymentsScopeOnTokenEndpointRequest.class);
		} else {
			callAndStopOnFailure(SetConsentsScopeOnTokenEndpointRequest.class);
		}

		call(sequence(addTokenEndpointClientAuthentication));
		callAndStopOnFailure(CallTokenEndpointAndReturnFullResponse.class);
		callAndContinueOnFailure(CheckTokenEndpointHttpStatus200.class, Condition.ConditionResult.FAILURE);
		callAndStopOnFailure(CheckIfTokenEndpointResponseError.class);
		eventLog.endBlock();
	}

	protected void createClientConfigurationRequest() {
		callAndStopOnFailure(CreateClientConfigurationRequestFromDynamicClientRegistrationResponse.class);
		// get a new SSA (technically there should be one in the DCR response, but they may be single use?)
		callAndStopOnFailure(FAPIBrazilCallDirectorySoftwareStatementEndpointWithBearerToken.class);
		callAndStopOnFailure(AddSoftwareStatementToClientConfigurationRequest.class);

		if (clientAuthType == ClientAuthType.MTLS) {
			callAndStopOnFailure(AddTlsClientAuthSubjectDnToClientConfigurationRequest.class);
		}
	}

	protected void callClientConfigurationEndpoint() {
		callAndStopOnFailure(CallClientConfigurationEndpoint.class);
		callAndContinueOnFailure(CheckRegistrationClientEndpointContentTypeHttpStatus200.class, Condition.ConditionResult.FAILURE, "OIDCD-4.3");
		callAndContinueOnFailure(CheckRegistrationClientEndpointContentType.class, Condition.ConditionResult.FAILURE, "OIDCD-4.3");
		callAndContinueOnFailure(CheckClientIdFromClientConfigurationEndpoint.class, Condition.ConditionResult.FAILURE, "RFC7592-3");
		callAndContinueOnFailure(CheckRedirectUrisFromClientConfigurationEndpoint.class, Condition.ConditionResult.FAILURE, "RFC7592-3");
		callAndContinueOnFailure(CheckClientConfigurationUriFromClientConfigurationEndpoint.class, Condition.ConditionResult.FAILURE, "RFC7592-3");
		callAndContinueOnFailure(CheckClientConfigurationAccessTokenFromClientConfigurationEndpoint.class, Condition.ConditionResult.FAILURE, "RFC7592-3");
	}

}

package net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.setPaymentAmountTo;

import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.AbstractSetAmount;

public class SetPaymentAmountToOutOfRangeOnPaymentInitiation extends AbstractSetAmount {

	@Override
	protected String getAmount() {
		return "999999999.99";
	}

	@Override
	protected String getEnvKey() {
		return "brazilPixPayment";
	}
}

package net.openid.conformance.openbanking_brasil.testmodules.v2;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.PreFlightCertCheckModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.AddProductTypeToPhase2V2Config;
import net.openid.conformance.openbanking_brasil.testmodules.support.consent.v2.ValidateConsentsOperationalFieldV2;
import net.openid.conformance.testmodule.PublishTestModule;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.VariantHidesConfigurationFields;

@PublishTestModule(
	testName = "preflight-check-operational-test-v2",
	displayName = "Pre-flight checks will validate the mTLS certificate before requesting an access token using the Directory client_id provided in the test configuration. An SSA will be generated using the Open Banking Brasil Directory. Finally" +
		"a check of mandatory fields will be made",
	summary = "Pre-flight checks will validate the mTLS certificate before requesting an access token using the Directory client_id provided in the test configuration. An SSA will be generated using the Open Banking Brasil Directory. Finally" +
		"a check of mandatory fields will be made, included fields for Operational Limits",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"resource.consentUrl",
		"resource.brazilCpf",
		"resource.brazilCnpj",
		"resource.brazilCpfOperational",
		"resource.brazilCnpjOperationalBusiness",
        "directory.client_id"
	}
)
@VariantHidesConfigurationFields(parameter = FAPI1FinalOPProfile.class, value = "openbanking_brazil", configurationFields = {
	"consent.productType"
})

public class PreFlightCheckOperationalV2Module extends PreFlightCertCheckModule {

    @Override
    protected void runTests() {
		super.runTests();
		runInBlock("Pre-flight Consent field checks", () -> {
			callAndStopOnFailure(AddProductTypeToPhase2V2Config.class);
			callAndContinueOnFailure(ValidateConsentsOperationalFieldV2.class, Condition.ConditionResult.FAILURE);
		});
    }
}

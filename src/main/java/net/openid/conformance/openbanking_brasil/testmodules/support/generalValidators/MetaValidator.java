package net.openid.conformance.openbanking_brasil.testmodules.support.generalValidators;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.testmodule.OIDFJSON;
import net.openid.conformance.util.field.DatetimeField;
import net.openid.conformance.util.field.IntField;
import net.openid.conformance.util.field.ObjectField;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.util.Map.entry;

public class MetaValidator {

    private final AbstractJsonAssertingCondition validator;
    private final boolean isTotalRecordsMandatory;
    private final boolean isTotalPagesMandatory;
    private final boolean isRequestDateTimeMandatory;
    private int pageSize;
    private int numberOfRecords;
    private int totalRecords;
    private int totalPages;
    private int expectedAmountPages;
    private String errorMessage = "";
    private final Map<String, Object> args = new HashMap<>();

    public MetaValidator(AbstractJsonAssertingCondition validator) {
        this.validator = validator;
        this.isTotalRecordsMandatory = true;
        this.isTotalPagesMandatory = true;
        this.isRequestDateTimeMandatory = true;
    }

    public MetaValidator(AbstractJsonAssertingCondition validator, boolean isTotalRecordsMandatory,
                         boolean isTotalPagesMandatory, boolean isRequestDateTimeMandatory) {
        this.validator = validator;
        this.isTotalRecordsMandatory = isTotalRecordsMandatory;
        this.isTotalPagesMandatory = isTotalPagesMandatory;
        this.isRequestDateTimeMandatory = isRequestDateTimeMandatory;
    }

    public void assertMetaObject(JsonElement body) {
        setPageSize(body);
        setNumberOfRecords(body);

        validator.assertField(body,
            new ObjectField
                .Builder("meta")
                .setValidator(this::assertMeta)
                .build());
    }

    private void setPageSize(JsonElement body) {
        String pageSizePattern = "page-size=(?<size>[0-9]+)";
        String selfLink = OIDFJSON.getString(Optional.ofNullable(body.getAsJsonObject()
            .getAsJsonObject("links")
            .get("self")
        ).orElse(new JsonPrimitive("")));

        Pattern pattern = Pattern.compile(pageSizePattern);
        Matcher matcher = pattern.matcher(selfLink);
        pageSize = matcher.find() ? Integer.parseInt(matcher.group("size")) : -1;
    }

    private void setNumberOfRecords(JsonElement body) {
        JsonElement data = body.getAsJsonObject().get("data");
        if (data.isJsonArray()) {
            numberOfRecords = data.getAsJsonArray().size();
        } else {
            numberOfRecords = 1;
            List<String> keysWithData = List.of("releases");
            for (Map.Entry<String, JsonElement> entry : data.getAsJsonObject().entrySet()) {
                if (keysWithData.contains(entry.getKey()) && entry.getValue().isJsonArray()) {
                    numberOfRecords = entry.getValue().getAsJsonArray().size();
                }
            }
        }
    }

    private void assertMeta(JsonObject meta) {
        assertPossibleOptionalIntField(meta, "totalRecords", isTotalRecordsMandatory);
        assertPossibleOptionalIntField(meta, "totalPages", isTotalPagesMandatory);

        DatetimeField.Builder requestDateTime = new DatetimeField.Builder("requestDateTime")
            .setMaxLength(20);
        if (!isRequestDateTimeMandatory) {
            requestDateTime = requestDateTime.setOptional();
        }
        validator.assertField(meta, requestDateTime.build());

        assertLogicConditions(meta);
    }

    private void assertLogicConditions(JsonObject meta) {
        totalRecords = OIDFJSON.getInt(
            Optional.ofNullable(meta.get("totalRecords")).orElse(new JsonPrimitive(-1)));
        totalPages = OIDFJSON.getInt(
            Optional.ofNullable(meta.get("totalPages")).orElse(new JsonPrimitive(-1)));
        if (totalRecords >= 0 && totalPages >= 0) {
            if (numberOfRecords == 0) {
                if (totalRecords != 0 || totalPages != 0) {
                    buildMessageAndArgs("When there are no resources, both totalRecords and totalPages should be 0",
                        "totalRecords", "number of records on page", "totalPages");
                }
            } else if (pageSize >= 0) {
                expectedAmountPages = (int) Math.ceil((double) totalRecords / pageSize);

                if (totalRecords > pageSize) {
                    if (totalPages <= 1) {
                        buildMessageAndArgs("Number of records exceeds page size, but totalPages is not greater than 1",
                            "totalRecords", "pageSize", "totalPages");
                    }
                    else if (numberOfRecords != pageSize) {
                        buildMessageAndArgs("Number of records does not match pageSize",
                            "totalRecords", "number of records on page", "pageSize");
                    }
                    else if (totalPages != expectedAmountPages) {
                        buildMessageAndArgs("The totalPages value is not equal to the amount of pages expected",
                            "totalRecords", "pageSize", "totalPages", "totalRecords / pageSize");
                    }
                }
                else {
                    if (totalPages != 1) {
                        buildMessageAndArgs("Number of records does not exceed page size, but totalPages is not equal to 1",
                            "totalRecords", "pageSize", "totalPages");
                    }
                    else if (numberOfRecords != totalRecords) {
                        buildMessageAndArgs("Number of records does not match totalRecords",
                            "totalRecords", "pageSize", "totalPages");
                    }
                }
            }
        }
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public Map<String, Object> getArgs() {
        return args;
    }

    private void assertPossibleOptionalIntField(JsonObject meta, String fieldName, boolean isMandatory) {
        IntField.Builder field = new IntField.Builder(fieldName);
        if (!isMandatory) {
            field = field.setOptional();
        }
        validator.assertField(meta, field.build());
    }

    private void buildMessageAndArgs(String message, String... args) {
        errorMessage = message;
        Map<String, Integer> argMap = Map.ofEntries(
            entry("totalRecords", totalRecords),
            entry("totalPages", totalPages),
            entry("number of records on page", numberOfRecords),
            entry("pageSize", pageSize),
            entry("totalRecords / pageSize", expectedAmountPages)
        );

        this.args.clear();
        for (String arg : args) {
            this.args.put(arg, argMap.get(arg));
        }
    }
}

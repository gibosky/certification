package net.openid.conformance.openbanking_brasil.testmodules.v2.payments;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.EnsureContentTypeApplicationJwt;
import net.openid.conformance.condition.client.ExtractSignedJwtFromResourceResponse;
import net.openid.conformance.condition.client.FAPIBrazilValidateResourceResponseSigningAlg;
import net.openid.conformance.condition.client.FAPIBrazilValidateResourceResponseTyp;
import net.openid.conformance.condition.client.FetchServerKeys;
import net.openid.conformance.condition.client.ValidateResourceResponseJwtClaims;
import net.openid.conformance.condition.client.ValidateResourceResponseSignature;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.support.EnsureResponseCodeWas201;
import net.openid.conformance.openbanking_brasil.testmodules.support.EnsureResponseCodeWas403;
import net.openid.conformance.openbanking_brasil.testmodules.support.EnsureResponseCodeWas422;
import net.openid.conformance.openbanking_brasil.testmodules.support.EnsureResponseWasJson;
import net.openid.conformance.openbanking_brasil.testmodules.support.ResetPaymentRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.SelectDICTCodeLocalInstrument;
import net.openid.conformance.openbanking_brasil.testmodules.support.SelectDICTCodePixLocalInstrument;
import net.openid.conformance.openbanking_brasil.testmodules.support.SetIncorrectAmountInPayment;
import net.openid.conformance.openbanking_brasil.testmodules.support.SetIncorrectCurrencyPayment;
import net.openid.conformance.openbanking_brasil.testmodules.support.SetScope;
import net.openid.conformance.openbanking_brasil.testmodules.support.StoreScope;
import net.openid.conformance.openbanking_brasil.testmodules.support.ValidatePaymentAndConsentHaveSameProperties;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureErrorResponse.EnsureErrorResponseCodeFieldWasPagamentaDivergenteConsentimento;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentStatus.EnsurePaymentStatusWasAscs;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "payments-api-negative-tests-v2",
	displayName = "Payments Consents API test module for dict local instrument",
	summary = "Ensure an error status in sent on different inconsistent scenarios (Reference Error 2.2.2.5/2.2.2.6)\n" +
		"1. Ensure error when currency is different between consents and payment\n" +
		"\u2022 Calls POST Consents Endpoint with valid payload, using DICT as the localInstrument\n" +
		"\u2022 Expects 201 - Validate Error\n" +
		"\u2022 Redirects the user to authorize the created consent\n" +
		"\u2022  Call GET Consent\n" +
		"\u2022 Expects 200 - Validate if status is \"AUTHORISED\"\n" +
		"\u2022 Calls the POST Payments Endpoint with a different currency\n" +
		"\u2022 Expects 422 PAGAMENTO_DIVERGENTE_CONSENTIMENTO \n" +
		"\u2022 Validate error message\n" +
		"\n" +
		"2. Ensure error when the amount is different between consent and payment\n" +
		"\u2022 Calls POST Consents Endpoint with valid payload, using DICT as the localInstrument\n" +
		"\u2022 Expects 201\n" +
		"\u2022 Redirects the user to authorize the created consent\n" +
		"\u2022 Call GET Consent\n" +
		"\u2022 Expects 200 - Validate if status is \"AUTHORISED\"\n" +
		"\u2022 Calls the POST Payments Endpoint with different amount\n" +
		"\u2022 Expects 422 PAGAMENTO_DIVERGENTE_CONSENTIMENTO \n" +
		"\u2022 Validate error message\n" +
		"\n" +
		"3. Ensure POST payments can only be called with authorization code flow\n" +
		"\u2022 Calls POST Consents Endpoint with valid payload\n" +
		"\u2022 Expects 201 - Validate Response\n" +
		"\u2022  Calls the POST Payments Endpoint with client_credentials token\n" +
		"\u2022 Expects 403\n" +
		"\u2022 Validate error message\n" +
		"\n" +
		"4. Ensure POST payment is successful when valid payload is sent\n" +
		"\u2022 Calls POST Consents Endpoint with valid payload, using DICT as the localInstrument\n" +
		"\u2022 Expects 201\n" +
		"\u2022 Redirects the user to authorize the consent\n" +
		"\u2022 Call GET Consent\n" +
		"\u2022 Expects 200 - Validate if status is \"AUTHORISED\"\n" +
		"\u2022 Calls the POST Payments Endpoint with valid payload\n" +
		"\u2022 Expects 201 - Validate Response\n" +
		"\u2022 Poll the Get Payments endpoint with the PaymentID Created while payment status is RCVD, ACCP or ACPD\n" +
		"\u2022 Expectes Definitive  state (ASCS) - Validate Response",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"resource.consentUrl"
	}
)
public class PaymentsApiNegativeTestModuleV2 extends AbstractPaymentUnhappyPathTestModuleV2 {

	private int batch = 1;

	@Override
	protected void configureDictInfo() {
		callAndStopOnFailure(SelectDICTCodeLocalInstrument.class);
		callAndStopOnFailure(SelectDICTCodePixLocalInstrument.class);
		callAndStopOnFailure(ValidatePaymentAndConsentHaveSameProperties.class);
		eventLog.startBlock("Storing authorisation endpoint");
		callAndStopOnFailure(StoreScope.class);
		callAndStopOnFailure(SetIncorrectCurrencyPayment.class);
	}

	@Override
	protected void validatePaymentRejectionReasonCode() {
		// Not needed in this test
	}

	@Override
	protected void validate422ErrorResponseCode() {
		if (batch == 1 || batch == 2) {
			callAndStopOnFailure(EnsureErrorResponseCodeFieldWasPagamentaDivergenteConsentimento.class);
		}

	}

	@Override
	protected Class<? extends Condition> getExpectedPaymentResponseCode() {
		if (batch == 1 || batch == 2) {
			return EnsureResponseCodeWas422.class;
		}

		if (batch == 3) {
			return EnsureResponseCodeWas403.class;
		}

		return EnsureResponseCodeWas201.class;

	}

	@Override
	protected ConditionSequence getPixPaymentSequence() {
		if (batch == 3) {
			return super.getPixPaymentSequence()
				.replace(EnsureContentTypeApplicationJwt.class, condition(EnsureResponseWasJson.class))
				.skip(ExtractSignedJwtFromResourceResponse.class, "JSON response is expected")
				.skip(FAPIBrazilValidateResourceResponseSigningAlg.class, "JSON response is expected")
				.skip(FAPIBrazilValidateResourceResponseTyp.class, "JSON response is expected")
				.skip(FetchServerKeys.class, "JSON response is expected")
				.skip(ValidateResourceResponseSignature.class, "JSON response is expected")
				.skip(ValidateResourceResponseJwtClaims.class, "JSON response is expected");
		}
		return super.getPixPaymentSequence();
	}


	@Override
	protected void performNon422ResponseActions() {
		if(batch == 3){
			performErrorValidation();
		}else {
			super.performNon422ResponseActions();
		}
	}

	@Override
	protected void validateFinalState() {
		callAndStopOnFailure(EnsurePaymentStatusWasAscs.class);
	}

	@Override
	protected void onPostAuthorizationFlowComplete() {
		switch (batch) {
			case 1: {
				eventLog.startBlock("2. Ensure error when the amount is different between consent and payment");
				validationStarted = false;
				batch++;
				callAndStopOnFailure(ResetPaymentRequest.class);
				callAndStopOnFailure(SetIncorrectAmountInPayment.class);
				callAndStopOnFailure(SetScope.class);
				performAuthorizationFlow();
				break;
			}
			case 2: {
				eventLog.startBlock("3. Ensure POST payments can only be called with authorization code flow");
				validationStarted = false;
				batch++;
				callAndStopOnFailure(SetScope.class);
				performPreAuthorizationSteps();
				requestProtectedResource();
				onPostAuthorizationFlowComplete();
				break;
			}
			case 3: {
				eventLog.startBlock("4. Ensure POST payment is successful when valid payload is sent");
				validationStarted = false;
				batch++;
				callAndStopOnFailure(ResetPaymentRequest.class);
				callAndStopOnFailure(SetScope.class);
				performAuthorizationFlow();
				break;
			}
			default:{
				fireTestFinished();
				break;
			}
		}
	}
}

package net.openid.conformance.openbanking_brasil.testmodules.dcr;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.AddSoftwareStatementToClientConfigurationRequest;
import net.openid.conformance.condition.client.AddTlsClientAuthSubjectDnToClientConfigurationRequest;
import net.openid.conformance.condition.client.CallTokenEndpointAndReturnFullResponse;
import net.openid.conformance.condition.client.CheckTokenEndpointHttpStatus200;
import net.openid.conformance.condition.client.CheckTokenEndpointReturnedJsonContentType;
import net.openid.conformance.condition.client.CreateClientConfigurationRequestFromDynamicClientRegistrationResponse;
import net.openid.conformance.condition.client.CreateTokenEndpointRequestForClientCredentialsGrant;
import net.openid.conformance.condition.client.ExtractMTLSCertificates2FromConfiguration;
import net.openid.conformance.condition.client.FAPIBrazilCallDirectorySoftwareStatementEndpointWithBearerToken;
import net.openid.conformance.condition.client.FAPIBrazilExtractClientMTLSCertificateSubject;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.AbstractDcmSubjectDnTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.ChuckWarning;
import net.openid.conformance.openbanking_brasil.testmodules.support.ExtractMTLSCertificates3FromConfiguration;
import net.openid.conformance.openbanking_brasil.testmodules.support.OverrideScopeWithOpenIdPaymentsConsents;
import net.openid.conformance.openbanking_brasil.testmodules.support.SetPaymentsConsentsScopeOnTokenEndpointRequest;
import net.openid.conformance.testmodule.PublishTestModule;
import net.openid.conformance.testmodule.TestFailureException;
import net.openid.conformance.variant.ClientAuthType;

import java.util.Optional;

@PublishTestModule(
	testName = "dcm-subject-dn-test-fvp",
	displayName = "Brazil DCM: payments: check that subjectdn can be updated using the dynamic client management endpoint\n",
	summary = "This test has the objective of making sure that servers that use tls_client_auth as a client authentication correctly support DCM by accepting the update of the tls_subject_dn value.\n" +
		"Test uses mtls2, a Certificate issued from Serasa and mtls3, certificate issued from Soluti, linked to directory2.client_id software statement\n" +
		"\u2022 Obtain a SSA from Participant Directory with the directory2.client_id using the mtls3 for client authentication\n" +
		"\u2022 Call the POST registration endpoint with payload containing the SSA obtained from the directory and the subject_dn for mtls2 certificate. Use mtls2 as the transport certificate \n" +
		"\u2022 Expect a 201 - Validate response obtained - Save client_id and registration_access_token\n" +
		"\u2022 Call the POST token endpoint with client_credentials grant using mtls3 as the transport certificate\n" +
		"\u2022 Expect a 400 or 401 response\n" +
		"\u2022 Call the GET registration endpoint using mtls3 as the transport certificate\n" +
		"\u2022 Expect a 200 - Save the response_body\n" +
		"\u2022 Call the PUT registration endpoint using the saved response_body, but switching subject_dn for that of mtls3 certificate\n" +
		"\u2022 Expect a 200 - Validate response obtained\n" +
		"\u2022 Call the POST token endpoint with client_credentials grant  using mtls2 as the transport certificate\n" +
		"\u2022 Expect a 400 or 401 response\n" +
		"\u2022 Call the GET registration endpoint using mtls2 as the transport certificate\n" +
		"\u2022 Expect a 200 - Save the response_body\n" +
		"\u2022 Call the PUT registration endpoint using the saved response_body, but switching subject_dn for that of mtls2 certificate\n" +
		"\u2022 Expect a 200 - Validate response obtained\n" +
		"\u2022 Call the POST token endpoint with client_credentials grant using mtls2 as the transport certificate\n" +
		"\u2022 Expect a 200\n" +
		"\u2022 Call the DELETE registration endpoint using mtls2 as the transport certificate\n" +
		"\u2022 Expect a 204 - Validate response obtained",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.discoveryUrl",
		"directory.client_id",
		"client.jwks",
		"mtls2.key",
		"mtls2.cert",
		"mtls2.ca",
		"mtls3.key",
		"mtls3.cert",
		"mtls3.ca",
		"directory.client_id",
		"directory.discoveryUrl",
		"directory.apibase",
		"directory.keystore",
		"directory2.client_id",
	}
)
public class DcmSubjectDnFvpTetsModule extends AbstractDcmSubjectDnTestModule {

	@Override
	protected void configureClient() {
		callAndStopOnFailure(OverrideScopeWithOpenIdPaymentsConsents.class);

		eventLog.startBlock("Switch to second client");
		String directory2ClientId = Optional.ofNullable(env.getString("config", "directory2.client_id"))
			.orElseThrow(() -> new TestFailureException(getId(), "Could not find directory2.client_id"));
		env.putString("config", "directory.client_id", directory2ClientId);

		super.configureClient();
	}

	@Override
	protected void getSsa() {
		//Hack to avoid copying paste the entire super.configureClient to change only the ExtractMTLSCertificatesFromConfiguration condition
		callAndContinueOnFailure(ExtractMTLSCertificates3FromConfiguration.class);
		switchToSecondClient();
		super.getSsa();
		unmapClient();
		callAndContinueOnFailure(ExtractMTLSCertificates2FromConfiguration.class, Condition.ConditionResult.FAILURE);
		callAndContinueOnFailure(FAPIBrazilExtractClientMTLSCertificateSubject.class);
	}

	@Override
	protected void addScopeToClientCredentialsGrant() {
		callAndStopOnFailure(SetPaymentsConsentsScopeOnTokenEndpointRequest.class);
	}

	@Override
	protected void setupResourceEndpoint() {
		// not needed as resource endpoint won't be called
	}

	@Override
	protected void validateDcrResponseScope() {
		// not needed as this is out of the scope of the test
	}

	@Override
	protected void onConfigure(JsonObject config, String baseUrl) {

	}

	@Override
	protected void performAuthorizationFlow() {
		ClientAuthType clientAuthType = getVariant(ClientAuthType.class);
		if (clientAuthType != ClientAuthType.MTLS) {
			env.putString("warning_message", "As this test is designed to use MTLS, and the private key jwt option is selected, the test will not continue.");
			callAndContinueOnFailure(ChuckWarning.class, Condition.ConditionResult.SUCCESS);
			fireTestFinished();
		}

		eventLog.startBlock("Switch to certificate with different subjectdn, verify that client credentials grant fails");
		switchToSecondClient();
		callClientCredentialsGrantExpectingFailure();
		eventLog.endBlock();

		eventLog.startBlock("Make GET request to client configuration endpoint expecting a success using second certificate");
		env.removeObject("registration_client_endpoint_request_body");
		callClientConfigurationEndpoint();
		eventLog.endBlock();

		eventLog.startBlock("Make PUT request to client configuration endpoint with subjectdn for second certificate expecting success");
		createClientConfigurationRequestWithSubjectDn();
		callClientConfigurationEndpoint();
		eventLog.endBlock();

		eventLog.startBlock("Switch back to original certificate, verify that client credentials grant now fails");
		unmapClient();
		callClientCredentialsGrantExpectingFailure();
		eventLog.endBlock();

		eventLog.startBlock("Make GET request to client configuration endpoint expecting a success using original certificate");
		env.removeObject("registration_client_endpoint_request_body");
		callClientConfigurationEndpoint();
		eventLog.endBlock();

		eventLog.startBlock("Make PUT request to client configuration endpoint with subjectdn for original certificate expecting success");
		callAndStopOnFailure(CreateClientConfigurationRequestFromDynamicClientRegistrationResponse.class);
		switchToSecondClient();
		callAndStopOnFailure(FAPIBrazilCallDirectorySoftwareStatementEndpointWithBearerToken.class);
		unmapClient();
		callAndStopOnFailure(AddSoftwareStatementToClientConfigurationRequest.class);
		callAndStopOnFailure(FAPIBrazilExtractClientMTLSCertificateSubject.class);
		callAndStopOnFailure(AddTlsClientAuthSubjectDnToClientConfigurationRequest.class);
		callClientConfigurationEndpoint();
		eventLog.endBlock();

		eventLog.startBlock("Make POST request to the token token endpoint with client_credentials grant with original certificate expecting success");
		callAndStopOnFailure(CreateTokenEndpointRequestForClientCredentialsGrant.class);
		addScopeToClientCredentialsGrant();
		call(sequence(addTokenEndpointClientAuthentication));
		callAndStopOnFailure(CallTokenEndpointAndReturnFullResponse.class);
		callAndContinueOnFailure(CheckTokenEndpointHttpStatus200.class);
		callAndContinueOnFailure(CheckTokenEndpointReturnedJsonContentType.class, Condition.ConditionResult.FAILURE, "OIDCC-3.1.3.4");
		eventLog.endBlock();

		fireTestFinished();
	}

	@Override
	protected void switchToSecondClient() {
		eventLog.log(getName(), "Switched to the second client");
		env.unmapKey("mutual_tls_authentication");
		env.mapKey("mutual_tls_authentication", "mutual_tls_authentication3");
		env.unmapKey("certificate_subject");
		env.mapKey("certificate_subject", "certificate_subject3");
	}
	@Override
	protected void unmapClient() {
		eventLog.log(getName(), "Switched to the first client");
		env.unmapKey("mutual_tls_authentication");
		env.mapKey("mutual_tls_authentication", "mutual_tls_authentication2");
		env.unmapKey("certificate_subject");
		env.mapKey("certificate_subject", "certificate_subject2");
	}

}

package net.openid.conformance.openbanking_brasil.testmodules.v2.yacs.payments.support;

import com.google.common.base.Strings;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.openbanking_brasil.testmodules.support.payments.DictHomologKeys;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.util.JsonObjectBuilder;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Map;
import java.util.Random;

public class AddPaymentConsentsRequestBodyToConfigYacs extends AbstractCondition {
	@Override
	public Environment evaluate(Environment env) {
		String brasilCpf = env.getString("config", "resource.brazilCpf");

		String businessEntityIdentification = env.getString("config", "resource.brazilCnpj");
		if(Strings.isNullOrEmpty(brasilCpf)) {
			throw error("brazilCpf is missing.");
		}

		LocalDate currentDate = LocalDate.now(ZoneId.of("UTC"));
		Random random = new Random();

		double paymentAmount = random.nextInt(2);

		JsonObjectBuilder jsonObjectBuilder = new JsonObjectBuilder()
			.addFields("data.loggedUser.document", Map.of(
				"identification", brasilCpf,
				"rel", "CPF")
			)
			.addFields("data.creditor",
				Map.of(
					"personType", DictHomologKeys.PROXY_PRODUCTION_PERSON_TYPE,
					"cpfCnpj", DictHomologKeys.PROXY_PRODUCTION_CPF,
					"name", DictHomologKeys.PROXY_PRODUCTION_NAME)
			)
			.addFields("data.payment", Map.of(
				"type", "PIX",
				"currency", DictHomologKeys.PROXY_EMAIL_STANDARD_CURRENCY,
				"amount", String.format("%.2f", paymentAmount),
				"date", currentDate.toString())
			)
			.addFields("data.payment.details", Map.of(
				"localInstrument", DictHomologKeys.PROXY_EMAIL_STANDARD_LOCALINSTRUMENT,
				"proxy", DictHomologKeys.PROXY_PRODUCTION_PROXY)
			)
			.addFields("data.payment.details.creditorAccount", Map.of(
				"ispb", DictHomologKeys.PROXY_PRODUCTION_ISPB,
				"issuer", DictHomologKeys.PROXY_PRODUCTION_BRANCH_NUMBER,
				"number", DictHomologKeys.PROXY_PRODUCTION_ACCOUNT_NUMBER,
				"accountType", DictHomologKeys.PROXY_PRODUCTION_ACCOUNT_TYPE)
			);

		if (businessEntityIdentification != null){
			jsonObjectBuilder
				.addFields("data.businessEntity.document", Map.of(
					"identification", businessEntityIdentification,
					"rel", "CNPJ"
				));
		}



		JsonObject consentRequest = jsonObjectBuilder.build();

		env.putObject("config", "resource.brazilPaymentConsent", consentRequest);

		logSuccess("Consent request body was added to the config", args("consentRequest", consentRequest));

		return env;
	}
}

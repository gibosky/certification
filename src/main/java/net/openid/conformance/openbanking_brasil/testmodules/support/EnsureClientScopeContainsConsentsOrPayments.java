package net.openid.conformance.openbanking_brasil.testmodules.support;

import com.google.gson.JsonElement;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;

public class EnsureClientScopeContainsConsentsOrPayments extends AbstractCondition {
	@Override
	@PreEnvironment(required = "config")
	public Environment evaluate(Environment env) {
		JsonElement scopeElement = env.getElementFromObject("config", "client.scope");
		if(scopeElement == null){
			throw error("Could not find client scope in the configuration");
		}

		String scope = OIDFJSON.getString(scopeElement);
		if(scope.contains("payments") || scope.contains("consents")){
			logSuccess("Expected scopes are presented in the client configuration");
		}else {
			throw error("Client scope configuration does not contain expected scopes",
				args("expected", "payments or consents", "actual", scope));
		}
		return env;
	}
}

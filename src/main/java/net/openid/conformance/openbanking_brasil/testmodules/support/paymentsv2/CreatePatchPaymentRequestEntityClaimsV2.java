package net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2;

import com.google.gson.JsonElement;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PostEnvironment;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;

public class CreatePatchPaymentRequestEntityClaimsV2 extends AbstractCondition {

	@Override
	@PreEnvironment(required = "resource" )
	@PostEnvironment(required = "resource_request_entity_claims")
	public Environment evaluate(Environment env) {
		JsonElement patchRequest = env.getElementFromObject("resource", "brazilPatchPixPayment");
		if(patchRequest == null || !patchRequest.isJsonObject()) {
			throw error("Could not load brazilPatchPixPayment from the resource");
		}
		env.putObject("resource_request_entity_claims", patchRequest.getAsJsonObject());

		logSuccess(args("resource_request_entity_claims", patchRequest));
		return env;
	}

}

package net.openid.conformance.openbanking_brasil.testmodules.support.yacs;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.extensions.yacs.dcr.DcrClientRepository;
import net.openid.conformance.extensions.yacs.dcr.InMemoryDcrClientRepository;
import net.openid.conformance.testmodule.Environment;

public class EnsureDcrClientExists extends AbstractCondition {

	private DcrClientRepository dcrClientRepository() {
		return new InMemoryDcrClientRepository();
	}


	@Override
	@PreEnvironment(strings = "test_id")
	public Environment evaluate(Environment env) {
		String testId = env.getString("test_id");
		JsonObject client = dcrClientRepository().getSavedClient(testId);
		if (client != null){
			logSuccess("DCR Client successfully found");
			return env;
		}
		throw error("DCR Client not found, test stopping");
	}
}

package net.openid.conformance.openbanking_brasil.testmodules.v2.yacs.payments;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.*;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.support.EnsureTokenEndpointResponseScopeWasPayments;
import net.openid.conformance.openbanking_brasil.testmodules.support.PrepareToPostConsentRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.SetProtectedResourceUrlToPaymentsEndpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentConsentStatus.EnsurePaymentConsentStatusWasAwaitingAuthorisation;
import net.openid.conformance.openbanking_brasil.testmodules.support.yacs.*;
import net.openid.conformance.openbanking_brasil.testmodules.v2.payments.AbstractOBBrasilQrCodePaymentFunctionalTestModuleV2;
import net.openid.conformance.openbanking_brasil.testmodules.v2.yacs.payments.support.AddPaymentConsentsRequestBodyToConfigYacs;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.sequence.client.OpenBankingBrazilPreAuthorizationSteps;
import net.openid.conformance.sequence.client.RefreshTokenRequestSteps;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "fvp-payments-consents-core-test-v2",
	displayName = "sdfsdfsdfsdsdfsdf",
	summary = "sdfsdf",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"resource.consentUrl"
	}
)
public class YACSPaymentsConsentsCoreTestModuleV2 extends AbstractOBBrasilQrCodePaymentFunctionalTestModuleV2 {
	@Override
	protected void requestProtectedResource() {
		callAndStopOnFailure(ExtractAndAddRefreshTokenToEnv.class);
		call(new RefreshTokenRequestSteps(false,addTokenEndpointClientAuthentication,false));
		callAndStopOnFailure(EnsureRefreshTokenNotRotated.class);
	}

	@Override
	protected void configureDictInfo() {
		// Not needed in this test
	}

	@Override
	protected void validateClientConfiguration() {
		callAndStopOnFailure(SetScopeInClientConfigurationToOpenId.class);
		super.validateClientConfiguration();
	}
	@Override
	protected void performPreAuthorizationSteps() {
		super.performPreAuthorizationSteps();
		callAndStopOnFailure(EnsurePaymentConsentStatusWasAwaitingAuthorisation.class, Condition.ConditionResult.FAILURE);
	}
	@Override
	protected ConditionSequence createOBBPreauthSteps() {
		ConditionSequence preAuthSteps = new OpenBankingBrazilPreAuthorizationSteps(false, false, addTokenEndpointClientAuthentication, true, false, false)
			.replace(SetPaymentsScopeOnTokenEndpointRequest.class, condition(AddScopeToTokenEndpointRequest.class))
			.insertAfter(CheckIfTokenEndpointResponseError.class, condition(EnsureTokenEndpointResponseScopeWasPayments.class).requirements("RFC6749-3.3").dontStopOnFailure());
		return preAuthSteps;
	}
	@Override
	protected void onConfigure(JsonObject config, String baseUrl) {
		call(new ValidateRegisteredEndpoints().replace(ValidateConsentsAndResourcesEndpoints.class, condition(ValidateConsentsAndPaymentsEndpoints.class)));
		callAndStopOnFailure(AddPaymentConsentsRequestBodyToConfigYacs.class);
		callAndStopOnFailure(PrepareToPostConsentRequest.class);
		callAndStopOnFailure(SetProtectedResourceUrlToPaymentsEndpoint.class);
	}
	@Override
	protected void requestAuthorizationCode() {
		// Store the original access token and ID token separately (see RefreshTokenRequestSteps)
		env.mapKey("access_token", "first_access_token");
		env.mapKey("id_token", "first_id_token");

		super.requestAuthorizationCode();

		// Set up the mappings for the refreshed access and ID tokens
		env.mapKey("access_token", "second_access_token");
		env.mapKey("id_token", "second_id_token");

	}

}

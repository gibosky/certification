package net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.validators;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.BodyExtractor;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.util.field.DatetimeField;
import net.openid.conformance.util.field.ExtraField;
import net.openid.conformance.util.field.ObjectField;
import net.openid.conformance.util.field.StringField;

import java.text.ParseException;
import java.util.Optional;

public class ValidateMetaOnlyRequestDateTime extends AbstractJsonAssertingCondition {

	public static final String RESPONSE_ENV_KEY = "endpoint_response";
	public static final String IS_META_OPTIONAL = "is_meta_optional";

	@Override
	protected JsonElement bodyFrom(Environment environment) {
		try {
			return BodyExtractor.bodyFrom(environment, RESPONSE_ENV_KEY)
				.orElseThrow(() -> error("Could not extract body from response"));
		} catch (ParseException e) {
			throw error("Error parsing JWT response");
		}
	}

	/**
	 * Validates metadata in any response
	 * An actual full response has to be mapped to the endpoint_response
	 * Be default meta object is mandatory. If is_meta_optional env var is set to true, then meta will be validated only if it is present.
	 */
	@Override
	@PreEnvironment(required = RESPONSE_ENV_KEY)
	public Environment evaluate(Environment environment) {
		JsonElement body = bodyFrom(environment);
		Boolean isOptional = Optional.ofNullable(environment.getBoolean(IS_META_OPTIONAL)).orElse(false);

		assertField(body,
			new ObjectField
				.Builder("meta")
				.setValidator(this::assertMeta)
				.setOptional(isOptional)
				.build());
		return environment;
	}

	public void assertMeta(JsonObject meta) {
		parseResponseBody(meta, "meta");

		assertField(meta,
			new StringField
				.Builder("requestDateTime")
				.setMaxLength(20)
				.setPattern(DatetimeField.ALTERNATIVE_PATTERN)
				.build());

		assertExtraFields(new ExtraField.Builder()
			.setPattern("^([A-Z]{4})(-)(.*)$")
			.build());

	}
}

package net.openid.conformance.openbanking_brasil.testmodules.v2.payments;


import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.SetConsentsScopeOnTokenEndpointRequest;
import net.openid.conformance.condition.client.SetPaymentsScopeOnTokenEndpointRequest;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.paymentInitiation.PaymentInitiationConsentRequestValidator;
import net.openid.conformance.openbanking_brasil.testmodules.PreFlightCertCheckModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.payments.CheckConsentUrlForPayments;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "preflight-cert-check-payments-test-v2",
	displayName = "Pre-flight checks will validate the consentUrl along with the loggedUser and debtorAccount, the mTLS certificate before requesting an access token using the Directory client_id provided in the test configuration. Finally, an SSA will be generated using the Open Banking Brasil Directory.",
	summary = "Pre-flight checks will validate the consentUrl along with the loggedUser and debtorAccount, the mTLS certificate before requesting an access token using the Directory client_id provided in the test configuration. Finally, an SSA will be generated using the Open Banking Brasil Directory.",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"resource.consentUrl",
		"directory.client_id"
	}
)

public class PreFlightCertCheckPaymentsModuleV2 extends PreFlightCertCheckModule {

	@Override
	protected ConditionSequence createGetAccessTokenWithClientCredentialsSequence(Class<? extends ConditionSequence> clientAuthSequence) {
		return super.createGetAccessTokenWithClientCredentialsSequence(clientAuthSequence)
			.replace(SetConsentsScopeOnTokenEndpointRequest.class, condition(SetPaymentsScopeOnTokenEndpointRequest.class));
	}

	@Override
	protected void preConfigure(JsonObject config, String baseUrl, String externalUrlOverride) {
		callAndStopOnFailure(CheckConsentUrlForPayments.class);

		call(exec().startBlock("Validating brazilPaymentConsent"));
		JsonElement brazilPaymentConsentElem = env.getElementFromObject("config", "resource.brazilPaymentConsent");
		if (brazilPaymentConsentElem != null) {
			env.putObject("consent_endpoint_request", brazilPaymentConsentElem.getAsJsonObject());
		} else {
			env.putObject("consent_endpoint_request", null);
		}
		callAndContinueOnFailure(PaymentInitiationConsentRequestValidator.class, Condition.ConditionResult.FAILURE);
		call(exec().endBlock());

		call(exec().startBlock("Validating brazilQrdnPaymentConsent"));
		JsonElement brazilQrdnPaymentConsent = env.getElementFromObject("config", "resource.brazilQrdnPaymentConsent");
		if (brazilQrdnPaymentConsent != null) {
			env.putObject("consent_endpoint_request", brazilQrdnPaymentConsent.getAsJsonObject());
		} else {
			env.putObject("consent_endpoint_request", null);
		}
		callAndContinueOnFailure(PaymentInitiationConsentRequestValidator.class, Condition.ConditionResult.FAILURE);
		call(exec().endBlock());
		super.preConfigure(config, baseUrl, externalUrlOverride);
	}
}

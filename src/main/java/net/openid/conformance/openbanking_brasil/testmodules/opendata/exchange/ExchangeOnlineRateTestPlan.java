package net.openid.conformance.openbanking_brasil.testmodules.opendata.exchange;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.opendata.exchange.ExchangeOnlineRateValidatorV1RC2;
import net.openid.conformance.openbanking_brasil.testmodules.AbstractNoAuthFunctionalTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.opendata.utils.PrepareToGetOpenDataApi;
import net.openid.conformance.openbanking_brasil.testmodules.support.DoNotStopOnFailure;
import net.openid.conformance.openinsurance.testplan.utils.CallNoCacheResource;
import net.openid.conformance.plan.PublishTestPlan;
import net.openid.conformance.plan.TestPlan;
import net.openid.conformance.testmodule.PublishTestModule;
import net.openid.conformance.variant.ClientAuthType;

import java.util.List;

@PublishTestPlan(
	testPlanName = "Opendata Exchange - Online Rate API test plan",
	profile = OBBProfile.OBB_PROFIlE_PHASE4,
	displayName = "Functional tests for Exchange - Online Rate API - based on Swagger version: 1.0.0-rc2.0 (WIP)",
	summary = "Structural and logical tests for Exchange API"
)
public class ExchangeOnlineRateTestPlan implements TestPlan {
	public static List<ModuleListEntry> testModulesWithVariants() {
		return List.of(
			new ModuleListEntry(
				List.of(ExchangeOnlineRateTestModule.class),
				List.of(new Variant(ClientAuthType.class, "none"))
			)
		);
	}

	@PublishTestModule(
		testName = "Opendata - Exchange Online Rate API test",
		displayName = "Validate structure of Exchange Online Rate response",
		summary = "Validate structure of Exchange Online Rate response",
		profile = OBBProfile.OBB_PROFIlE_PHASE4
	)
	public static class ExchangeOnlineRateTestModule extends AbstractNoAuthFunctionalTestModule {

		@Override
		protected void runTests() {
			runInBlock("Validate ProductsNServices Exchange Online Rate response", () -> {
				callAndStopOnFailure(PrepareToGetOpenDataApi.class);
				callAndStopOnFailure(CallNoCacheResource.class);
				callAndContinueOnFailure(DoNotStopOnFailure.class);
				callAndContinueOnFailure(ExchangeOnlineRateValidatorV1RC2.class, Condition.ConditionResult.FAILURE);
			});
		}
	}
}

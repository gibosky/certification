package net.openid.conformance.openbanking_brasil.testmodules.support.yacs;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;

import java.util.Map;

public abstract class AbstractValidateEndpointExistsForAs extends AbstractCondition {


	@Override
	@PreEnvironment(required = "authorisation_server")
	public Environment evaluate(Environment env) {
		JsonObject authorisationServer = env.getObject("authorisation_server");
		if (authorisationServer == null){
			throw error("Authorisation Server not found");
		}
		JsonArray apiResources = authorisationServer.getAsJsonArray("ApiResources");
		var endpoints = getEndpoints();
		var tempEndpointMap = getEndpoints();

		if (apiResources != null){
			endpoints.forEach((familyType, version) -> {
			for (JsonElement entry : apiResources) {
				JsonObject apiResource = entry.getAsJsonObject();
				if (OIDFJSON.getString(apiResource.get("ApiFamilyType")).equals(familyType) && OIDFJSON.getString(apiResource.get("ApiVersion")).equals(version.get("version"))){
						var apiDiscoveryEndpoints = apiResource.getAsJsonArray("ApiDiscoveryEndpoints");
						for (JsonElement apiEndpoint: apiDiscoveryEndpoints){
							JsonObject apiEndpointObject = apiEndpoint.getAsJsonObject();
							if (OIDFJSON.getString(apiEndpointObject.get("ApiEndpoint")).endsWith(version.get("endpoint"))){
								logSuccess("Successfully found api endpoint: " + OIDFJSON.getString(apiEndpointObject.get("ApiEndpoint")));
							}
						}
						logSuccess("Successfully found api endpoint with correct version for: " + familyType);
						tempEndpointMap.remove(familyType);
				}
			}
			});
		} else {
			throw error("Authorisation Servers does not have any endpoints available");
		}
		if (!tempEndpointMap.isEmpty()){
			throw error("Unable to locate the defined endpoint(s)", args("Endpoints", tempEndpointMap));
		}
		logSuccess("Validated Authorisation Server contains API endpoints supporting the correct version of the API", args("Endpoints", endpoints));
		return env;
	}


	protected abstract Map<String, Map<String, String>> getEndpoints();
}



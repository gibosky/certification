package net.openid.conformance.openbanking_brasil.testmodules.v2.resources;

import com.google.gson.JsonObject;
import net.openid.conformance.ConditionSequenceRepeater;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.AddFAPIAuthDateToResourceEndpointRequest;
import net.openid.conformance.condition.client.AddFAPIInteractionIdToResourceEndpointRequest;
import net.openid.conformance.condition.client.AddIpV4FapiCustomerIpAddressToResourceEndpointRequest;
import net.openid.conformance.condition.client.CallProtectedResource;
import net.openid.conformance.condition.client.CheckForDateHeaderInResourceResponse;
import net.openid.conformance.condition.client.CheckForFAPIInteractionIdInResourceResponse;
import net.openid.conformance.condition.client.CreateEmptyResourceEndpointRequestHeaders;
import net.openid.conformance.condition.client.CreateRandomFAPIInteractionId;
import net.openid.conformance.condition.client.EnsureContentTypeJson;
import net.openid.conformance.condition.client.EnsureHttpStatusCodeIs201;
import net.openid.conformance.condition.client.EnsureMatchingFAPIInteractionId;
import net.openid.conformance.condition.client.EnsureResourceResponseReturnedJsonContentType;
import net.openid.conformance.condition.client.ExtractConsentIdFromConsentEndpointResponse;
import net.openid.conformance.condition.client.FAPIBrazilAddConsentIdToClientScope;
import net.openid.conformance.condition.client.FAPIBrazilConsentEndpointResponseValidatePermissions;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.generic.ErrorValidator;
import net.openid.conformance.openbanking_brasil.resourcesAPI.v1.PrepareUrlForResourcesCall;
import net.openid.conformance.openbanking_brasil.resourcesAPI.v2.EnsureUnavailableResourceIsNotOnList;
import net.openid.conformance.openbanking_brasil.resourcesAPI.v2.PrepareUrlForApiListForSavedResourceCall;
import net.openid.conformance.openbanking_brasil.resourcesAPI.v2.PrepareUrlForSavedResourceCall;
import net.openid.conformance.openbanking_brasil.resourcesAPI.v2.ResourcesResponseValidatorV2;
import net.openid.conformance.openbanking_brasil.resourcesAPI.v2.SaveUnavailableResourceData;
import net.openid.conformance.openbanking_brasil.resourcesAPI.v2.UpdateSavedResourceData;
import net.openid.conformance.openbanking_brasil.testmodules.creditOperations.financing.AddScopesForFinancingsApi;
import net.openid.conformance.openbanking_brasil.testmodules.customerAPI.AddScopesForCustomerApi;
import net.openid.conformance.openbanking_brasil.testmodules.support.AddAccountScope;
import net.openid.conformance.openbanking_brasil.testmodules.support.AddCreditCardScopes;
import net.openid.conformance.openbanking_brasil.testmodules.support.AddInvoiceFinancingsScope;
import net.openid.conformance.openbanking_brasil.testmodules.support.AddLoansScope;
import net.openid.conformance.openbanking_brasil.testmodules.support.AddResourcesScope;
import net.openid.conformance.openbanking_brasil.testmodules.support.AddUnarrangedOverdraftScope;
import net.openid.conformance.openbanking_brasil.testmodules.support.BuildResourcesConfigResourceUrlFromConsentUrl;
import net.openid.conformance.openbanking_brasil.testmodules.support.ChuckWarning;
import net.openid.conformance.openbanking_brasil.testmodules.support.EnsureNotOnlyCustomerDataPermissionsWereReturned;
import net.openid.conformance.openbanking_brasil.testmodules.support.EnsureResponseCodeWas200;
import net.openid.conformance.openbanking_brasil.testmodules.support.EnsureResponseCodeWas403;
import net.openid.conformance.openbanking_brasil.testmodules.support.EnsureResponseHasLinksForConsents;
import net.openid.conformance.openbanking_brasil.testmodules.support.ForceToValidateConsentResponse;
import net.openid.conformance.openbanking_brasil.testmodules.support.PrepareAllResourceRelatedConsentsForHappyPathTest;
import net.openid.conformance.openbanking_brasil.testmodules.support.ResourceApiV2PollingSteps;
import net.openid.conformance.openbanking_brasil.testmodules.support.ValidateResponseMetaData;
import net.openid.conformance.openbanking_brasil.testmodules.support.preCallProtectedResourceSteps;
import net.openid.conformance.openbanking_brasil.testmodules.support.warningMessages.UnavailableResourcesApiPollingTimeout;
import net.openid.conformance.openbanking_brasil.testmodules.v2.AbstractPhase2V2TestModule;
import net.openid.conformance.openbanking_brasil.testmodules.v2.GenerateRefreshAccessTokenSteps;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.sequence.client.OpenBankingBrazilPreAuthorizationSteps;
import net.openid.conformance.testmodule.PublishTestModule;
import net.openid.conformance.testmodule.TestFailureException;
import net.openid.conformance.variant.ClientAuthType;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.VariantHidesConfigurationFields;

@PublishTestModule(
	testName = "resources-api-test-unavailable-v2",
	displayName = "Validates that the server has correctly implemented the expected behaviour for temporarily blocked resources",
	summary = "Validates that the server has correctly implemented the expected behaviour for temporarily blocked resources\n" +
		"\u2022 Creates a CONSENT with all the existing permissions including either business or personal data, depending on what has been provided on the test configuration\n" +
		"\u2022 Expects a Success 201\n" +
		"\u2022 Redirect the user to authorize the CONSENT - Redirect URI must contain all phase 2 scopes\n" +
		"\u2022 Expect a Successful authorization with an authorization code created\n" +
		"\u2022 POLL the GET RESOURCES API with the authorized consent for 10 minutes - Refresh Token to be called each 3 minutes\n" +
		"\u2022 Continue pooling until AT LEAST one Resource returned and is on the state TEMPORARILY_UNAVAILABLE/UNAVAILABLE\n" +
		"\u2022 Evaluate which Resource is on the TEMPORARILY_UNAVAILABLE/UNAVAILABLE state, fetch the resource id, create the base request URI for said resource\n" +
		"\u2022 Call either the CONTRACTS or the ACCOUNTS list API for this Resource\n" +
		"\u2022 Expect a 200 - Make sure the Server returns a 200 without that TEMPORARILY_UNAVAILABLE/UNAVAILABLE resource on it's list\n" +
		"\u2022 Depending on the unavailable resource, call one of the following APIs depending: (1) /contracts/{contractId}/warranties for credit operations, (2) /accounts/{creditCardAccountId}/bills for credit cards, or (3) /accounts/{accountId}/balances for accounts\n" +
		"\u2022 Expect a 403 - Validate that the field response.errors.code is STATUS_RESOURCE_TEMPORARILY_UNAVAILABLE/STATUS_RESOURCE_UNAVAILABLE\n" +
		"\u2022 \n",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"resource.consentUrl",
		"resource.brazilCpf",
		"consent.productType"
	}
)
@VariantHidesConfigurationFields(parameter = FAPI1FinalOPProfile.class, value = "openbanking_brazil", configurationFields = {
	"client.org_jwks"
})
public class ResourcesApiTestModuleUnavailableV2 extends AbstractPhase2V2TestModule {

	private ClientAuthType clientAuthType;

	@Override
	protected void configureClient() {
		callAndStopOnFailure(BuildResourcesConfigResourceUrlFromConsentUrl.class);
		super.configureClient();
	}

	@Override
	protected void onConfigure(JsonObject config, String baseUrl) {
		super.onConfigure(config, baseUrl);
		callAndStopOnFailure(AddAccountScope.class);
		callAndStopOnFailure(AddCreditCardScopes.class);
		callAndStopOnFailure(AddScopesForCustomerApi.class);
		callAndStopOnFailure(AddInvoiceFinancingsScope.class);
		callAndStopOnFailure(AddScopesForFinancingsApi.class);
		callAndStopOnFailure(AddLoansScope.class);
		callAndStopOnFailure(AddUnarrangedOverdraftScope.class);
		callAndStopOnFailure(AddResourcesScope.class);

		callAndStopOnFailure(PrepareAllResourceRelatedConsentsForHappyPathTest.class);
		callAndStopOnFailure(PrepareUrlForResourcesCall.class);

		clientAuthType = getVariant(ClientAuthType.class);
		super.onConfigure(config, baseUrl);
	}


	@Override
	protected ConditionSequence createOBBPreauthSteps() {
		return new OpenBankingBrazilPreAuthorizationSteps(false, false, addTokenEndpointClientAuthentication, false, false, false);
	}

	@Override
	protected void performPreAuthorizationSteps() {
		super.performPreAuthorizationSteps();
		callAndStopOnFailure(ForceToValidateConsentResponse.class);
		env.mapKey("endpoint_response", "consent_endpoint_response_full");
		callAndContinueOnFailure(EnsureHttpStatusCodeIs201.class, Condition.ConditionResult.FAILURE);
		callAndContinueOnFailure(EnsureNotOnlyCustomerDataPermissionsWereReturned.class, Condition.ConditionResult.WARNING);
		if (getResult() == Result.WARNING) {
			fireTestFinished();
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				Thread.currentThread().interrupt();
				throw new TestFailureException(getId(), "Thread.sleep threw exception: " + e.getMessage());
			}

		} else {
			callAndContinueOnFailure(EnsureContentTypeJson.class, Condition.ConditionResult.FAILURE);
			env.unmapKey("endpoint_response");
			callAndContinueOnFailure(FAPIBrazilConsentEndpointResponseValidatePermissions.class, Condition.ConditionResult.INFO);
			callAndContinueOnFailure(EnsureResponseHasLinksForConsents.class, Condition.ConditionResult.FAILURE);
			callAndContinueOnFailure(ValidateResponseMetaData.class, Condition.ConditionResult.FAILURE);
			callAndStopOnFailure(ExtractConsentIdFromConsentEndpointResponse.class);
			callAndContinueOnFailure(CheckForFAPIInteractionIdInResourceResponse.class, Condition.ConditionResult.FAILURE, "FAPI-R-6.2.1-11", "FAPI1-BASE-6.2.1-11");
			callAndStopOnFailure(FAPIBrazilAddConsentIdToClientScope.class);
			eventLog.endBlock();
		}
	}

	@Override
	protected void requestProtectedResource() {
		eventLog.startBlock(currentClientString() + "Resource server endpoint tests");
		callAndStopOnFailure(CreateEmptyResourceEndpointRequestHeaders.class);
		callAndStopOnFailure(AddFAPIAuthDateToResourceEndpointRequest.class);
		callAndStopOnFailure(AddIpV4FapiCustomerIpAddressToResourceEndpointRequest.class);
		callAndStopOnFailure(CreateRandomFAPIInteractionId.class);
		callAndStopOnFailure(AddFAPIInteractionIdToResourceEndpointRequest.class);
		callAndStopOnFailure(CallProtectedResource.class, "FAPI1-BASE-6.2.1-1", "FAPI1-BASE-6.2.1-3");
		call(exec().mapKey("endpoint_response", "resource_endpoint_response_full"));
		callAndStopOnFailure(CheckForDateHeaderInResourceResponse.class);
		callAndStopOnFailure(CheckForFAPIInteractionIdInResourceResponse.class);
		callAndStopOnFailure(EnsureMatchingFAPIInteractionId.class);
		callAndStopOnFailure(EnsureResourceResponseReturnedJsonContentType.class);

		eventLog.endBlock();

		eventLog.startBlock(currentClientString() + "Validate response");
		validateResponse();
		eventLog.endBlock();
	}

	@Override
	protected void validateResponse() {

		ResourceApiV2PollingSteps pollingSteps = new ResourceApiV2PollingSteps(env, getId(),
			eventLog, testInfo, getTestExecutionManager());
		runInBlock("Polling Resources API", () -> call(pollingSteps));

		callAndContinueOnFailure(ResourcesResponseValidatorV2.class, Condition.ConditionResult.FAILURE);

		ConditionSequenceRepeater sequenceRepeater = new ConditionSequenceRepeater(env, getId(), eventLog, testInfo, executionManager,
			() -> new preCallProtectedResourceSteps()
				.then(condition(SaveUnavailableResourceData.class)
					.dontStopOnFailure()
					.onFail(Condition.ConditionResult.INFO))
		)
			.untilTrue("resource_found")
			.times(10)
			.trailingPause(60)
			.refreshSequence(() -> new GenerateRefreshAccessTokenSteps(clientAuthType), 3)
			.onTimeout(sequenceOf(condition(UnavailableResourcesApiPollingTimeout.class), condition(ChuckWarning.class)));
		sequenceRepeater.run();

		runInBlock("Ensure we cannot see the given unavailable resource in its api list.", () -> {
			callAndStopOnFailure(UpdateSavedResourceData.class);
			callAndStopOnFailure(PrepareUrlForApiListForSavedResourceCall.class);
			callAndContinueOnFailure(CallProtectedResource.class, Condition.ConditionResult.FAILURE);
			callAndContinueOnFailure(EnsureResponseCodeWas200.class, Condition.ConditionResult.FAILURE);
			callAndContinueOnFailure(EnsureUnavailableResourceIsNotOnList.class, Condition.ConditionResult.FAILURE);
		});

		runInBlock("Ensure we cannot access the unavailable resource.", () -> {
			callAndStopOnFailure(PrepareUrlForSavedResourceCall.class);
			callAndContinueOnFailure(CallProtectedResource.class, Condition.ConditionResult.FAILURE);
			callAndContinueOnFailure(EnsureResponseCodeWas403.class);
			callAndContinueOnFailure(ErrorValidator.class, Condition.ConditionResult.FAILURE);
		});
	}

}

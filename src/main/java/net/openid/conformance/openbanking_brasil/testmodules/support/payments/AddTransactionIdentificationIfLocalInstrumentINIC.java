package net.openid.conformance.openbanking_brasil.testmodules.support.payments;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;

import java.util.Objects;

public class AddTransactionIdentificationIfLocalInstrumentINIC extends AbstractCondition {

	@Override
	@PreEnvironment(required = "resource")
	public Environment evaluate(Environment env) {
		JsonObject payment = env.getElementFromObject("resource", "brazilPixPayment").getAsJsonObject();
		JsonObject paymentData = payment.getAsJsonObject("data");
		if (Objects.equals(OIDFJSON.getString(paymentData.get("localInstrument")), "INIC")) {
			paymentData.addProperty("transactionIdentification", DictHomologKeys.PROXY_TRANSACTION_IDENTIFICATION);
			logSuccess("Added transactionIdentification to payment data.", args("data", paymentData));
		} else {
			logSuccess("Nothing happened as localInstrument is not INIC.");
		}
		return env;
	}
}

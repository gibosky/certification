package net.openid.conformance.openbanking_brasil.testmodules.v2.operationalLimits;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.account.v2.AccountBalancesResponseValidatorV2;
import net.openid.conformance.openbanking_brasil.account.v2.AccountIdentificationResponseValidatorV2;
import net.openid.conformance.openbanking_brasil.account.v2.AccountLimitsValidatorV2;
import net.openid.conformance.openbanking_brasil.account.v2.AccountListValidatorV2;
import net.openid.conformance.openbanking_brasil.account.v2.AccountTransactionsCurrentValidatorV2;
import net.openid.conformance.openbanking_brasil.account.v2.AccountTransactionsValidatorV2;
import net.openid.conformance.openbanking_brasil.resourcesAPI.EnumResourcesType;
import net.openid.conformance.openbanking_brasil.testmodules.ExtractSpecifiedResourceApiAccountIdsFromConfig;
import net.openid.conformance.openbanking_brasil.testmodules.account.BuildAccountsConfigResourceUrlFromConsentUrl;
import net.openid.conformance.openbanking_brasil.testmodules.account.PrepareAllAccountRelatedConsentsForHappyPathTest;
import net.openid.conformance.openbanking_brasil.testmodules.account.PrepareUrlForFetchingAccountBalances;
import net.openid.conformance.openbanking_brasil.testmodules.account.PrepareUrlForFetchingAccountResource;
import net.openid.conformance.openbanking_brasil.testmodules.account.PrepareUrlForFetchingAccountTransactionLimit;
import net.openid.conformance.openbanking_brasil.testmodules.account.PrepareUrlForFetchingAccountTransactions;
import net.openid.conformance.openbanking_brasil.testmodules.account.PrepareUrlForFetchingAccountTransactionsCurrent;
import net.openid.conformance.openbanking_brasil.testmodules.support.*;
import net.openid.conformance.openbanking_brasil.testmodules.support.deprecated.ValidateMetaOnlyRequestDateTimeDeprecated;
import net.openid.conformance.testmodule.OIDFJSON;
import net.openid.conformance.testmodule.PublishTestModule;
import net.openid.conformance.variant.ClientAuthType;

import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

@PublishTestModule(
	testName = "accounts-api-operational-limits",
	displayName = "Accounts Api operational limits test module",
	summary = "This test will require the user to have set at least one ACTIVE resources each with at least 20 Transactions to be returned on the transactions endpoint. The tested resource must be sent on the field FirstAccountId (R_1) on the test configuration and on SecondAccountId (R_2), in case the company supports multiple accounts for the same user. If the server does not provide the SecondAccountId (R_2) the test will skip this part of the test \n" +
		"Make Sure that the fields “Client_id for Operational Limits Test” (client_id for OL), the CPF for Operational Limits (CPF for OL) and at least the FirstAccountId (R_1) have been provided \n" +
		"\u2022 Using the HardCoded clients provided on the test summary link, use the client_id for OL and the CPF/CNPJ for OL passed on the configuration and create a Consent Request sending the Accounts permission group - Expect Server to return a 201 - Save ConsentID (1) \n" +
		"\u2022 Return a Success if Consent Response is a 201 containing all permissions required on the scope of the test. Return a Warning and end the test if the consent request returns either a 422 or a 201 without Permission for this specific test.\n" +
		"\u2022 With the authorized consent id (1), call the GET Accounts API with the saved Resource ID (R_1) 4 Times - Expect a 200 response\n" +
		"\u2022 With the authorized consent id (1), call the GET Accounts Transactions API with the saved Resource ID (R_1) 4 Times, send query parameters fromBookingDate as D-6 and toBookingDate as Today - Expect a 200 response - Make Sure That at least 20 Transactions have been returned \n" +
		"\u2022 With the authorized consent id (1), call the GET Accounts Balances API with the saved Resource ID (R_1) 420 Times  - Expect a 200 response. Every 100 calls the Access token is refreshed.\n" +
		"\u2022 With the authorized consent id (1), call the GET Accounts Limits API with the saved Resource ID (R_1) 420 Times  - Expect a 200 response. Every 100 calls the Access token is refreshed.\n" +
		"\u2022 With the authorized consent id (1), call the GET Accounts Transactions-Current API with the saved Resource ID (R_1) 240 Times  - Expect a 200 response. Every 100 calls the Access token is refreshed.\n" +
		"\u2022 With the authorized consent id (1), refresh Access Token using the Refresh Token" +
		"\u2022 With the authorized consent id (1), call the GET Accounts Transactions-Current API with the saved Resource ID (R_1) Once,  send query parameters fromBookingDate as D-6 and toBookingDate as Today, with page-size=1 - Expect a 200 response - Fetch the links.next URI \n" +
		"\u2022 Call the GET Accounts Transactions-Current API 19 more times, always using the links.next returned and always forcing the page-size to be equal to 1 - Expect a 200 on every single call \n" +
		"\u2022 With the authorized consent id (1), call the GET Accounts API with the saved Resource ID (R_2) once - Expect a 200 response \n" +
		"\u2022 If the field SecondAccountId (R_2) has been returned, repeat  the exact same process done with the first tested resources (R_1) but now, execute it against the second returned Resource (R_2) \n",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"resource.consentUrl",
		"resource.brazilCpfOperational",
		"resource.brazilCnpjOperationalBusiness",
		"resource.first_account_id",
		"resource.second_account_id"
	}
)
public class AccountsApiOperationalLimitsTestModule extends AbstractOperationalLimitsTestModule {
	private static final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd");
	private static final int REQUIRED_NUMBER_OF_RECORDS = 20;
	private static final String API_RESOURCE_ID = "accountId";
	private static final int NUMBER_OF_IDS_TO_FETCH = 2;

	@Override
	protected void configureClient() {
		callAndStopOnFailure(BuildAccountsConfigResourceUrlFromConsentUrl.class);
		super.configureClient();
	}

	@Override
	protected void onConfigure(JsonObject config, String baseUrl) {
		env.putString("api_type", EnumResourcesType.ACCOUNT.name());
		callAndContinueOnFailure(ExtractSpecifiedResourceApiAccountIdsFromConfig.class, Condition.ConditionResult.INFO);
		callAndStopOnFailure(AddAccountScope.class);
		callAndStopOnFailure(PrepareAllAccountRelatedConsentsForHappyPathTest.class);
		callAndContinueOnFailure(OperationalLimitsToConsentRequest.class);
		clientAuthType = getVariant(ClientAuthType.class);
		super.onConfigure(config, baseUrl);
	}


	@Override
	protected void requestProtectedResource() {
		JsonArray fetchedApiIds = prepareApiIds();

		for (int i = 0; i < NUMBER_OF_IDS_TO_FETCH; i++) {

			// If during the second resource test part, fetchedApiIds size is less than 2 it means that the second account ID was not provided by the client.
			// In that case we assume that the multiple accounts for the same users feature is not supported, therefore we skip the part of the test with the second resource ID.
			if (i == 1 && fetchedApiIds.size() < NUMBER_OF_IDS_TO_FETCH) {
				eventLog.log(getName(), "Could not find second account ID. Skipping part of the test with the second resource ID");
				continue;
			}

			String accountId = OIDFJSON.getString(fetchedApiIds.get(i));
			env.putString(API_RESOURCE_ID, accountId);
			accountsOperationalLimitCalls();
		}
	}

	private JsonArray prepareApiIds() {
		JsonArray fetchedApiIds = env.getObject("fetched_api_ids").getAsJsonArray("fetchedApiIds");

		// IF no resource IDs were provided in the config, we will fetch instead
		if (fetchedApiIds.isEmpty()) {

			preCallProtectedResource("Fetching Accounts");
			// Validate accounts  response
			runInBlock("Validating Accounts response", () -> {
				callAndStopOnFailure(AccountListValidatorV2.class);
				callAndStopOnFailure(ValidateResponseMetaData.class);

				env.putString("apiIdName", API_RESOURCE_ID);
				callAndStopOnFailure(ExtractAllSpecifiedApiIds.class);

				JsonArray extractedApiIds = env.getObject("extracted_api_ids").getAsJsonArray("extractedApiIds");

				if (extractedApiIds.size() >= NUMBER_OF_IDS_TO_FETCH) {
					env.putInteger("number_of_ids_to_fetch", NUMBER_OF_IDS_TO_FETCH);
				} else {
					env.putInteger("number_of_ids_to_fetch", 1);
				}

				callAndStopOnFailure(FetchSpecifiedNumberOfExtractedApiIds.class);
			});

			fetchedApiIds = env.getObject("fetched_api_ids").getAsJsonArray("fetchedApiIds");
		}
		return fetchedApiIds;
	}

	@Override
	protected void validateResponse() {
		// Not needed for this test
	}

	private void accountsOperationalLimitCalls() {
		//Call Account resource
		callAndStopOnFailure(PrepareUrlForFetchingAccountResource.class);

		for (int i = 0; i < 4; i++) {
			preCallProtectedResource(String.format("[%d] Fetching First Account", i + 1));
			if (i == 0) {
				runInLoggingBlock(() -> runInBlock("Validate Account Response", () -> {
					callAndContinueOnFailure(AccountIdentificationResponseValidatorV2.class, Condition.ConditionResult.FAILURE);
					callAndStopOnFailure(ValidateResponseMetaData.class);
				}));

			}
		}

		makeOverOlCall("Account");

		enableLogging();
		callAndStopOnFailure(PrepareUrlForFetchingAccountTransactions.class);

		LocalDate currentDate = LocalDate.now(ZoneId.of("America/Sao_Paulo"));
		env.putString("fromBookingDate", currentDate.minusDays(6).format(FORMATTER));
		env.putString("toBookingDate", currentDate.format(FORMATTER));

		callAndStopOnFailure(AddToAndFromBookingDateParametersToProtectedResourceUrl.class);

		env.putInteger("required_number_of_records", REQUIRED_NUMBER_OF_RECORDS);
		for (int i = 0; i < 4; i++) {
			preCallProtectedResource(String.format("[%d] Fetching transactions with booking date parameters", i + 1));

			if (i == 0) {
				runInLoggingBlock(() -> runInBlock("Validate Account Transactions Response", () -> {
					callAndContinueOnFailure(AccountTransactionsValidatorV2.class, Condition.ConditionResult.FAILURE);
					callAndContinueOnFailure(ValidateMetaOnlyRequestDateTimeDeprecated.class);

					call(condition(VerifyAdditionalFieldsWhenMetaOnlyRequestDateTime.class)
						.dontStopOnFailure()
						.onFail(Condition.ConditionResult.WARNING));

					callAndStopOnFailure(EnsureAtLeastSpecifiedNumberOfRecordsWereReturned.class);
				}));
			}
		}

		makeOverOlCall("Transactions");

		enableLogging();
		callAndStopOnFailure(PrepareUrlForFetchingAccountBalances.class);

		for (int i = 0; i < 420; i++) {
			preCallProtectedResource(String.format("[%d] Fetching balances", i + 1));
			validateFields(i, "Validate Account Transactions Balances", AccountBalancesResponseValidatorV2.class);
		}

		makeOverOlCall("Balances");

		enableLogging();
		callAndStopOnFailure(PrepareUrlForFetchingAccountTransactionLimit.class);

		for (int i = 0; i < 420; i++) {
			preCallProtectedResource(String.format("[%d] Fetching accounts limits", i + 1));
			validateFields(i, "Validate accounts limits Balances", AccountLimitsValidatorV2.class);
		}

		makeOverOlCall("Trans. Limits");

		enableLogging();
		callAndStopOnFailure(PrepareUrlForFetchingAccountTransactionsCurrent.class);

		for (int i = 0; i < 240; i++) {
			preCallProtectedResource(String.format("[%d] Fetching Account Transactions current", i + 1));
			validateFields(i, "Validate Account Transactions current", AccountTransactionsCurrentValidatorV2.class);
		}

		enableLogging();
		env.putString("fromBookingDate", currentDate.minusDays(6).format(FORMATTER));
		env.putString("toBookingDate", currentDate.format(FORMATTER));

		callAndStopOnFailure(AddToAndFromBookingDateParametersToProtectedResourceUrl.class);
		env.putInteger("required_page_size", 1);
		callAndContinueOnFailure(AddSpecifiedPageSizeParameterToProtectedResourceUrl.class);

		enableLogging();
		for (int i = 0; i < REQUIRED_NUMBER_OF_RECORDS; i++) {
			eventLog.startBlock(String.format("[%d] Fetching Accounts Transactions Current next link", i + 1));
			preCallProtectedResource();

			if (i == 0) {
				callAndContinueOnFailure(AccountTransactionsCurrentValidatorV2.class, Condition.ConditionResult.FAILURE);
				callAndContinueOnFailure(ValidateMetaOnlyRequestDateTimeDeprecated.class);

				call(condition(VerifyAdditionalFieldsWhenMetaOnlyRequestDateTime.class)
					.dontStopOnFailure()
					.onFail(Condition.ConditionResult.WARNING));

				callAndStopOnFailure(ValidateNumberOfRecordsPage1.class);
			}

			if (i == 1) {
				callAndStopOnFailure(ValidateNumberOfRecordsPage2.class);
			}

			callAndStopOnFailure(EnsureOnlyOneRecordWasReturned.class);
			callAndStopOnFailure(ExtractNextLink.class);

			env.putString("value", "1");
			env.putString("parameter", "page-size");
			callAndStopOnFailure(SetSpecifiedValueToSpecifiedUrlParameter.class);
			env.putString("protected_resource_url", env.getString("extracted_link"));

			eventLog.endBlock();
			disableLogging();
		}

		makeOverOlCall("Trans. Current");
		enableLogging();
	}

	private void validateFields(int i, String message, Class<? extends Condition> conditionClass) {
		if (i == 0) {
			runInLoggingBlock(() -> runInBlock(message, () -> callAndStopOnFailure(conditionClass, Condition.ConditionResult.FAILURE)));
		}
	}

}

package net.openid.conformance.openbanking_brasil.testmodules;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.AbstractFunctionalTestModule;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.*;
import net.openid.conformance.openbanking_brasil.paymentInitiation.PaymentInitiationConsentValidator;
import net.openid.conformance.openbanking_brasil.testmodules.support.AddJWTAcceptHeader;
import net.openid.conformance.openbanking_brasil.testmodules.support.AddPaymentConsentRequestBodyToConfig;
import net.openid.conformance.openbanking_brasil.testmodules.support.ExpectJWTResponse;
import net.openid.conformance.openbanking_brasil.testmodules.support.FAPIBrazilCallPaymentConsentEndpointWithBearerTokenAnyMethod;
import net.openid.conformance.openbanking_brasil.testmodules.support.PaymentConsentIdExtractor;
import net.openid.conformance.openbanking_brasil.testmodules.support.PrepareToFetchConsentRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentConsentStatus.EnsurePaymentConsentStatusWasAuthorised;
import net.openid.conformance.sequence.client.PerformStandardIdTokenChecks;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.VariantNotApplicable;

@VariantNotApplicable(parameter = FAPI1FinalOPProfile.class, values = {"openbanking_uk", "plain_fapi", "consumerdataright_au"})
public abstract class AbstractOBBrasilFunctionalTestModule extends AbstractFunctionalTestModule {

	protected void runInBlock(String blockText, Runnable actor) {
		eventLog.startBlock(blockText);
		actor.run();
		eventLog.endBlock();
	}

	@Override
	protected void performPreAuthorizationSteps() {
		super.performPreAuthorizationSteps();
		if (brazilPayments.isTrue()) {
			callAndStopOnFailure(PaymentInitiationConsentValidator.class);
		}
	}

	@Override
	protected void setupResourceEndpoint() {
		if(isBrazil.isTrue() && scopeContains("payments")){
			callAndStopOnFailure(AddPaymentConsentRequestBodyToConfig.class);
		}
		super.setupResourceEndpoint();
	}

	@Override
	protected void performPostAuthorizationFlow() {
		if (brazilPayments.isTrue()) {
			fetchConsentToCheckAuthorisedStatus();
			validateGetConsentResponse();
		}
		super.performPostAuthorizationFlow();
	}

	protected void fetchConsentToCheckAuthorisedStatus() {
		eventLog.startBlock("Checking the created consent - Expecting AUTHORISED status");
		callAndStopOnFailure(PaymentConsentIdExtractor.class);
		callAndStopOnFailure(AddJWTAcceptHeader.class);
		callAndStopOnFailure(ExpectJWTResponse.class);
		callAndStopOnFailure(PrepareToFetchConsentRequest.class);
		callAndContinueOnFailure(FAPIBrazilCallPaymentConsentEndpointWithBearerTokenAnyMethod.class, Condition.ConditionResult.FAILURE);
		callAndContinueOnFailure(EnsurePaymentConsentStatusWasAuthorised.class, Condition.ConditionResult.WARNING);
		eventLog.endBlock();
	}

	protected void validateGetConsentResponse() {
		// No Validation
	}

	@Override
	protected void requestAuthorizationCode(){

			JsonObject AccessTokenRequest = env.getObject("token_endpoint_request_form_parameters");
			String grantType = AccessTokenRequest.get("grant_type").toString();

			callAndStopOnFailure(CallTokenEndpoint.class);

			eventLog.startBlock(currentClientString() + "Verify token endpoint response");

			callAndStopOnFailure(CheckIfTokenEndpointResponseError.class);

			callAndStopOnFailure(CheckForAccessTokenValue.class, "FAPI1-BASE-5.2.2-14");

			callAndStopOnFailure(ExtractAccessTokenFromTokenResponse.class);

			callAndContinueOnFailure(ExtractExpiresInFromTokenEndpointResponse.class, "RFC6749-5.1");
			skipIfMissing(new String[] { "expires_in" }, null, Condition.ConditionResult.INFO,
				ValidateExpiresIn.class, Condition.ConditionResult.FAILURE, "RFC6749-5.1");
			if (getVariant(FAPI1FinalOPProfile.class) == FAPI1FinalOPProfile.OPENBANKING_BRAZIL) {
				skipIfMissing(new String[] { "expires_in" }, null, Condition.ConditionResult.INFO,
					FAPIBrazilValidateExpiresIn.class, Condition.ConditionResult.FAILURE, "BrazilOB-5.2.2-13");
			}
			// scope is not *required* to be returned as the request was passed in signed request object - FAPI-R-5.2.2-15
			// https://gitlab.com/openid/conformance-suite/issues/617

			callAndContinueOnFailure(CheckForRefreshTokenValue.class);

			skipIfElementMissing("token_endpoint_response", "refresh_token", Condition.ConditionResult.INFO,
				EnsureMinimumRefreshTokenLength.class, Condition.ConditionResult.FAILURE, "RFC6749-10.10");

			skipIfElementMissing("token_endpoint_response", "refresh_token", Condition.ConditionResult.INFO,
				EnsureMinimumRefreshTokenEntropy.class, Condition.ConditionResult.FAILURE, "RFC6749-10.10");

			callAndContinueOnFailure(EnsureMinimumAccessTokenLength.class, Condition.ConditionResult.FAILURE, "FAPI1-BASE-5.2.2-16");

			callAndContinueOnFailure(EnsureMinimumAccessTokenEntropy.class, Condition.ConditionResult.FAILURE, "FAPI1-BASE-5.2.2-16");

			// Upon a successful validation of refresh token, it's response body might not contain an id_token.\
			if(grantType.equals("\"refresh_token\"")){
				JsonElement id_token = env.getElementFromObject("token_endpoint_response", "id_token");
				if(id_token != null && id_token.isJsonPrimitive()){
					callAndStopOnFailure(ExtractIdTokenFromTokenResponse.class, "FAPI1-BASE-5.2.2.1-6", "OIDCC-3.3.2.5");
					call(new PerformStandardIdTokenChecks());

					callAndContinueOnFailure(EnsureIdTokenContainsKid.class, Condition.ConditionResult.FAILURE, "OIDCC-10.1");

					performProfileIdTokenValidation();

					if (getVariant(FAPI1FinalOPProfile.class) == FAPI1FinalOPProfile.OPENBANKING_BRAZIL) {
						callAndContinueOnFailure(FAPIBrazilValidateIdTokenSigningAlg.class, Condition.ConditionResult.FAILURE, "BrazilOB-6.1-1");
					} else {
						callAndContinueOnFailure(FAPIValidateIdTokenSigningAlg.class, Condition.ConditionResult.FAILURE, "FAPI1-ADV-8.6");
					}
				}
			}else{
				callAndStopOnFailure(ExtractIdTokenFromTokenResponse.class, "FAPI1-BASE-5.2.2.1-6", "OIDCC-3.3.2.5");
				call(new PerformStandardIdTokenChecks());

				callAndContinueOnFailure(EnsureIdTokenContainsKid.class, Condition.ConditionResult.FAILURE, "OIDCC-10.1");

				performProfileIdTokenValidation();

				if (getVariant(FAPI1FinalOPProfile.class) == FAPI1FinalOPProfile.OPENBANKING_BRAZIL) {
					callAndContinueOnFailure(FAPIBrazilValidateIdTokenSigningAlg.class, Condition.ConditionResult.FAILURE, "BrazilOB-6.1-1");
				} else {
					callAndContinueOnFailure(FAPIValidateIdTokenSigningAlg.class, Condition.ConditionResult.FAILURE, "FAPI1-ADV-8.6");
				}
			}

			skipIfElementMissing("id_token", "jwe_header", Condition.ConditionResult.INFO,
				FAPIValidateIdTokenEncryptionAlg.class, Condition.ConditionResult.FAILURE,"FAPI1-ADV-8.6.1-1");
			if (getVariant(FAPI1FinalOPProfile.class) == FAPI1FinalOPProfile.CONSUMERDATARIGHT_AU) {
				callAndContinueOnFailure(ValidateIdTokenEncrypted.class, Condition.ConditionResult.FAILURE, "CDR-tokens");
			}

			performTokenEndpointIdTokenExtraction();
			callAndContinueOnFailure(ExtractAtHash.class, Condition.ConditionResult.INFO, "OIDCC-3.3.2.11");

			/* these all use 'INFO' if the field isn't present - whether the hash is a may/should/shall is
			 * determined by the Extract*Hash condition
			 */
			skipIfMissing(new String[]{"c_hash"}, null, Condition.ConditionResult.INFO,
				ValidateCHash.class, Condition.ConditionResult.FAILURE, "OIDCC-3.3.2.11");
			skipIfMissing(new String[]{"s_hash"}, null, Condition.ConditionResult.INFO,
				ValidateSHash.class, Condition.ConditionResult.FAILURE, "FAPI1-ADV-5.2.2.1-5");
			skipIfMissing(new String[]{"at_hash"}, null, Condition.ConditionResult.INFO,
				ValidateAtHash.class, Condition.ConditionResult.FAILURE, "OIDCC-3.3.2.11");

			if (!jarm.isTrue()) {
				eventLog.startBlock(currentClientString() + "Verify at_hash in the authorization endpoint id_token");

				env.mapKey("id_token", "authorization_endpoint_id_token");

				callAndContinueOnFailure(ExtractAtHash.class, Condition.ConditionResult.INFO, "OIDCC-3.3.2.11");

				skipIfMissing(new String[]{"at_hash"}, null, Condition.ConditionResult.INFO,
					ValidateAtHash.class, Condition.ConditionResult.FAILURE, "OIDCC-3.3.2.11");

				env.unmapKey("id_token");

				eventLog.endBlock();
			}
		}
	}



package net.openid.conformance.openbanking_brasil.testmodules.v2.yacs;

import com.google.gson.JsonObject;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.AbstractClientCredentialsGrantFunctionalTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.CallDirectoryParticipantsEndpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.EnsureBrazilCpf;
import net.openid.conformance.openbanking_brasil.testmodules.support.ValidateWellKnownUriSteps;
import net.openid.conformance.openbanking_brasil.testmodules.support.yacs.CallDirectoryParticipantsEndpointFromConfig;
import net.openid.conformance.openbanking_brasil.testmodules.support.yacs.EnsureDcrClientExists;
import net.openid.conformance.openbanking_brasil.testmodules.support.yacs.ValidateRegisteredEndpoints;
import net.openid.conformance.testmodule.PublishTestModule;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.VariantHidesConfigurationFields;

@PublishTestModule(
	testName = "fvp-preflight-check-test-v2",
	displayName = "Pre-Flights test that ensures that tested server accepted a DCR request and all the required tested URIs are registered within the Directory",
	summary = "Make sure the brazilCpf has been provided\n" +
		"\n" +
		"Make sure a client_id has been generated on the DCR\n" +
		"\n" +
		"Call the Tested Server Token endpoint with client_credentials grant - Make sure that server returns a 200\n" +
		"\n" +
		"Call the Directory participants endpoint for either Sandbox or Production, depending on the used platform\n" +
		"\n" +
		"Make sure The Provided Well-Known is registered within the Directory\n" +
		"\n" +
		"Make sure that the server has registered a ApiFamilyType of value consents with ApiVersion of value “2.0.1\". Return the ApiEndpointthat ends with /consents/v2/consents\n" +
		"\n" +
		"Make sure that the server has registered a ApiFamilyType of value resources with ApiVersion of value “2.0.1\". Return the ApiEndpointthat ends with /resources/v2/resources",
	profile = OBBProfile.OBB_PROFILE_PROD_FVP,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"resource.brazilCpf",
		"directory.client_id",
		"directory.Participants"
	}
)
@VariantHidesConfigurationFields(parameter = FAPI1FinalOPProfile.class, value = "openbanking_brazil", configurationFields = {
	"resource.brazilCnpj",
	"resource.consentUrl",

})
public class YACSPreFlightCertCheckV2Module extends AbstractClientCredentialsGrantFunctionalTestModule {

	@Override
	protected void preConfigure(JsonObject config, String baseUrl, String externalUrlOverride) {
		callAndStopOnFailure(EnsureBrazilCpf.class);
		env.putString("test_id", getId());
		callAndStopOnFailure(EnsureDcrClientExists.class);
		call(new ValidateWellKnownUriSteps().replace(CallDirectoryParticipantsEndpoint.class,condition(CallDirectoryParticipantsEndpointFromConfig.class)));
	}

	@Override
	protected void runTests() {
		call(new ValidateRegisteredEndpoints());
	}
}

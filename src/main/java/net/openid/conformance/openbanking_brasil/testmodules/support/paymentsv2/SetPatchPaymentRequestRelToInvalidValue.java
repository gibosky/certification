package net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;

import java.util.Optional;

public class SetPatchPaymentRequestRelToInvalidValue extends AbstractCondition {
	@Override
	@PreEnvironment(required = "resource")
	public Environment evaluate(Environment env) {
		JsonElement patchRequest = env.getElementFromObject("resource", "brazilPatchPixPayment");
		if(patchRequest == null || !patchRequest.isJsonObject()) {
			throw error("Could not load brazilPatchPixPayment from the resource");
		}
		JsonObject document = Optional.of(patchRequest.getAsJsonObject())
			.flatMap(request -> Optional.ofNullable(request.getAsJsonObject("data")))
			.flatMap(data -> Optional.ofNullable(data.getAsJsonObject("cancellation")))
			.flatMap(cancellation -> Optional.ofNullable(cancellation.getAsJsonObject("cancelledBy")))
			.flatMap(cancelledBy -> Optional.ofNullable(cancelledBy.getAsJsonObject("document")))
			.orElseThrow(() -> error("Could not extract data.cancellation.cancelledBy.document from brazilPatchPixPayment"))
			.getAsJsonObject();
		document.addProperty("rel", "XXX");
		logSuccess("Set PATCH payment request rel to invalid value", args("brazilPatchPixPayment", env.getElementFromObject("resource", "brazilPatchPixPayment").getAsJsonObject()));
		return env;
	}
}

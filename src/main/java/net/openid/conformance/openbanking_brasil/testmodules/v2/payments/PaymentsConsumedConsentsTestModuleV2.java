package net.openid.conformance.openbanking_brasil.testmodules.v2.payments;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.support.*;
import net.openid.conformance.openbanking_brasil.testmodules.support.payments.SanitiseQrCodeConfig;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.EnsureErrorResponseCodeFieldWasConsentimentoInvalido;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureErrorResponse.EnsureErrorResponseCodeFieldWasPagamentaDivergenteConsentimento;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentConsentStatus.EnsurePaymentConsentStatusWasAwaitingAuthorisation;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentConsentStatus.EnsurePaymentConsentStatusWasConsumed;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.validators.CreatePaymentErrorValidatorV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.validators.ValidateMetaOnlyRequestDateTime;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.testmodule.PublishTestModule;


@PublishTestModule(
	testName = "payments-api-consumed-consent-test-v2",
	displayName = "Payments API basic consumed consent test module",
	summary = "1. Ensure an error is sent when consent is reused at a failed payment flow\n" +
		"\u2022 Calls POST Consents Endpoint with valid payload, using DICT as the localInstrument\n" +
		"\u2022 Expects 201\n" +
		"\u2022 Call GET Consent\n" +
		"\u2022 Expects 200 - Validate if status is \"AWAITING_AUTHORISATION\"\n" +
		"\u2022 Redirects the user to authorize the created consent" +
		"\u2022 Call GET Consent\n" +
		"\u2022 Expects 200 - Validate if status is \"AUTHORISED\"\n" +
		"\u2022 Calls the POST Payments Endpoint with different amount\n" +
		"\u2022 Expects 422 PAGAMENTO_DIVERGENTE_CONSENTIMENTO \n" +
		"\u2022 Call GET Consent\n" +
		"\u2022 Expects 200 - Validate if status is \"CONSUMED\"\n" +
		"\u2022 Calls the POST Payments with a correct amount \n" +
		"\u2022 Expects 422 CONSENTIMENTO_INVALIDO - Validate error message\n" +
		"2. Ensure an error is sent when consent is reused at a success payment flow\n" +
		"\u2022 Calls POST Consents Endpoint with valid payload, using DICT as the localInstrument\n" +
		"\u2022 Expects 201 - Validate if status is \"AWAITING_AUTHORISATION\"\n" +
		"\u2022 Redirects the user to authorize the created consent    \n" +
		"\u2022 Call GET Consent\n" +
		"\u2022 Expects 200 - Validate if status is \"AUTHORISED\"\n" +
		"\u2022 Calls the POST Payments with valid payload\n" +
		"\u2022 Expects 201 - Validate Response    \n" +
		"\u2022 Poll the Get Payments endpoint with the PaymentID Created while payment status is RCVD, ACCP or ACPD\n" +
		"\u2022 Call GET Consent\n" +
		"\u2022 Expects 200 - Validate if status is \"CONSUMED\"\n" +
		"\u2022 Calls the POST Payments with the same consent\n" +
		"\u2022 Expects 422 CONSENTIMENTO_INVALIDO - Validate error message",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"resource.consentUrl"
	}
)
public class PaymentsConsumedConsentsTestModuleV2 extends AbstractOBBrasilQrCodePaymentFunctionalTestModuleV2 {

	private int batch = 1;
	@Override
	protected  void configureDictInfo(){
		callAndStopOnFailure(SelectDICTCodeLocalInstrument.class);
		callAndStopOnFailure(SanitiseQrCodeConfig.class);
		callAndStopOnFailure(ValidatePaymentAndConsentHaveSameProperties.class);
		eventLog.startBlock("Storing authorisation endpoint");
		callAndStopOnFailure(StoreScope.class);
		callAndStopOnFailure(SetIncorrectAmountInPayment.class);
	}

	@Override
	protected void onConfigure(JsonObject config, String baseUrl) {
		eventLog.startBlock("Setting date to today");
		callAndStopOnFailure(EnsurePaymentDateIsToday.class);
		callAndStopOnFailure(AddBrazilPixPaymentToTheResource.class);
		callAndStopOnFailure(PrepareToPostConsentRequest.class);
		callAndStopOnFailure(SetProtectedResourceUrlToPaymentsEndpoint.class);
		configureDictInfo();
	}

	@Override
	protected void performPreAuthorizationSteps() {
		env.mapKey("access_token", "saved_client_credentials");
		call(createOBBPreauthSteps());

		eventLog.startBlock("Checking the created consent - Expecting AWAITING_AUTHORISATION status");
		callAndStopOnFailure(PaymentConsentIdExtractor.class);
		callAndStopOnFailure(AddJWTAcceptHeader.class);
		callAndStopOnFailure(ExpectJWTResponse.class);
		callAndStopOnFailure(PrepareToFetchConsentRequest.class);
		callAndContinueOnFailure(FAPIBrazilCallPaymentConsentEndpointWithBearerTokenAnyMethod.class, Condition.ConditionResult.FAILURE);
		callAndContinueOnFailure(EnsurePaymentConsentStatusWasAwaitingAuthorisation.class, Condition.ConditionResult.FAILURE);

		runInBlock(currentClientString() + "Validate consents response", this::validateConsentResponse);
	}

	@Override
	protected void requestProtectedResource() {
		ConditionSequence unhappyPixSequence = getPixPaymentSequence()
			.replace(EnsureResponseCodeWas201.class, condition(EnsureResponseCodeWas400or422.class));
		switch (batch) {
			case 1: {
				batch++;
				eventLog.startBlock("Getting consent consumed through unhappy path (payment with different amount)");
				call(unhappyPixSequence);
				validateUnhappyResponse(EnsureErrorResponseCodeFieldWasPagamentaDivergenteConsentimento.class);
				eventLog.endBlock();
				eventLog.startBlock("Checking if consent was consumed");
				env.mapKey("access_token", "saved_client_credentials");
				callAndStopOnFailure(FAPIBrazilCallPaymentConsentEndpointWithBearerTokenAnyMethod.class);
				env.unmapKey("access_token");
				callAndContinueOnFailure(EnsurePaymentConsentStatusWasConsumed.class, Condition.ConditionResult.FAILURE);
				eventLog.endBlock();
				eventLog.startBlock("Trying to make payment again (expects failure, as consent is consumed)");
				callAndStopOnFailure(ResetPaymentRequest.class);
				call(unhappyPixSequence);
				validateUnhappyResponse(EnsureErrorResponseCodeFieldWasConsentimentoInvalido.class);
				eventLog.endBlock();
				break;
			}
			case 2: {
				batch++;
				eventLog.startBlock("Getting consent consumed through happy path");
				ConditionSequence pixSequence = getPixPaymentSequence();
				call(pixSequence);
				eventLog.startBlock(currentClientString() + "Validate response");
				validateResponse();
				eventLog.endBlock();
				eventLog.startBlock("Checking if consent was consumed");
				env.mapKey("access_token", "saved_client_credentials");
				callAndStopOnFailure(FAPIBrazilCallPaymentConsentEndpointWithBearerTokenAnyMethod.class);
				env.unmapKey("access_token");
				callAndStopOnFailure(EnsurePaymentConsentStatusWasConsumed.class);
				eventLog.endBlock();
				eventLog.startBlock("Trying to make payment again (expects failure, as consent is consumed)");
				call(unhappyPixSequence);
				validateUnhappyResponse(EnsureErrorResponseCodeFieldWasConsentimentoInvalido.class);
				eventLog.endBlock();
				break;
			}
		}
	}

	@Override
	protected void onPostAuthorizationFlowComplete() {
		if(batch==2) {
			eventLog.startBlock("2. Ensure error when the amount is different between consent and payment");
			validationStarted = false;
			callAndStopOnFailure(SetScope.class);
			performAuthorizationFlow();
		}
		else {
				fireTestFinished();
			}
		}

	protected void validateUnhappyResponse(Class<? extends Condition> errorValidator) {
		env.mapKey(ValidateMetaOnlyRequestDateTime.RESPONSE_ENV_KEY, "resource_endpoint_response_full");
		callAndStopOnFailure(errorValidator);
		env.putBoolean(ValidateMetaOnlyRequestDateTime.IS_META_OPTIONAL, true);
		callAndStopOnFailure(CreatePaymentErrorValidatorV2.class);
		callAndStopOnFailure(ValidateMetaOnlyRequestDateTime.class);
		env.putBoolean(ValidateMetaOnlyRequestDateTime.IS_META_OPTIONAL, false);
		env.unmapKey(ValidateMetaOnlyRequestDateTime.RESPONSE_ENV_KEY);
	}

}

package net.openid.conformance.openbanking_brasil.testmodules.v2.operationalLimits;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.registrationData.v2.BusinessIdentificationValidatorV2;
import net.openid.conformance.openbanking_brasil.registrationData.v2.BusinessQualificationResponseValidatorV2;
import net.openid.conformance.openbanking_brasil.registrationData.v2.BusinessRelationsResponseValidatorV2;
import net.openid.conformance.openbanking_brasil.testmodules.customerAPI.AddScopesForCustomerApi;
import net.openid.conformance.openbanking_brasil.testmodules.customerAPI.PrepareAllCustomerBusinessRelatedConsentsForHappyPathTest;
import net.openid.conformance.openbanking_brasil.testmodules.customerAPI.PrepareToGetBusinessFinancialRelations;
import net.openid.conformance.openbanking_brasil.testmodules.customerAPI.PrepareToGetBusinessQualifications;
import net.openid.conformance.openbanking_brasil.testmodules.support.AddDummyBusinessProductTypeToConfig;
import net.openid.conformance.openbanking_brasil.testmodules.support.BuildBusinessCustomersConfigResourceUrlFromConsentUrl;
import net.openid.conformance.openbanking_brasil.testmodules.support.OperationalLimitsToConsentRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.ValidateResponseMetaData;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.sequence.client.OpenBankingBrazilPreAuthorizationSteps;
import net.openid.conformance.testmodule.PublishTestModule;


@PublishTestModule(
	testName = "customer-business-api-operational-limits",
	displayName = "Make sure that the server is not blocking access to the APIs as long as the operational limits for the Customer Business API are considered correctly",
	summary ="This test will make sure that the server is not blocking access to the APIs as long as the operational limits for the Customer Business API are considered correctly.\n" +
		"\u2022 Make Sure that the fields “Client_id for Operational Limits Test” (client_id for OL) and at least the CPF for Operational Limits (CPF for OL) test have been provided\n" +
		"\u2022 Using the HardCoded clients provided on the test summary link, use the client_id for OL and the CPF/CNPJ for OL passed on the configuration and create a Consent Request sending the Customer Business permission group - Expect Server to return a 201 - Save ConsentID (1)\n" +
		"\u2022 Return a Success if Consent Response is a 201 containing all permissions required on the scope of the test. Return a Warning and end the test if the consent request returns either a 422 or a 201 without Permission for this specific test.\n" +
		"\u2022 With the authorized consent id (1), call the GET Customer Business Identifications API 4 Times - Expect a 200 response\n" +
		"\u2022 With the authorized consent id (1), call the GET Customer Business Qualifications 4 Times - Expect a 200 response\n" +
		"\u2022 With the authorized consent id (1), call the GET Customer Business Financial Relations 4 Times - Expect a 200 response\n",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"resource.consentUrl",
		"resource.brazilCpfOperational",
		"resource.brazilCnpjOperationalBusiness"
	}
)
public class CustomerBusinessApiOperationalLimitsTestModuleV2 extends AbstractOperationalLimitsTestModule {

	@Override
	protected void configureClient() {
		callAndContinueOnFailure(BuildBusinessCustomersConfigResourceUrlFromConsentUrl.class);
		super.configureClient();
	}

	@Override
	protected void onConfigure(JsonObject config, String baseUrl) {
		super.onConfigure(config, baseUrl);
		callAndStopOnFailure(AddScopesForCustomerApi.class);
		callAndStopOnFailure(PrepareAllCustomerBusinessRelatedConsentsForHappyPathTest.class);
		callAndStopOnFailure(AddDummyBusinessProductTypeToConfig.class);
		callAndContinueOnFailure(OperationalLimitsToConsentRequest.class);
	}


	@Override
	protected ConditionSequence createOBBPreauthSteps() {
		return new OpenBankingBrazilPreAuthorizationSteps(isSecondClient(), false, addTokenEndpointClientAuthentication, brazilPayments.isTrue(), false, false);
	}

	@Override
	protected void validateResponse() {
		// Validate Business Identification response
		callAndStopOnFailure(BusinessIdentificationValidatorV2.class);
		callAndStopOnFailure(ValidateResponseMetaData.class);

		eventLog.endBlock();

		// Call Business Identification 3 times
		disableLogging();
		for (int i = 1; i < 4; i++) {
			preCallProtectedResource(String.format("[%d] Calling Business Identification Endpoint", i + 1));
		}

		makeOverOlCall("Identification");

		// Call Business Qualifications once with validation
		runInLoggingBlock(() -> {
			callAndStopOnFailure(PrepareToGetBusinessQualifications.class);

			preCallProtectedResource("Calling Business Qualifications Endpoint");
			validateResponse("Validate Business Qualifications response", BusinessQualificationResponseValidatorV2.class);
		});

		// Call Business Qualifications 3 times
		for (int i = 1; i < 4; i++) {
			preCallProtectedResource(String.format("[%d] Calling Business Qualifications Endpoint", i + 1));
		}

		makeOverOlCall("Qualifications");



		// Call Customer Business Financial Relations once with validation
		runInLoggingBlock(() -> {
			callAndStopOnFailure(PrepareToGetBusinessFinancialRelations.class);

			preCallProtectedResource("Calling Customer Business Financial Relations Endpoint");
			validateResponse("Validate Customer Business Financial Relations response", BusinessRelationsResponseValidatorV2.class);

		});

		// Call Customer Business Financial Relations 3 times
		for (int i = 1; i < 4; i++) {
			preCallProtectedResource(String.format("[%d] Calling Customer Business Financial Relations Endpoint", i + 1));
		}

		makeOverOlCall("Fin Relations");


	}

	private void validateResponse(String message, Class<? extends Condition> validator) {
		runInBlock(message, () -> {
			callAndStopOnFailure(validator);
			callAndStopOnFailure(ValidateResponseMetaData.class);
		});
	}

}

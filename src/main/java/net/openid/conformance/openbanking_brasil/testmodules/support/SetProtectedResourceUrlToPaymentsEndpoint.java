package net.openid.conformance.openbanking_brasil.testmodules.support;

import net.openid.conformance.openbanking_brasil.testmodules.support.resource.ResourceBuilder;
import net.openid.conformance.testmodule.Environment;

public class SetProtectedResourceUrlToPaymentsEndpoint extends ResourceBuilder {

	@Override
	public Environment evaluate(Environment env) {
		setApi("payments");
		setEndpoint("/pix/payments");

		return super.evaluate(env);
	}
}

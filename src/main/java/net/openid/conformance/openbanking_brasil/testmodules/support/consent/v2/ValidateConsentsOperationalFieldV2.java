package net.openid.conformance.openbanking_brasil.testmodules.support.consent.v2;

import com.google.common.base.Strings;
import com.google.gson.JsonElement;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;

public class ValidateConsentsOperationalFieldV2 extends AbstractJsonAssertingCondition {
    @Override
	@PreEnvironment(required = "config")
	public Environment evaluate(Environment env) {

		JsonElement config = env.getObject("config");

		String productType = env.getString("config", "consent.productType");
		if (Strings.isNullOrEmpty(productType)) {
			throw error("Product type (Business or Personal) must be specified in the test configuration");
		}

		JsonElement brazilCpfOperationalElement;
		String brazilCpfOperational;
		JsonElement brazilCnpjOperationalElement;
		String brazilCnpjOperational;

		brazilCpfOperationalElement = findByPath(config, "$.resource.brazilCpfOperational");
		brazilCpfOperational = OIDFJSON.getString(brazilCpfOperationalElement);
		if(Strings.isNullOrEmpty(brazilCpfOperational)) {
			logFailure("brazilCpfOperational is not valid", args("config", config));
		}
		if (productType.equals("business")) {
			brazilCnpjOperationalElement = findByPath(config, "$.resource.brazilCnpjOperationalBusiness");
			brazilCnpjOperational = OIDFJSON.getString(brazilCnpjOperationalElement);
			if(Strings.isNullOrEmpty(brazilCnpjOperational)) {
				logFailure("brazilCnpjOperationalBusiness is not valid", args("config", config));
			}
		}

        return env;
	}
}

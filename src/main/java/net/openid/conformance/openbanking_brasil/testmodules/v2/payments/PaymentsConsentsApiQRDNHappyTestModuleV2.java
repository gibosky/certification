package net.openid.conformance.openbanking_brasil.testmodules.v2.payments;


import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.CreatePaymentRequestEntityClaims;
import net.openid.conformance.condition.client.CreateTokenEndpointRequestForClientCredentialsGrant;
import net.openid.conformance.condition.client.FAPIBrazilCreatePaymentConsentRequest;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.RememberOriginalScopes;
import net.openid.conformance.openbanking_brasil.testmodules.ResetScopesToConfigured;
import net.openid.conformance.openbanking_brasil.testmodules.support.*;
import net.openid.conformance.openbanking_brasil.testmodules.support.payments.*;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.AddTransactionIdentificationFromConfig;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureErrorResponse.EnsureErrorResponseCodeFieldWasDetalhePagamentoInvalido;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureErrorResponse.EnsureErrorResponseCodeFieldWasFormaPagamentoInvalida;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureErrorResponse.EnsureErrorResponseCodeFieldWasPagamentoRecusadoDetentora;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentStatus.EnsurePaymentStatusWasAscs;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentStatus.EnsurePaymentStatusWasRjct;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentsRejection.EnsurePaymentRejectionReasonCodeWasPagamentoRecusadoDetentora;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.validators.CreateConsentErrorValidatorV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.validators.CreatePaymentErrorValidatorV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.validators.PaymentConsentValidatorV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.validators.ValidateMetaOnlyRequestDateTime;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.testmodule.PublishTestModule;
import net.openid.conformance.testmodule.TestFailureException;
import org.springframework.http.HttpStatus;

import java.util.Optional;


@PublishTestModule(
	testName = "payments-api-qrdn-good-proxy-test-v2",
	displayName = "Payments Consents API v2 test module for QRDN local instrument with user provided details",
	summary = "Ensures payment reaches an accepted state when a valid QRDN is sent. Additionally, checks if POST payments is Rejected when QRDN is reused.\n" +
		"The Dynamic QRCode must be created by the organisation by using the PIX Tester environment and all the creditor details must be aligned with what is supplied on the config fields.\n" +
		"\u2022 Call POST Consents with user provided QRDN \n" +
		"\u2022 Expects 201 - Validate Response\n" +
		"\u2022 Redirects the user to authorize the created consent\n" +
		"\u2022 Call GET Consent\n" +
		"\u2022 Expects 200 - Validate if status is \"AUTHORISED\"\n" +
		"\u2022 Calls the POST Payments Endpoint - Send the CNPJ Initiator provided by the user and Create the E2EID using the CNPJ Initiator\n" +
		"\u2022 Expects 201 - Validate Response\n" +
		"\u2022 Poll the Get Payments endpoint with the PaymentID Created while payment status is RCVD, ACCP, or ACPD (Validate only the first call) \n" +
		"\u2022 Expects Accepted state (ACSC) - Validate Response\n" +
		"\u2022 Call POST Consent with the same QRDN\n" +
		"\u2022 Expects 422 - DETALHE_PAGAMENTO_INVALIDO\n" +
		"\u2022 Validate Error message\n" +
		"If no errors are identified:\n" +
		"\u2022 Call GET Consent\n" +
		"\u2022 Expects 200 - Validate if the status is \"AUTHORISED\"\n" +
		"\u2022 Redirects the user to authorize the created consent\n" +
		"\u2022 Calls the POST Payments Endpoint - Send the CNPJ Initiator provided by the user and Create the E2EID using the CNPJ Initiator\n" +
		"\u2022 Expects 422 - PAGAMENTO_RECUSADO_DETENTORA\n" +
		"\u2022 Validate Error message\n" +
		"If no errors are identified:\n" +
		"\u2022 Poll the Get Payments endpoint with the PaymentID Created while the payment status is RCVD, ACCP or ACPD\n" +
		"\u2022 Expects RJCT Status - Ensure rejectionReason is PAGAMENTO_RECUSADO_DETENTORA",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"resource.consentUrl",
		"resource.loggedUserIdentification",
		"resource.businessEntityIdentification",
		"resource.debtorAccountIspb",
		"resource.debtorAccountIssuer",
		"resource.debtorAccountNumber",
		"resource.debtorAccountType",
		"resource.paymentAmount",
		"resource.brazilQrdnPaymentConsent",
		"resource.brazilQrdnCnpj",
		"resource.transactionIdentifier"
	}
)
public class PaymentsConsentsApiQRDNHappyTestModuleV2 extends AbstractOBBrasilQrCodePaymentFunctionalTestModuleV2 {

	private boolean secondPayment = false;

	@Override
	protected void performPreAuthorizationSteps() {
		env.mapKey("access_token", "saved_client_credentials");
		eventLog.log(getName(), "Payments scope present - protected resource assumed to be a payments endpoint");
		ConditionSequence steps;
		if (!secondPayment) {
			steps = new OpenBankingBrazilPreAuthorizationErrorAgnosticSteps(addTokenEndpointClientAuthentication)
				.replace(FAPIBrazilCreatePaymentConsentRequest.class, condition(SelectPaymentConsentWithQrdnCode.class))
				.insertBefore(CreateTokenEndpointRequestForClientCredentialsGrant.class, condition(RememberOriginalScopes.class))
				.replace(
					ValidateErrorAndMetaFieldNames.class,
					condition(CreateConsentErrorValidatorV2.class)
						.dontStopOnFailure()
						.onFail(Condition.ConditionResult.FAILURE)
						.skipIfStringMissing("validate_errors"));
			call(steps);
			//if error 422 and proceed empty, validate error again and skip test
			callAndContinueOnFailure(EnsureErrorResponseCodeFieldWasFormaPagamentoInvalida.class, Condition.ConditionResult.INFO);
			if (Optional.ofNullable(env.getBoolean("error_status_FPI")).orElse(false) ) {
				fireTestSkipped("422 - FORMA_PAGAMENTO_INVALIDA” implies that the institution does not support the used localInstrument set or the Payment time and the test scenario will be skipped. With the skipped condition the institution must not use this payment method on production until a new certification is re-issued");
			}

			eventLog.startBlock(currentClientString() + "Validate consents response");
			callAndContinueOnFailure(PaymentConsentValidatorV2.class, Condition.ConditionResult.FAILURE);
			env.mapKey(ValidateMetaOnlyRequestDateTime.RESPONSE_ENV_KEY, "consent_endpoint_response_full");
			callAndContinueOnFailure(ValidateMetaOnlyRequestDateTime.class, Condition.ConditionResult.FAILURE);
			env.unmapKey(ValidateMetaOnlyRequestDateTime.RESPONSE_ENV_KEY);
			eventLog.endBlock();
		} else {
			steps = new OpenBankingBrazilPreAuthorizationErrorAgnosticSteps(addTokenEndpointClientAuthentication)
				.insertBefore(CreateTokenEndpointRequestForClientCredentialsGrant.class, condition(ResetScopesToConfigured.class))
				.replace(FAPIBrazilCreatePaymentConsentRequest.class, condition(SelectPaymentConsentWithQrdnCode.class))
				.replace(
					ValidateErrorAndMetaFieldNames.class,
					condition(CreateConsentErrorValidatorV2.class)
						.dontStopOnFailure()
						.onFail(Condition.ConditionResult.FAILURE)
						.skipIfStringMissing("validate_errors")
				);
			call(steps);
			int status = Optional.ofNullable(env.getInteger("consent_endpoint_response_full", "status"))
				.orElseThrow(() -> new TestFailureException(getId(), "Could not find consent_endpoint_response_full"));
			env.mapKey(ValidateMetaOnlyRequestDateTime.RESPONSE_ENV_KEY, "consent_endpoint_response_full");
			if (status == org.apache.http.HttpStatus.SC_CREATED) {
				callAndContinueOnFailure(PaymentConsentValidatorV2.class, Condition.ConditionResult.FAILURE);
				callAndContinueOnFailure(ValidateMetaOnlyRequestDateTime.class, Condition.ConditionResult.FAILURE);
				env.unmapKey(ValidateMetaOnlyRequestDateTime.RESPONSE_ENV_KEY);
			} else {
				callAndContinueOnFailure(EnsureConsentResponseCodeWas422.class);
				env.putBoolean(ValidateMetaOnlyRequestDateTime.IS_META_OPTIONAL, true);
				callAndContinueOnFailure(ValidateMetaOnlyRequestDateTime.class, Condition.ConditionResult.FAILURE);
				env.mapKey(EnsureErrorResponseCodeFieldWasDetalhePagamentoInvalido.RESPONSE_ENV_KEY, "consent_endpoint_response_full");
				callAndContinueOnFailure(EnsureErrorResponseCodeFieldWasDetalhePagamentoInvalido.class);
				fireTestFinished();
			}
		}

	}

	@Override
	protected ConditionSequence getPixPaymentSequence() {
		ConditionSequence steps = new CallPixPaymentsEndpointSequence()
			.replace(CreatePaymentRequestEntityClaims.class, condition(CreatePaymentRequestEntityClaimsFromQrdnConfig.class));
		if (secondPayment) {
			steps = steps.replace(EnsureResponseCodeWas201.class, condition(EnsureResourceResponseCodeWas201Or422.class));
		}
		return steps;
	}

	@Override
	protected void validateResponse() {
		if (!secondPayment) {
			super.validateResponse();
		} else {
			int status = Optional.ofNullable(env.getInteger("resource_endpoint_response_full", "status"))
				.orElseThrow(() -> new TestFailureException(getId(), "Could not find resource_endpoint_response_full"));

			env.mapKey(ValidateMetaOnlyRequestDateTime.RESPONSE_ENV_KEY, "resource_endpoint_response_full");

			if (status == HttpStatus.UNPROCESSABLE_ENTITY.value()) {
				eventLog.log(getName(), "Validating 422 response");
				env.putBoolean(ValidateMetaOnlyRequestDateTime.IS_META_OPTIONAL, true);
				callAndStopOnFailure(CreatePaymentErrorValidatorV2.class);
				callAndStopOnFailure(ValidateMetaOnlyRequestDateTime.class);
				callAndStopOnFailure(EnsureErrorResponseCodeFieldWasPagamentoRecusadoDetentora.class);
			} else {
				super.validateResponse();
			}
		}
	}

	@Override
	protected void configureDictInfo() {
		callAndStopOnFailure(SelectQRDNCodeLocalInstrumentWithQrdnConfig.class);
		callAndStopOnFailure(SelectQRDNCodePixLocalInstrument.class);
		callAndStopOnFailure(AddHardcodedBrazilQrdnRemittanceToTheResource.class);
		callAndStopOnFailure(AddTransactionIdentificationFromConfig.class);
		callAndStopOnFailure(ValidateQrdnConfig.class);
	}


	@Override
	protected void onPostAuthorizationFlowComplete() {
		if (!secondPayment) {
			eventLog.startBlock("Try to re-use QR code");
			secondPayment = true;
			validationStarted = false;
			callAndContinueOnFailure(SubsequentPixPaymentEditorCondition.class);
			performAuthorizationFlow();
		} else {
			super.onPostAuthorizationFlowComplete();
		}
	}

	@Override
	protected void validateFinalState() {
		if (!secondPayment) {
			callAndStopOnFailure(EnsurePaymentStatusWasAscs.class);
		} else {
			callAndStopOnFailure(EnsurePaymentStatusWasRjct.class);
			callAndStopOnFailure(EnsurePaymentRejectionReasonCodeWasPagamentoRecusadoDetentora.class);
		}
	}


}


package net.openid.conformance.openbanking_brasil.testmodules.support.yacs;

import net.openid.conformance.openbanking_brasil.testmodules.support.CallDirectoryParticipantsEndpoint;
import net.openid.conformance.testmodule.Environment;

public class CallDirectoryParticipantsEndpointFromConfig extends CallDirectoryParticipantsEndpoint {

	@Override
	public Environment evaluate(Environment env) {
		String participantsUri = getStringFromEnvironment(env,"config","directory.Participants","Participants Endpoint for directory");
		setParticipantsUri(participantsUri);
		return super.evaluate(env);
	}

}

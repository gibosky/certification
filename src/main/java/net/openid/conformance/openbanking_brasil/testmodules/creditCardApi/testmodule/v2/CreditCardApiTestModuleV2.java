package net.openid.conformance.openbanking_brasil.testmodules.creditCardApi.testmodule.v2;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.creditCard.v2.CardAccountsDataResponseResponseValidatorV2;
import net.openid.conformance.openbanking_brasil.creditCard.v2.CardIdentificationResponseValidatorV2;
import net.openid.conformance.openbanking_brasil.creditCard.v2.CreditCardAccountsLimitsResponseValidatorV2;
import net.openid.conformance.openbanking_brasil.creditCard.v2.CreditCardAccountsTransactionBillResponseValidatorV2;
import net.openid.conformance.openbanking_brasil.creditCard.v2.CreditCardAccountsTransactionResponseValidatorV2;
import net.openid.conformance.openbanking_brasil.creditCard.v2.CreditCardBillValidatorV2;
import net.openid.conformance.openbanking_brasil.testmodules.account.PrepareUrlForFetchingAccountResource;
import net.openid.conformance.openbanking_brasil.testmodules.creditCardApi.CardBillSelector;
import net.openid.conformance.openbanking_brasil.testmodules.creditCardApi.PrepareUrlForFetchingBillTransactionResource;
import net.openid.conformance.openbanking_brasil.testmodules.creditCardApi.PrepareUrlForFetchingCardBills;
import net.openid.conformance.openbanking_brasil.testmodules.creditCardApi.PrepareUrlForFetchingCardLimits;
import net.openid.conformance.openbanking_brasil.testmodules.creditCardApi.PrepareUrlForFetchingCardTransactions;
import net.openid.conformance.openbanking_brasil.testmodules.support.*;
import net.openid.conformance.openbanking_brasil.testmodules.support.deprecated.ValidateMetaOnlyRequestDateTimeDeprecated;
import net.openid.conformance.openbanking_brasil.testmodules.v2.AbstractPhase2V2TestModule;
import net.openid.conformance.testmodule.PublishTestModule;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.VariantHidesConfigurationFields;

import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

@PublishTestModule(
	testName = "credit-card-api-test-v2",
	displayName = "Validate structure of all credit card API resources",
	summary = "Validates the structure of all credit card API resources\n" +
		"\u2022 Creates a Consent with the complete set of the credit cards permission group ([\"CREDIT_CARDS_ACCOUNTS_READ\", \"CREDIT_CARDS_ACCOUNTS_BILLS_READ\", \"CREDIT_CARDS_ACCOUNTS_BILLS_TRANSACTIONS_READ\", \"CREDIT_CARDS_ACCOUNTS_LIMITS_READ\", \"CREDIT_CARDS_ACCOUNTS_TRANSACTIONS_READ\", \"RESOURCES_READ\"])\n" +
		"\u2022 Expects a success 201 - Expects a success on Redirect as well \n" +
		"\u2022 Calls GET Credit Cards Accounts API V2\n" +
		"\u2022 Expects a 200 response \n" +
		"\u2022 Calls GET Credit Cards Accounts API V2 with AccountID specified\n" +
		"\u2022 Expects a 200 response\n" +
		"\u2022 Calls GET Credit Cards Accounts Limits API V2 with AccountID specified\n" +
		"\u2022 Expects a 200 response\n" +
		"\u2022 Calls GET Credit Cards Accounts Transactions API V2 with AccountID specified\n" +
		"\u2022 Expects a 200 response\n" +
		"\u2022 Calls GET Credit Cards Accounts Bills API V2 with AccountID specified\n" +
		"\u2022 Expects a 200 response\n" +
		"\u2022 Calls GET Credit Cards Accounts Bills Transactions V2 API with AccountID specified\n" +
		"\u2022 Expects a 200 response\n",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"resource.consentUrl",
		"resource.brazilCpf",
	}
)
@VariantHidesConfigurationFields(parameter = FAPI1FinalOPProfile.class, value = "openbanking_brazil", configurationFields = {
	"client.org_jwks"
})
public class CreditCardApiTestModuleV2 extends AbstractPhase2V2TestModule {

	private static final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd");

	@Override
	protected void configureClient(){
		callAndStopOnFailure(BuildCreditCardsAccountsConfigResourceUrlFromConsentUrl.class);
		LocalDate currentDate = LocalDate.now(ZoneId.of("America/Sao_Paulo"));
		env.putString("fromTransactionDate", currentDate.minusDays(360).format(FORMATTER));
		env.putString("toTransactionDate", currentDate.format(FORMATTER));
		env.putString("fromDueDate", currentDate.minusDays(360).format(FORMATTER));
		env.putString("toDueDate", currentDate.format(FORMATTER));
		super.configureClient();
	}

	@Override
	protected void onConfigure(JsonObject config, String baseUrl) {
		super.onConfigure(config, baseUrl);
		callAndStopOnFailure(PrepareAllCreditCardRelatedConsentsForHappyPathTest.class);
		callAndStopOnFailure(AddCreditCardScopes.class);
	}

	@Override
	protected void validateResponse() {
		callAndContinueOnFailure(CardAccountsDataResponseResponseValidatorV2.class, Condition.ConditionResult.FAILURE);
		callAndContinueOnFailure(EnsureResponseHasLinks.class, Condition.ConditionResult.FAILURE);
		callAndContinueOnFailure(ValidateResponseMetaData.class, Condition.ConditionResult.FAILURE);
		call(sequence(ValidateSelfEndpoint.class));

		callAndStopOnFailure(CardAccountSelector.class);
		callAndStopOnFailure(PrepareUrlForFetchingAccountResource.class);
		preCallProtectedResource("Fetch Credit Card details V2");
		callAndContinueOnFailure(CardIdentificationResponseValidatorV2.class, Condition.ConditionResult.FAILURE);
		callAndContinueOnFailure(EnsureResponseHasLinks.class, Condition.ConditionResult.FAILURE);
		callAndContinueOnFailure(ValidateResponseMetaData.class, Condition.ConditionResult.FAILURE);
		call(sequence(ValidateSelfEndpoint.class));

		callAndStopOnFailure(PrepareUrlForFetchingCardLimits.class);
		preCallProtectedResource("Fetch card limits V2");
		callAndContinueOnFailure(CreditCardAccountsLimitsResponseValidatorV2.class, Condition.ConditionResult.FAILURE);
		callAndContinueOnFailure(EnsureResponseHasLinks.class, Condition.ConditionResult.FAILURE);
		callAndContinueOnFailure(ValidateResponseMetaData.class, Condition.ConditionResult.FAILURE);
		call(sequence(ValidateSelfEndpoint.class));

		callAndStopOnFailure(PrepareUrlForFetchingCardTransactions.class);
		callAndStopOnFailure(AddToAndFromTransactionDateParametersToProtectedResourceUrl.class);
		preCallProtectedResource("Fetch card transactions V2");
		callAndContinueOnFailure(CreditCardAccountsTransactionResponseValidatorV2.class, Condition.ConditionResult.FAILURE);
		callAndContinueOnFailure(EnsureResponseHasLinks.class, Condition.ConditionResult.FAILURE);
		callAndContinueOnFailure(ValidateMetaOnlyRequestDateTimeDeprecated.class, Condition.ConditionResult.FAILURE);

		call(new ValidateSelfEndpoint()
			.replace(ValidateResponseMetaData.class, condition(ValidateMetaOnlyRequestDateTimeDeprecated.class)
			));

		call(condition(VerifyAdditionalFieldsWhenMetaOnlyRequestDateTime.class)
			.dontStopOnFailure()
			.onFail(Condition.ConditionResult.WARNING));

		callAndStopOnFailure(PrepareUrlForFetchingCardBills.class);
		callAndStopOnFailure(AddToAndFromDueDateParametersToProtectedResourceUrl.class);
		preCallProtectedResource("Fetch card bills V2");
		callAndContinueOnFailure(CreditCardBillValidatorV2.class, Condition.ConditionResult.FAILURE);
		callAndContinueOnFailure(EnsureResponseHasLinks.class, Condition.ConditionResult.FAILURE);
		callAndContinueOnFailure(ValidateResponseMetaData.class, Condition.ConditionResult.FAILURE);
		call(sequence(ValidateSelfEndpoint.class));

		callAndStopOnFailure(CardBillSelector.class);
		callAndStopOnFailure(PrepareUrlForFetchingBillTransactionResource.class);
		callAndStopOnFailure(AddToAndFromTransactionDateParametersToProtectedResourceUrl.class);
		preCallProtectedResource("Fetch Credit Card bill transaction V2");
		callAndContinueOnFailure(CreditCardAccountsTransactionBillResponseValidatorV2.class, Condition.ConditionResult.FAILURE);
		callAndContinueOnFailure(EnsureResponseHasLinks.class, Condition.ConditionResult.FAILURE);
		callAndContinueOnFailure(ValidateResponseMetaData.class, Condition.ConditionResult.FAILURE);
		call(sequence(ValidateSelfEndpoint.class));
	}

}

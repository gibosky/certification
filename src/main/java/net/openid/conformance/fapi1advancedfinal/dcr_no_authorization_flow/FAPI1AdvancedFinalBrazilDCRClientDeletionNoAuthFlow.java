package net.openid.conformance.fapi1advancedfinal.dcr_no_authorization_flow;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.*;
import net.openid.conformance.fapi1advancedfinal.FAPI1AdvancedFinalBrazilDCRClientDeletion;
import net.openid.conformance.openbanking_brasil.testmodules.support.SetScopeOnTokenEndpointRequestAccordingToDCRResponse;
import net.openid.conformance.sequence.client.CallDynamicRegistrationEndpointAndVerifySuccessfulResponse;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "fapi1-advanced-final-brazildcr-client-delete-no-authorization-flow",
	displayName = "FAPI1-Advanced-Final: Brazil DCR client deletion",
	summary = "Obtain a software statement from the Brazil directory (using the client MTLS certificate and directory client id provided in the test configuration), register a new client on the target authorization server then check behaviour of GET/DELETE operations after client deletion.",
	profile = "FAPI1-Advanced-Final",
	configurationFields = {
		"server.discoveryUrl",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"directory.discoveryUrl",
		"directory.client_id",
		"directory.apibase"
	}
)
public class FAPI1AdvancedFinalBrazilDCRClientDeletionNoAuthFlow extends FAPI1AdvancedFinalBrazilDCRClientDeletion {

	@Override
	protected void setupResourceEndpoint() {
		// not needed as resource endpoint won't be called
	}

	@Override
	protected boolean scopeContains(String requiredScope) {
		// Not needed as scope field is optional
		return false;
	}

	@Override
	protected void callRegistrationEndpoint() {
		call(sequence(CallDynamicRegistrationEndpointAndVerifySuccessfulResponse.class));
		callAndContinueOnFailure(ClientManagementEndpointAndAccessTokenRequired.class, Condition.ConditionResult.FAILURE, "BrazilOBDCR-7.1", "RFC7592-2");
		eventLog.endBlock();
	}

	@Override
	protected void performClientCredentialsGrant() {
		callAndStopOnFailure(CreateTokenEndpointRequestForClientCredentialsGrant.class);
		callAndStopOnFailure(SetScopeOnTokenEndpointRequestAccordingToDCRResponse.class);
		call(sequence(addTokenEndpointClientAuthentication));
		callAndStopOnFailure(CallTokenEndpointAndReturnFullResponse.class);
	}

	@Override
	public void start() {
		setStatus(Status.RUNNING);

		call(exec().startBlock("Verify that client_credentials grant can be used"));

		performClientCredentialsGrant();
		callAndContinueOnFailure(CheckTokenEndpointHttpStatus200.class, Condition.ConditionResult.FAILURE);
		callAndStopOnFailure(CheckIfTokenEndpointResponseError.class);
		callAndStopOnFailure(CheckForAccessTokenValue.class);


		eventLog.startBlock("Deleting client then expecting GET / DELETE on configuration endpoint to fail");
		callAndContinueOnFailure(UnregisterDynamicallyRegisteredClient.class, Condition.ConditionResult.FAILURE, "BrazilOBDCR-7.1", "RFC7592-2.3");

		callAndContinueOnFailure(WaitFor60Seconds.class);

		callAndStopOnFailure(CallClientConfigurationEndpoint.class, "OIDCD-4.2");

		call(exec().mapKey("endpoint_response", "registration_client_endpoint_response"));

		callAndContinueOnFailure(EnsureHttpStatusCodeIs401.class, Condition.ConditionResult.FAILURE, "RFC7592-2.1");
		callAndContinueOnFailure(CheckNoClientIdFromClientConfigurationEndpoint.class, Condition.ConditionResult.FAILURE);

		call(exec().unmapKey("endpoint_response"));

		callAndContinueOnFailure(UnregisterDynamicallyRegisteredClientExpectingFailure.class, Condition.ConditionResult.FAILURE, "BrazilOBDCR-7.1", "RFC7592-2.3");

		call(exec().startBlock("Verify that client_credentials grant fails now client has been deleted"));

		performClientCredentialsGrant();

		callAndContinueOnFailure(ValidateErrorFromTokenEndpointResponseError.class, Condition.ConditionResult.FAILURE);
		callAndContinueOnFailure(CheckTokenEndpointHttpStatus400or401.class, Condition.ConditionResult.FAILURE);
		callAndContinueOnFailure(CheckErrorFromTokenEndpointResponseErrorInvalidClient.class, Condition.ConditionResult.FAILURE);

		// we already deregistered the client, so prevent cleanup from trying to do so again
		JsonObject client = env.getObject("client");
		client.remove("registration_client_uri");
		client.remove("registration_access_token");

		fireTestFinished();
	}
}

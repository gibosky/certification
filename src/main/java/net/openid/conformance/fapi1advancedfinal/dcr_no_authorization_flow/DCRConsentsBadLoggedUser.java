package net.openid.conformance.fapi1advancedfinal.dcr_no_authorization_flow;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.AddClientAssertionToTokenEndpointRequest;
import net.openid.conformance.condition.client.AddClientIdToTokenEndpointRequest;
import net.openid.conformance.condition.client.AddFAPIAuthDateToResourceEndpointRequest;
import net.openid.conformance.condition.client.CallTokenEndpoint;
import net.openid.conformance.condition.client.CheckIfTokenEndpointResponseError;
import net.openid.conformance.condition.client.ClientManagementEndpointAndAccessTokenRequired;
import net.openid.conformance.condition.client.CreateClientAuthenticationAssertionClaims;
import net.openid.conformance.condition.client.CreateEmptyResourceEndpointRequestHeaders;
import net.openid.conformance.condition.client.CreateTokenEndpointRequestForClientCredentialsGrant;
import net.openid.conformance.condition.client.EnsureContentTypeJson;
import net.openid.conformance.condition.client.EnsureHttpStatusCodeIs201;
import net.openid.conformance.condition.client.ExtractAccessTokenFromTokenResponse;
import net.openid.conformance.condition.client.ExtractConsentIdFromConsentEndpointResponse;
import net.openid.conformance.condition.client.FAPIBrazilAddExpirationPlus30ToConsentRequest;
import net.openid.conformance.condition.client.FAPIBrazilCreatePaymentConsentRequest;
import net.openid.conformance.condition.client.FAPIBrazilOpenBankingCreateConsentRequest;
import net.openid.conformance.condition.client.FAPIBrazilSignPaymentConsentRequest;
import net.openid.conformance.condition.client.GetResourceEndpointConfiguration;
import net.openid.conformance.condition.client.SetConsentsScopeOnTokenEndpointRequest;
import net.openid.conformance.condition.client.SetPaymentsScopeOnTokenEndpointRequest;
import net.openid.conformance.condition.client.SignClientAuthenticationAssertion;
import net.openid.conformance.fapi1advancedfinal.FAPI1AdvancedFinalBrazilDCRHappyFlow;
import net.openid.conformance.openbanking_brasil.testmodules.support.AddConsentScope;
import net.openid.conformance.openbanking_brasil.testmodules.support.AddDummyBrazilPaymentConsent;
import net.openid.conformance.openbanking_brasil.testmodules.support.AddDummyCPFToConfig;
import net.openid.conformance.openbanking_brasil.testmodules.support.AddDummyPersonalProductTypeToConfig;
import net.openid.conformance.openbanking_brasil.testmodules.support.CallConsentEndpointWithBearerTokenAnyHttpMethod;
import net.openid.conformance.openbanking_brasil.testmodules.support.CopyClientJwksToClient;
import net.openid.conformance.openbanking_brasil.testmodules.support.EnsureConsentUrlIsNotNull;
import net.openid.conformance.openbanking_brasil.testmodules.support.EnsureEndpointResponseWas400or422or201;
import net.openid.conformance.openbanking_brasil.testmodules.support.EnsureResponseCodeWas204;
import net.openid.conformance.openbanking_brasil.testmodules.support.EnsureSecondaryConsentUrlIsNotNull;
import net.openid.conformance.openbanking_brasil.testmodules.support.PrepareToDeleteConsent;
import net.openid.conformance.openbanking_brasil.testmodules.support.PrepareToFetchConsentRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.PrepareToPostConsentRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.SetContentTypeApplicationJson;
import net.openid.conformance.openbanking_brasil.testmodules.support.SignedPaymentConsentSequence;
import net.openid.conformance.openbanking_brasil.testmodules.support.SwitchToSecondaryConsentUrl;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.sequence.client.CallDynamicRegistrationEndpointAndVerifySuccessfulResponse;
import net.openid.conformance.testmodule.PublishTestModule;
import net.openid.conformance.testmodule.TestFailureException;
import net.openid.conformance.variant.ClientAuthType;
import org.apache.http.HttpStatus;

import java.util.Optional;

@PublishTestModule(
	testName = "consents-bad-logged",
	displayName = "FAPI1-Advanced-Final: Brazil DCR happy flow without authentication flow",
	summary = "This test will try to use the recently created DCR to access either a Consents or, if the server does not support Phase 2, a Payments Consent Call with a dummy, but well-formated payload to make sure that the server will read the request but won’t be able to process it. If server supports both Consents API, server will try to call both of them and expect a success.\n" +
		"\u2022 Create a client by performing a DCR against the provided server - Expect Success\n" +
		"\u2022 Generate a token with the client_id created using client_credentials grant with either payments or consents scope\n" +
		"\u2022 Use the token to call either the POST Consents or POST Payments Consents API, depending on the directory configuration provided the first provided consentURI\n" +
		"\u2022 Expect the server to accept the message but return a failure because with either 400, 422 because of well formatted but invalid payload sent or 201 - If the message is a Payments Consents Response make sure the JWT has been correctly signed by the key registered on the A.S.\n" +
		"\u2022 Call either the POST consents or payments-consents API, depending on what has been provided on the  on the \"SecondaryConsentUrl\" field\n" +
		"\u2022 Expect the server to accept the message but return a failure because with either 400, 422 because of well formatted but invalid payload sent or 201 - If the message is a Payments Consents Response make sure the JWT has been correctly signed by the key registered on the A.S.",
	profile = "FAPI1-Advanced-Final",
	configurationFields = {
		"server.discoveryUrl",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"directory.discoveryUrl",
		"directory.client_id",
		"directory.apibase",
		"resource.consentUrl",
		"resource.consentUrl2",
		"resource.brazilOrganizationId"
	}
)

public class DCRConsentsBadLoggedUser extends FAPI1AdvancedFinalBrazilDCRHappyFlow {

	protected ClientAuthType clientAuthType;

	@Override
	public void start() {
		setStatus(Status.RUNNING);
		super.onPostAuthorizationFlowComplete();
	}

	@Override
	protected void configureClient() {
		clientAuthType = getVariant(ClientAuthType.class);
		super.configureClient();
	}

	@Override
	protected void setupResourceEndpoint() {
		// not needed as resource endpoint won't be called
	}

	@Override
	protected boolean scopeContains(String requiredScope) {
		// Not needed as scope field is optional
		return false;
	}

	@Override
	protected void callRegistrationEndpoint() {
		call(sequence(CallDynamicRegistrationEndpointAndVerifySuccessfulResponse.class));
		callAndContinueOnFailure(ClientManagementEndpointAndAccessTokenRequired.class, Condition.ConditionResult.FAILURE, "BrazilOBDCR-7.1", "RFC7592-2");
		eventLog.endBlock();

		eventLog.startBlock("Configuring dummy data");
		callAndStopOnFailure(AddDummyCPFToConfig.class);
		callAndStopOnFailure(AddDummyPersonalProductTypeToConfig.class);
		callAndStopOnFailure(AddDummyBrazilPaymentConsent.class);
		callAndStopOnFailure(GetResourceEndpointConfiguration.class);
		eventLog.endBlock();

		eventLog.startBlock("Checking consentURL");
		callAndStopOnFailure(EnsureConsentUrlIsNotNull.class);
		callAndStopOnFailure(EnsureSecondaryConsentUrlIsNotNull.class);
		String consentUrl = env.getString("config", "resource.consentUrl");
		String secondaryConsentUrl = env.getString("config", "resource.consentUrl2");

		callConsentsEndpoint(consentUrl, "consentUrl");
		callAndContinueOnFailure(SwitchToSecondaryConsentUrl.class);
		callConsentsEndpoint(secondaryConsentUrl, "secondaryConsentUrl");

	}

	private void callConsentsEndpoint(String consentUrl, String message) {
		if (consentUrl.matches("^(https://)(.*?)(consents/v[0-9]/consents)")) {
			eventLog.startBlock("Calling Token Endpoint using Client Credentials");
			eventLog.log(getName(), String.format("%s is identified as Consents consent URL", message));
			callAndStopOnFailure(CreateTokenEndpointRequestForClientCredentialsGrant.class);
			callAndStopOnFailure(SetConsentsScopeOnTokenEndpointRequest.class);

			call(callTokenEndpointShortVersion());
			eventLog.endBlock();

			eventLog.startBlock(String.format("Calling Consents API using %s", message));
			call(consentsApiSequence());

			int status = Optional.ofNullable(env.getInteger("consent_endpoint_response_full", "status"))
				.orElseThrow(() -> new TestFailureException(getId(), "Could not find resource_endpoint_response_full"));

			if (status == HttpStatus.SC_CREATED) {
				callAndStopOnFailure(ExtractConsentIdFromConsentEndpointResponse.class);
				deleteConsent();
				env.mapKey("resource_endpoint_response_full", "consent_endpoint_response_full");
				callAndStopOnFailure(EnsureResponseCodeWas204.class);
			}

		} else if (consentUrl.matches("^(https://)(.*?)(payments/v[0-9]/consents)")) {
			eventLog.startBlock("Calling Token Endpoint using Client Credentials");
			eventLog.log(getName(), String.format("%s is identified as Payments consent URL", message));
			callAndStopOnFailure(CreateTokenEndpointRequestForClientCredentialsGrant.class);
			callAndStopOnFailure(SetPaymentsScopeOnTokenEndpointRequest.class);

			call(callTokenEndpointShortVersion());
			eventLog.endBlock();
			eventLog.startBlock(String.format("Calling Consents API using %s", message));
			call(getPaymentsConsentSequence());


			env.mapKey("endpoint_response", "consent_endpoint_response_full");
			if(env.getInteger("endpoint_response", "status") == HttpStatus.SC_BAD_REQUEST){
				callAndStopOnFailure(EnsureContentTypeJson.class);
			}
			env.unmapKey("endpoint_response");

		}
		eventLog.endBlock();
	}

	private ConditionSequence getPaymentsConsentSequence() {
		return new SignedPaymentConsentSequence()
			.insertAfter(AddFAPIAuthDateToResourceEndpointRequest.class, condition(FAPIBrazilCreatePaymentConsentRequest.class))
			.insertBefore(FAPIBrazilSignPaymentConsentRequest.class, condition(CopyClientJwksToClient.class))
			.replace(EnsureHttpStatusCodeIs201.class, condition(EnsureEndpointResponseWas400or422or201.class));
	}

	private ConditionSequence consentsApiSequence() {
		return sequenceOf(
			condition(PrepareToPostConsentRequest.class),
			condition(AddConsentScope.class),
			condition(GetResourceEndpointConfiguration.class),
			condition(CreateEmptyResourceEndpointRequestHeaders.class),
			condition(AddFAPIAuthDateToResourceEndpointRequest.class),
			condition(FAPIBrazilOpenBankingCreateConsentRequest.class),
			condition(FAPIBrazilAddExpirationPlus30ToConsentRequest.class),
			condition(SetContentTypeApplicationJson.class),
			condition(CallConsentEndpointWithBearerTokenAnyHttpMethod.class).dontStopOnFailure().onFail(Condition.ConditionResult.INFO)
		);
	}

	private ConditionSequence callTokenEndpointShortVersion() {
		ConditionSequence sequence = sequenceOf(
			condition(AddClientIdToTokenEndpointRequest.class),
			condition(CreateClientAuthenticationAssertionClaims.class).dontStopOnFailure(),
			condition(SignClientAuthenticationAssertion.class).dontStopOnFailure(),
			condition(AddClientAssertionToTokenEndpointRequest.class).dontStopOnFailure(),
			condition(CallTokenEndpoint.class),
			condition(CheckIfTokenEndpointResponseError.class),
			condition(ExtractAccessTokenFromTokenResponse.class)
		);

		if (clientAuthType == ClientAuthType.MTLS) {
			sequence.skip(CreateClientAuthenticationAssertionClaims.class, "Not needed for MTLS")
				.skip(SignClientAuthenticationAssertion.class, "Not needed for MTLS")
				.skip(AddClientAssertionToTokenEndpointRequest.class, "Not needed for MTLS");
		}
		return sequence;
	}

	public void deleteConsent() {
		eventLog.startBlock("Deleting consent");
		callAndContinueOnFailure(PrepareToFetchConsentRequest.class);
		callAndContinueOnFailure(PrepareToDeleteConsent.class, Condition.ConditionResult.FAILURE);
		callAndContinueOnFailure(CallConsentEndpointWithBearerTokenAnyHttpMethod.class, Condition.ConditionResult.FAILURE);
	}
	@Override
	protected void onPostAuthorizationFlowComplete() {
		// not needed as resource endpoint won't be called
	}
}

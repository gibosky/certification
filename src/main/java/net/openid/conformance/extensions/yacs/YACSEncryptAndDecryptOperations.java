package net.openid.conformance.extensions.yacs;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import net.openid.conformance.extensions.YACSKmsOperations;
import net.openid.conformance.testmodule.OIDFJSON;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import software.amazon.awssdk.core.SdkBytes;
import software.amazon.awssdk.services.kms.KmsClient;
import software.amazon.awssdk.services.kms.model.DecryptRequest;
import software.amazon.awssdk.services.kms.model.DecryptResponse;
import software.amazon.awssdk.services.kms.model.GetPublicKeyRequest;
import software.amazon.awssdk.services.kms.model.GetPublicKeyResponse;


import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.OAEPParameterSpec;
import javax.crypto.spec.PSource;
import java.nio.charset.StandardCharsets;
import java.security.*;
import java.security.spec.*;
import java.util.Base64;
import java.util.Map;

public class YACSEncryptAndDecryptOperations {

	private final YACSKmsOperations kmsOperations;

	private final KmsClient kmsClient;

	private final String kmsKeyId;

	public YACSEncryptAndDecryptOperations(YACSKmsOperations kmsOperations, KmsClient kmsClient, String kmsKeyId) {
		this.kmsOperations = kmsOperations;
		this.kmsClient = kmsClient;
		this.kmsKeyId = kmsKeyId;
	}

	public JsonObject decryptJSONObject(JsonObject jsonObject) {
		if (jsonObject != null) {
			for (Map.Entry<String, JsonElement> entry : jsonObject.entrySet()) {
					DecryptResponse response = decryptValue(OIDFJSON.getString(entry.getValue()),kmsKeyId);
					JsonElement newElement = JsonParser.parseString(response.plaintext().asString(StandardCharsets.UTF_8));
					jsonObject.add(entry.getKey(),newElement);

			}
			return jsonObject;
		}
		return null;
	}

	public JsonObject encryptJSONObject(JsonObject jsonObject) throws NoSuchAlgorithmException, InvalidParameterSpecException, NoSuchPaddingException, InvalidAlgorithmParameterException, InvalidKeyException, InvalidKeySpecException, IllegalBlockSizeException, BadPaddingException {
		if (jsonObject != null) {
			for (Map.Entry<String, JsonElement> entry : jsonObject.entrySet()) {
					String cipherText = encryptValue(entry.getValue().toString(),kmsKeyId);
					jsonObject.addProperty(entry.getKey(), cipherText);
			}
			return jsonObject;
		}
		return null;
	}

	private String encryptValue(String value, String kmsKeyId) throws NoSuchAlgorithmException, InvalidKeySpecException, InvalidParameterSpecException, NoSuchPaddingException, InvalidAlgorithmParameterException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException {
		GetPublicKeyRequest getPublicKeyRequest = GetPublicKeyRequest.builder().keyId(kmsKeyId).build();
		GetPublicKeyResponse getPublicKeyResponse = kmsOperations.getPublicKey(kmsClient,getPublicKeyRequest);

		PublicKey publicKey = KeyFactory.getInstance("RSA").generatePublic(new X509EncodedKeySpec(getPublicKeyResponse.publicKey().asByteArray()));
		AlgorithmParameters parameters = AlgorithmParameters.getInstance("OAEP", Security.getProvider("BC"));
		AlgorithmParameterSpec specification = new OAEPParameterSpec("SHA-256", "MGF1", MGF1ParameterSpec.SHA256, PSource.PSpecified.DEFAULT);
		parameters.init(specification);
		Cipher cipher = Cipher.getInstance("RSA/ECB/OAEPWithSHA-256AndMGF1Padding", new BouncyCastleProvider());
		cipher.init(Cipher.ENCRYPT_MODE, publicKey, parameters);

		return Base64.getEncoder().encodeToString(cipher.doFinal(value.getBytes()));
	}

	private DecryptResponse decryptValue(String value, String kmsKeyId){
		DecryptRequest decryptRequest = DecryptRequest.builder().encryptionAlgorithm("RSAES_OAEP_SHA_256").keyId(kmsKeyId).ciphertextBlob(SdkBytes.fromByteArray(Base64.getDecoder().decode(value.getBytes()))).build();

		return kmsOperations.decrypt(kmsClient, decryptRequest);
	}
}

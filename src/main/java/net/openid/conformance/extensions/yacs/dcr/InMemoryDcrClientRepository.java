package net.openid.conformance.extensions.yacs.dcr;

import com.google.gson.JsonObject;
import net.openid.conformance.testmodule.OIDFJSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

public class InMemoryDcrClientRepository implements DcrClientRepository {

	private static final Logger LOG = LoggerFactory.getLogger(InMemoryDcrClientRepository.class);

	private Map<String, JsonObject> clients = new HashMap<>();

	@Override
	public void saveClient(String testId, JsonObject clientConfig) {
		String clientId = OIDFJSON.getString(clientConfig.get("client_id"));
		LOG.info("Adding DCR-obtained client {} for test {}", clientId, testId);
		clients.put(testId, clientConfig);
	}

	@Override
	public JsonObject getSavedClient(String testId) {
		return clients.get(testId);
	}

	@Override
	public void deleteSavedClient(String id) {
		JsonObject removed = clients.remove(id);
		String clientId = OIDFJSON.getString(removed.get("client_id"));
		LOG.info("Removing DCR-obtained client {} for test {}", clientId, id);
	}
}

package net.openid.conformance.extensions.yacs;

import com.google.gson.JsonObject;
import com.nimbusds.jose.shaded.json.JSONObject;
import com.nimbusds.jwt.JWT;
import net.openid.conformance.security.AuthenticationFacade;
import net.openid.conformance.testmodule.OIDFJSON;
import org.mitre.openid.connect.model.OIDCAuthenticationToken;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

public class OrgOwnershipEnhancedVerification implements EnhancedVerification {

	private static final Logger LOG = LoggerFactory.getLogger(OrgOwnershipEnhancedVerification.class);

	private AuthenticationFacade authenticationFacade;

	private final ParticipantsService participantsService;

	public OrgOwnershipEnhancedVerification(AuthenticationFacade authenticationFacade, ParticipantsService participantsService) {
		this.authenticationFacade = authenticationFacade;
		this.participantsService = participantsService;
	}

	@Override
	public boolean allowed(BodyRepeatableHttpServletRequest request) {
		JsonObject config = request.body();
		config = config.getAsJsonObject("server");
		if(config == null) {
			LOG.error("No server config - failing");
			return false; // maybe exception?
		}
		String wellKnown = OIDFJSON.getString(config.get("discoveryUrl"));
		OIDCAuthenticationToken auth = (OIDCAuthenticationToken) authenticationFacade.getContextAuthentication();
		JWT idToken = auth.getIdToken();
			try {
				JSONObject trustFrameworkProfile = (JSONObject) idToken.getJWTClaimsSet().getClaim("trust_framework_profile");
				if(trustFrameworkProfile == null) {
					LOG.info("No trust framework profile on id token");
					return false;
				}
				JSONObject orgAccessDetails = (JSONObject) trustFrameworkProfile.get("org_access_details");
				if(orgAccessDetails == null) {
					LOG.info("No org access details in trust framework profile");
					return false;
				}
				Set<String> orgIds = orgAccessDetails.keySet();
				Set<Map> orgs = participantsService.orgsFor(orgIds);
				List<String> authServers = new ArrayList<>();
				for(Map org: orgs) {
					List<Map> authServersForOrg = (List<Map>) org.get("AuthorisationServers");
					authServers.addAll(authServersForOrg.stream()
						.map(a -> a.get("OpenIDDiscoveryDocument"))
							.map(v -> String.valueOf(v))
						.collect(Collectors.toList()));
				}
				boolean allowed = authServers.contains(wellKnown);
				LOG.info(allowed ? "Well-known in config belongs to org of which user is a member" : "Well-known is not from any org linked to user");
				return allowed;
			} catch (ParseException e) {
				throw new RuntimeException(e);
			}
	}


}

package net.openid.conformance.extensions.yacs.dcr;

import com.google.gson.JsonObject;

public class JwksUriAdder implements DcrRequestProcessor {

	private static final String JWKS_TEMPLATE = "%s/%s/%s/application.jwks";

	private DirectoryDetails directoryDetails;

	public JwksUriAdder(DirectoryDetails directoryDetails) {
		this.directoryDetails = directoryDetails;
	}

    @Override
	public void process(JsonObject dcrRequest) {
		dcrRequest.addProperty("jwks_uri",
				String.format(JWKS_TEMPLATE,
					directoryDetails.getKeystoreUri(),
					directoryDetails.getOrgId(),
					directoryDetails.getSoftwareStatementId()));
	}
}

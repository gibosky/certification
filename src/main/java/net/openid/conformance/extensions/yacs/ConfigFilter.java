package net.openid.conformance.extensions.yacs;

import com.google.gson.JsonObject;
import com.nimbusds.jose.shaded.json.JSONObject;
import com.nimbusds.jwt.JWT;
import net.openid.conformance.security.AuthenticationFacade;
import net.openid.conformance.testmodule.OIDFJSON;
import net.openid.conformance.util.JsonObjectBuilder;
import org.mitre.openid.connect.model.OIDCAuthenticationToken;
import org.springframework.beans.factory.annotation.Value;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.ParseException;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class ConfigFilter extends AbstractYACSFilter {

	private ParticipantsService participantsService;
	private AuthenticationFacade authenticationFacade;

	@Value("${fintechlabs.yacs.directory.uri}")
	private String participantsApiUri;

	public ConfigFilter(ParticipantsService participantsService, AuthenticationFacade authenticationFacade) {
		super(new MethodAndPathMatch("POST", "/api/plan"));
		this.participantsService = participantsService;
		this.authenticationFacade = authenticationFacade;
	}

	@Override
	public void processRequest(BodyRepeatableHttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		JsonObject body = request.body();
		inferAlias(body);
		insertConfigObject("directory","Participants",participantsApiUri,body);
		request.updateBody(body);
	}

	private void inferAlias(JsonObject config) {
		JsonObject serverConfig = config.getAsJsonObject("server");
		if(serverConfig == null) {
			return;
		}
		String wellKnown = OIDFJSON.getString(serverConfig.get("discoveryUrl"));
		OIDCAuthenticationToken auth = (OIDCAuthenticationToken) authenticationFacade.getContextAuthentication();
		JWT idToken = auth.getIdToken();
		try {
			JSONObject trustFrameworkProfile = (JSONObject) idToken.getJWTClaimsSet().getClaim("trust_framework_profile");
			if(trustFrameworkProfile == null) {
				return;
			}
			JSONObject orgAccessDetails = (JSONObject) trustFrameworkProfile.get("org_access_details");
			if(orgAccessDetails == null) {
				return;
			}
			Set<String> orgIds = orgAccessDetails.keySet();
			Set<Map> orgs = participantsService.orgsFor(orgIds);
			for(Map org: orgs) {
				String orgId = String.valueOf(org.get("OrganisationId"));
				List<Map> authServersForOrg = (List<Map>) org.get("AuthorisationServers");
				for(Map authServer: authServersForOrg) {
					String oidcDiscovery = String.valueOf(authServer.get("OpenIDDiscoveryDocument"));
					if(oidcDiscovery.equals(wellKnown)) {
						config.addProperty("alias", orgId);
						return;
					}
				}

			}

		} catch (ParseException e) {
			throw new RuntimeException(e);
		}
	}

	private void insertConfigObject(String key, String path, String value, JsonObject config){
		JsonObjectBuilder jsonObjectBuilder = new JsonObjectBuilder()
			.addField(path,value);

		JsonObject newConfigObject = jsonObjectBuilder.build();

		config.add(key, newConfigObject);
	}
}

package net.openid.conformance.extensions.yacs.dcr;

import com.google.gson.JsonObject;
import net.openid.conformance.testmodule.OIDFJSON;

import java.security.cert.X509Certificate;

public class TlsClientAuthProcessor implements DcrRequestProcessor {

	private final DirectoryCredentials credentials;

	public TlsClientAuthProcessor(DirectoryCredentials directoryCredentials) {
		this.credentials = directoryCredentials;
	}

	@Override
	public void process(JsonObject dcrRequest) {
		dcrRequest.addProperty("token_endpoint_auth_method", "tls_client_auth");
		X509Certificate cert = credentials.clientCert();
		JsonObject jsonObject = CertificateUtils.extractSubject(cert);
		String subjectDn = OIDFJSON.getString(jsonObject.get("subjectdn"));
		dcrRequest.addProperty("tls_client_auth_subject_dn", subjectDn);
	}

}

package net.openid.conformance.extensions.yacs;

import com.google.gson.Gson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

public class ParticipantsService {

	private static final Logger LOG = LoggerFactory.getLogger(ParticipantsService.class);

	@Value("${fintechlabs.yacs.directory.uri}")
	private String participantsApiUri;

	private RestTemplate restTemplate = new RestTemplate();

	public Set<Map> orgsFor(Set<String> orgIds) {
		LOG.info("Looking up orgs for {}", orgIds);
		Map<String, Map> allOrgs = participants();
		return allOrgs.entrySet()
			.stream()
			.filter(kv -> orgIds.contains(kv.getKey()))
			.map(Map.Entry::getValue)
			.collect(Collectors.toSet());
	}

	@Cacheable("participants")
	public Map<String, Map> participants() {
		LOG.info("Refreshing participants data");
		ResponseEntity<String> forEntity = restTemplate.getForEntity(participantsApiUri, String.class);
		String e = forEntity.getBody();
		Map[] all =  new Gson().fromJson(e, Map[].class);
		Map<String, Map> lookup = new HashMap<>();
		for(Map m: all) {
			String orgId = (String) m.get("OrganisationId");
			orgId = orgId.replaceAll("-", "_");
			lookup.put(orgId, m);
		}

		return lookup;
	}

	@CacheEvict(value = "participants", allEntries = true)
	@Scheduled(fixedRateString = "60000")
	public void emptyParticipantsCache() {
		LOG.info("Emptying participants cache");

	}

}

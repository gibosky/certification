package net.openid.conformance.extensions.yacs;

import com.google.common.collect.ImmutableMap;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.mongodb.BasicDBObjectBuilder;
import com.mongodb.DBObject;
import net.openid.conformance.info.SavedConfigurationService;
import net.openid.conformance.security.AuthenticationFacade;
import net.openid.conformance.variant.VariantSelection;
import org.apache.commons.lang3.RandomStringUtils;
import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import software.amazon.awssdk.services.kms.model.InvalidCiphertextException;

import java.time.Instant;

public class YACSDBSavedConfigurationService implements SavedConfigurationService {
	public static final String COLLECTION = "TEST_CONFIG";

	@Autowired
	private MongoTemplate mongoTemplate;

	@Autowired
	private AuthenticationFacade authenticationFacade;

	@Autowired
	private YACSEncryptAndDecryptOperations yacsEncryptAndDecryptOperations;


	/* (non-Javadoc)
	 * @see SavedConfigurationService#getLastConfigForCurrentUser()
	 */
	@Override
	public Document getLastConfigForCurrentUser() {
		ImmutableMap<String, String> user = authenticationFacade.getPrincipal();

		if (user == null) {
			return null;
		}

		var document =  mongoTemplate.getCollection(COLLECTION)
			.find(new Document("owner", user))
			.sort(new Document("time", -1))
			.limit(1)
			.first();

		if (document != null){
			String mongoDocument = document.toJson();
			Gson gson = new Gson().newBuilder().create();
			JsonObject documentObject = gson.fromJson(mongoDocument, JsonObject.class);
			try {
				documentObject = yacsEncryptAndDecryptOperations.decryptJSONObject(documentObject.getAsJsonObject("config"));
			} catch (InvalidCiphertextException | IllegalArgumentException e){
				documentObject = new JsonObject();
			}
			document.append("config", documentObject);
		}
		return document;
	}

	@Override
	public void saveTestConfigurationForCurrentUser(JsonObject config, String testName, VariantSelection variant) {
		ImmutableMap<String, String> user = authenticationFacade.getPrincipal();

		if (user == null) {
			throw new IllegalStateException("No user found");
		}

		JsonObject encryptedConfig;
		clearOldConfigs(user);
		try {
			encryptedConfig = yacsEncryptAndDecryptOperations.encryptJSONObject(config);
		} catch (Exception e){
			throw new RuntimeException("Unable to encrypt config: " + e);
		}

		DBObject document = BasicDBObjectBuilder.start()
			.add("_id", RandomStringUtils.randomAlphanumeric(30))
			.add("owner", user)
			.add("testName", testName)
			.add("config", encryptedConfig)
			.add("variant", variant)
			.add("time", Instant.now().toString())
			.get();

		mongoTemplate.insert(document, COLLECTION);
	}

	@Override
	public void savePlanConfigurationForCurrentUser(JsonObject config, String planName, VariantSelection variant) {
		ImmutableMap<String, String> user = authenticationFacade.getPrincipal();

		if (user == null) {
			throw new IllegalStateException("No user found");
		}
		JsonObject encryptedConfig;
		clearOldConfigs(user);
		try {
			encryptedConfig = yacsEncryptAndDecryptOperations.encryptJSONObject(config);
		} catch (Exception e){
			throw new RuntimeException("Unable to encrypt config: " + e);
		}

		DBObject document = BasicDBObjectBuilder.start()
			.add("_id", RandomStringUtils.randomAlphanumeric(30))
			.add("owner", user)
			.add("planName", planName)
			.add("config", encryptedConfig)
			.add("variant", variant)
			.add("time", Instant.now().toString())
			.get();

		mongoTemplate.insert(document, COLLECTION);
	}

	private void clearOldConfigs(ImmutableMap<String, String> user) {

		// TODO: save more than just the last one

		Criteria criteria = new Criteria();
		criteria.and("owner").is(user);

		Query query = new Query(criteria);

		mongoTemplate.remove(query, COLLECTION);

	}
}

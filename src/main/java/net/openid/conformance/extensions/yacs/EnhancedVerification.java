package net.openid.conformance.extensions.yacs;

public interface EnhancedVerification {

	EnhancedVerification ALWAYS = (r) -> true;
	EnhancedVerification NEVER = (r) -> false;

	boolean allowed(BodyRepeatableHttpServletRequest request);

}

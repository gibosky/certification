package net.openid.conformance.extensions.yacs.dcr;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import net.openid.conformance.testmodule.OIDFJSON;
import net.openid.conformance.util.JsonObjectBuilder;
import net.openid.conformance.variant.ClientAuthType;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.HttpClientConnectionManager;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.conn.BasicHttpClientConnectionManager;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import javax.net.ssl.KeyManager;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Collections;

public class DcrHelper {

	private final DirectoryCredentials directoryCredentials;
	private final DirectoryDetails directoryDetails;
	private RestTemplate restTemplate;

	public DcrHelper(DirectoryCredentials directoryCredentials,
					 DirectoryDetails directoryDetails) {
		this.directoryCredentials = directoryCredentials;
		this.directoryDetails = directoryDetails;
		HttpClient httpClient = null;
		try {
			httpClient = createHttpClient(directoryCredentials);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		restTemplate = new RestTemplate(new HttpComponentsClientHttpRequestFactory(httpClient));
	}

	public JsonObject registerClient(String authServerDisvoveryUri, ClientAuthType clientAuthType, String redirect) {
		JsonObject response = new JsonObject();
		JsonObject jwks = directoryCredentials.signingJwks();
		JsonObject credentials = new JsonObjectBuilder()
			.addField("jwks", jwks)
			.addField("clientKey", directoryCredentials.clientKeyData())
			.addField("clientCert", directoryCredentials.clientCertData())
			.addField("clientCa", directoryCredentials.clientCaData())
			.addField("directoryClientId", directoryCredentials.directoryClientId())
			.build();
		response.add("credentials", credentials);
		JsonObject directoryDiscoveryDocument = getDiscoveryDocument(directoryDetails.getDiscoveryUri());
		JsonObject authServerDiscoveryDocument = getDiscoveryDocument(authServerDisvoveryUri);
		String tokenEndpoint = OIDFJSON.getString(directoryDiscoveryDocument.get("token_endpoint"));
		String dcrEndpoint = OIDFJSON.getString(authServerDiscoveryDocument.get("registration_endpoint"));

		JsonObject token = getTokenForDcr(tokenEndpoint, directoryCredentials.directoryClientId(), "directory:software");
		String ss = getSoftwareStatement(token);
		JsonObject client = doDcr(ss, dcrEndpoint, clientAuthType, redirect);
		response.add("client", client);
		return response;
	}

	public void unregisterClient(JsonObject savedClient) throws ClientDeletionException {
		String uri = OIDFJSON.getString(savedClient.get("registration_client_uri"));
		String accessToken = OIDFJSON.getString(savedClient.get("registration_access_token"));
		HttpHeaders headers = new HttpHeaders();

		headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
		headers.setBearerAuth(accessToken);

		HttpEntity<String> request = new HttpEntity<>(null, headers);

		ResponseEntity<String> res = restTemplate.exchange(uri, HttpMethod.DELETE, request, String.class);
		if(!res.getStatusCode().is2xxSuccessful()) {
			throw new ClientDeletionException(uri);
		}
	}

	private JsonObject getDiscoveryDocument(String directoryDiscoveryUri) {
		return getDiscoveryDocument(directoryDiscoveryUri, true);
	}

	private JsonObject getTokenForDcr(String tokenEndpoint, String clientId, String... scopes) {
		HttpHeaders headers = new HttpHeaders();

		headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
		headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

		MultiValueMap<String, String> requestBody = new LinkedMultiValueMap<>();
		requestBody.add("grant_type", "client_credentials");
		requestBody.add("scope", String.join(" ", scopes));
		requestBody.add("client_id", clientId);

		HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(requestBody, headers);

		String jsonString = null;

		jsonString = restTemplate.postForObject(tokenEndpoint, request, String.class);

		return JsonParser.parseString(jsonString).getAsJsonObject();
	}

	private String getSoftwareStatement(JsonObject token) {
		HttpHeaders headers = new HttpHeaders();

		headers.set("accept", "application/jwt;charset=UTF-8");
		headers.setBearerAuth(OIDFJSON.getString(token.get("access_token")));

		HttpEntity<String> request = new HttpEntity<>(null, headers);

		String ssUri = directoryDetails.softwareStatementUri();

		ResponseEntity<String> response = restTemplate.exchange(ssUri, HttpMethod.GET, request, String.class);

		String ss = response.getBody();

		return ss;

	}

	private JsonObject doDcr(String softwareStatement, String registrationEndpoint, ClientAuthType authType, String redirectUri) {

		JsonObject dcrRequest = new JsonObject();
		GrantTypeAdder.INSTANCE.process(dcrRequest);
		ResponseTypeAdder.INSTANCE.process(dcrRequest);
		new JwksUriAdder(directoryDetails).process(dcrRequest);
		new RedirectUrisAdder(redirectUri)
			.process(dcrRequest);
		new SoftwareStatementAdder(softwareStatement).process(dcrRequest);
		switch (authType) {
			case MTLS:
				new TlsClientAuthProcessor(directoryCredentials).process(dcrRequest);
				break;
			case PRIVATE_KEY_JWT:
				new PrivateKeyJWTAuthProcessor().process(dcrRequest);
				break;
			default:
				throw new RuntimeException("Unknown client auth type");
		}

		HttpHeaders headers = new HttpHeaders();

		headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
		headers.setContentType(MediaType.APPLICATION_JSON);

		HttpEntity<String> request = new HttpEntity<>(dcrRequest.toString(), headers);

		String jsonString = null;

		jsonString = restTemplate.postForObject(registrationEndpoint, request, String.class);

		JsonObject client = JsonParser.parseString(jsonString).getAsJsonObject();

		return client;

	}


	private JsonObject getDiscoveryDocument(String directoryDiscoveryUri, boolean mtlsAliases) {
		HttpHeaders headers = new HttpHeaders();

		headers.set("accept", "application/json");

		HttpEntity<String> request = new HttpEntity<>(null, headers);

		ResponseEntity<String> response = restTemplate.exchange(directoryDiscoveryUri, HttpMethod.GET, request, String.class);

		String ss = response.getBody();

		JsonObject discoveryDocument = JsonParser.parseString(ss).getAsJsonObject();

		if(mtlsAliases) {
			discoveryDocument = discoveryDocument.getAsJsonObject("mtls_endpoint_aliases");
		}

		return discoveryDocument;
	}

	private HttpClient createHttpClient(DirectoryCredentials credentials)
		throws NoSuchAlgorithmException,
		KeyManagementException {

		HttpClientBuilder builder = HttpClientBuilder.create()
			.useSystemProperties();

		int timeout = 60;
		RequestConfig config = RequestConfig.custom()
			.setConnectTimeout(timeout * 1000)
			.setConnectionRequestTimeout(timeout * 1000)
			.setSocketTimeout(timeout * 1000).build();
		builder.setDefaultRequestConfig(config);

		KeyManager[] km = credentials.mtlsKeymanagers();

		String[] suites = {"TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256", "TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384"};

		TrustManager[] trustAllCerts = {
			new X509TrustManager() {

				@Override
				public X509Certificate[] getAcceptedIssuers() {
					return new X509Certificate[0];
				}

				@Override
				public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {
				}

				@Override
				public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {
				}
			}
		};

		SSLContext sc = SSLContext.getInstance("TLS");
		sc.init(km, trustAllCerts, new SecureRandom());

		builder.setSSLContext(sc);

		SSLConnectionSocketFactory sslConnectionSocketFactory = new SSLConnectionSocketFactory(sc,
			new String[] { "TLSv1.2" },
			suites,
			NoopHostnameVerifier.INSTANCE);

		builder.setSSLSocketFactory(sslConnectionSocketFactory);

		Registry<ConnectionSocketFactory> registry = RegistryBuilder.<ConnectionSocketFactory> create()
			.register("https", sslConnectionSocketFactory)
			.register("http", new PlainConnectionSocketFactory())
			.build();

		HttpClientConnectionManager ccm = new BasicHttpClientConnectionManager(registry);
		builder.setConnectionManager(ccm);

		builder.disableRedirectHandling();

		builder.disableAutomaticRetries();

		HttpClient httpClient = builder.build();
		return httpClient;
	}


}

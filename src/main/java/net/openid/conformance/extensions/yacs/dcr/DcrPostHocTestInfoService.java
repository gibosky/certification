package net.openid.conformance.extensions.yacs.dcr;

import com.google.common.collect.ImmutableMap;
import com.google.gson.JsonObject;
import net.openid.conformance.info.TestInfoService;
import net.openid.conformance.testmodule.OIDFJSON;
import net.openid.conformance.testmodule.TestModule;
import net.openid.conformance.util.JsonObjectBuilder;
import net.openid.conformance.variant.ClientAuthType;
import net.openid.conformance.variant.VariantSelection;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpServerErrorException;

import java.time.Instant;
import java.util.List;
import java.util.Map;

import static net.openid.conformance.testmodule.TestModule.Status.FINISHED;
import static net.openid.conformance.testmodule.TestModule.Status.INTERRUPTED;

public class DcrPostHocTestInfoService implements TestInfoService {

	private final DirectoryCredentials directoryCredentials;
	@Value("${fintechlabs.base_url}")
	private String baseUrl;

	private final TestInfoService delegate;
	private final DcrClientRepository clientRepository;
	private final DcrHelper dcrHelper;

	public DcrPostHocTestInfoService(TestInfoService delegate,
									 DcrClientRepository clientRepository,
									 DcrHelper dcrHelper,
									 DirectoryCredentials directoryCredentials) {
		this.delegate = delegate;
		this.dcrHelper = dcrHelper;
		this.clientRepository = clientRepository;
		this.directoryCredentials = directoryCredentials;
	}

	@Override
	public void createTest(String id, String testName, VariantSelection variant,
						   VariantSelection variantFromPlanDefinition, String url,
						   JsonObject config, String alias, Instant started,
						   String testPlanId, String description, String summary, String publish) {
		try {
			injectDcrClient(id, variant, config);
		} catch(RuntimeException t) {
			throw new DcrException(t);
		}
		JsonObjectBuilder newConfig = new JsonObjectBuilder();
		//config not needed for info, no need to store the values of it
		newConfig.addField("restOfConfig", "obscured for security reasons");
		if (config.getAsJsonObject("server") != null){
			newConfig.addField("server.discoveryUrl", OIDFJSON.getString(config.getAsJsonObject("server").get("discoveryUrl")));
		}
		config = newConfig.build();
		delegate.createTest(id, testName, variant, variantFromPlanDefinition, url, config, alias, started, testPlanId, description, summary, publish);
	}

	@Override
	public void updateTestResult(String id, TestModule.Result result) {
		delegate.updateTestResult(id, result);
	}

	@Override
	public void updateTestStatus(String id, TestModule.Status status) {
		if(status.equals(FINISHED) || status.equals(INTERRUPTED)) {
			JsonObject savedClient = clientRepository.getSavedClient(id);
			try {
				dcrHelper.unregisterClient(savedClient);
				clientRepository.deleteSavedClient(id);
			} catch(ClientDeletionException e) {
				throw new HttpServerErrorException(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage());
			}

		}
		delegate.updateTestStatus(id, status);
	}

	@Override
	public ImmutableMap<String, String> getTestOwner(String id) {
		return delegate.getTestOwner(id);
	}

	@Override
	public boolean publishTest(String id, String publish) {
		return delegate.publishTest(id, publish);
	}

	@Override
	public void createIndexes() {
		delegate.createIndexes();
	}

	@Override
	public boolean deleteTests(List<String> id) {
		return delegate.deleteTests(id);
	}

	private void injectDcrClient(String id, VariantSelection variant, JsonObject config) {
		String alias = OIDFJSON.getString(config.get("alias"));
		JsonObject serverConfig = config.getAsJsonObject("server");
		String authServerDiscoveryUrl = OIDFJSON.getString(serverConfig.get("discoveryUrl"));
		String redirectUri = String.format("%s/test/a/%s/callback", baseUrl, alias);
		Map<String, String> variantDetails = variant.getVariant();
		String clientAuth = variantDetails.get("client_auth_type");
		ClientAuthType clientAuthType;
		switch (clientAuth) {
			case "private_key_jwt":
				clientAuthType = ClientAuthType.PRIVATE_KEY_JWT;
				break;
			default:
				clientAuthType = ClientAuthType.MTLS;
				break;
		}
		JsonObject dcrResult = dcrHelper.registerClient(authServerDiscoveryUrl, clientAuthType, redirectUri);
		JsonObject dynamicClient = dcrResult.getAsJsonObject("client");
		String clientId = OIDFJSON.getString(dynamicClient.get("client_id"));
		JsonObject credentials = dcrResult.getAsJsonObject("credentials");
		JsonObject clientConfig = getOrCreate("client", config);
		JsonObject directoryConfig = getOrCreate("directory", config);
		clientConfig.addProperty("client_id", clientId);
		clientConfig.add("jwks", credentials.get("jwks").getAsJsonObject());
		clientConfig.add("org_jwks", credentials.get("jwks").getAsJsonObject());
		JsonObject mtls = getOrCreate("mtls", config);
		directoryCredentials.populateMtlsConfig(mtls);
		directoryConfig.addProperty("client_id", OIDFJSON.getString(credentials.get("directoryClientId")));
		clientRepository.saveClient(id, dynamicClient);
	}

	private JsonObject getOrCreate(String name, JsonObject parent) {
		JsonObject child = parent.getAsJsonObject(name);
		if(child == null) {
			child = new JsonObject();
			parent.add(name, child);
		}
		return child;
	}

}

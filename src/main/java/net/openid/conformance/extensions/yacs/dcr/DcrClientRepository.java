package net.openid.conformance.extensions.yacs.dcr;

import com.google.gson.JsonObject;

public interface DcrClientRepository {

	void saveClient(String testId, JsonObject clientConfig);
	JsonObject getSavedClient(String testId);

    void deleteSavedClient(String id) throws ClientDeletionException;
}

package net.openid.conformance.extensions.yacs;

import org.mitre.oauth2.model.RegisteredClient;
import org.mitre.openid.connect.client.OIDCAuthenticationFilter;
import org.mitre.openid.connect.client.OIDCAuthenticationProvider;
import org.mitre.openid.connect.client.service.impl.DynamicServerConfigurationService;
import org.mitre.openid.connect.client.service.impl.StaticClientConfigurationService;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.BeanPostProcessor;

import java.util.Map;

public class YACSBeanPostProcessor implements BeanPostProcessor {

	private final Map<String, RegisteredClient> staticClients;

	@Value("${oidc.admin.group:}")
	private String adminGroup;
	@Value("${oidc.admin.issuer}")
	private String adminIss;

	public YACSBeanPostProcessor(Map<String, RegisteredClient> staticClients) {
		this.staticClients = staticClients;
	}

	@Override
	public Object postProcessAfterInitialization(Object bean, String beanName)
		throws BeansException {
		if (bean instanceof OIDCAuthenticationFilter) {
			customizeAuthFilter((OIDCAuthenticationFilter) bean);
		}
		if(bean instanceof OIDCAuthenticationProvider) {
			customizeAuthenticationProvider((OIDCAuthenticationProvider) bean);
		}
		return bean;
	}

	/**
	 * Here we customise the OIDC authentication filter so that we don't need to modify
	 * existing config
	 */
	private void customizeAuthFilter(OIDCAuthenticationFilter filter) {
		DynamicServerConfigurationService serverConfig = new DynamicServerConfigurationService();
		serverConfig.setWhitelist(staticClients.keySet());
		StaticClientConfigurationService clientConfigService = new StaticClientConfigurationService();
		clientConfigService.setClients(staticClients);
		filter.setAuthRequestUrlBuilder(new ProperlyEncodingAuthRequestUrlBuilder());
		filter.setClientConfigurationService(clientConfigService);
		filter.setServerConfigurationService(serverConfig);
		filter.setValidationServices(new DelegatingCacheService());
	}

	private void customizeAuthenticationProvider(OIDCAuthenticationProvider authenticationProvider) {
		authenticationProvider.setAuthoritiesMapper(new AuthDomainRoleAwareOBBAuthoritiesMapper(adminGroup, adminIss));
	}


}
